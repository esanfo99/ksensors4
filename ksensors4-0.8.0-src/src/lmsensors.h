/***************************************************************************
                          lmsensors.h  -  part of KSensors4
                             -------------------
    begin                : Mon Aug 6 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORS_H
#define LMSENSORS_H

#include "lmsensorschip.h"
#include "hdsensorslist.h"
#include "i8ksensorslist.h"

#include <kconfig.h>

#include <Qt/qobject.h>
#include <Qt/qwidget.h>
#include <Qt/qstringlist.h>
#include <Qt/qlist.h>

#include <sensors/sensors.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Parent of Sensor lists - LMSensorsChip, HDSensorsList, I8KSensorsList
 */

class LMSensors : public QWidget {
    Q_OBJECT

    friend class LMSensor;

public:

    LMSensors(QWidget *parent = 0, const char *name = 0);
    virtual ~LMSensors();

    void setMonitorized(bool enable);

    const QObjectList *getSensorsChips() {
        return (&children());
    };

    SensorsList *getSensorsChip(int index) {
        if ((index < 0) || (index >= children().size())) {
            return 0;
        }
        return (&children()) ? static_cast<SensorsList *> (((&children()))->at(index)) : 0;
    }

    SensorsList *getSensorsChip(const char *name) {
        return findChild<SensorsList *>(QString(name));
    }

    int count() {
        return (&children() != NULL) ? children().count() : 0;
    }

    Sensor *getSensor(const char *name);

    void emitConfigChanged(const char *name = 0) {
        emit configChanged(name);
    }

signals:

    void valueChanged(Sensor *sensor);
    void configChanged(const char *name);

protected slots:
    // Debugging
    void setValueChanged(Sensor *sensor);

private:

    bool initSensors();
    void createLMSensors();
    void createHDSensors();
    void createI8KSensors();
    void childEvent(QChildEvent *e) override;

#if SENSORS_API_VERSION < 0x400 /* libsensor 3 code */
    static int existSensor(const sensors_chip_name *chip_name, const char *sensor_name);
#endif
};

#endif
