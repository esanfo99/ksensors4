/***************************************************************************
                          generalcfg.h  -  part of KSensors4
                             -------------------
    begin                : mar may 14 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef GENERALCFG_H
#define GENERALCFG_H

#include "generalcfgdesign_ui.h"
#include "lmsensors.h"
#include "lmsensorswidget.h"
#include "palettecfg.h"

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Configuration page (global settings), controlled by KSensorsCfg
 */

class GeneralCfg : public GeneralCfgDesign {
    Q_OBJECT

public:
    GeneralCfg(LMSensors *lsensors, QWidget *parent = 0, const char *name = 0);
    virtual ~GeneralCfg();

public slots:
    void slotApplyChanges();
    void slotColorDialogFinished();

protected:
    bool eventFilter(QObject *o, QEvent *e) override;

private:
    QPointer<PaletteCfg> palPanelCfg;
    LMSensors *sensors;
};

#endif
