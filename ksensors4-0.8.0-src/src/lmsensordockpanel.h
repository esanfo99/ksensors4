/***************************************************************************
                          lmsensordockpanel.h  -  part of KSensors4
                             -------------------
    begin                : Tue Sep 18 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORDOCKPANEL_H
#define LMSENSORDOCKPANEL_H

#include "sensor.h"
#include "lmsensorswidget.h"

#include <kstatusnotifieritem.h>

#include <Qt/qwidget.h>
#include <Qt/qstring.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short System tray icon, controls context menu, controlled by LMSensorsDock
 */

class LMSensorDockPanel : public KStatusNotifierItem {
    Q_OBJECT

    friend class LMSensorsDock;

public:
    LMSensorDockPanel(Sensor *_sensor = 0, LMSensorsWidget *sensorsWidget = 0, 
            const char *name = 0);
    virtual ~LMSensorDockPanel();

    static void writeColorAlarm(const QString &name, const QColor &color);
    static void writeColorNormal(const QString &name, const QColor &color);
    static void writeShowInDock(const QString &name, bool fShow);

    static QColor readColorAlarm(const QString &name);
    static QColor readColorNormal(const QString &name);
    static bool readShowInDock(const QString &name);

public slots:

    void setValue(double value);
    void updateConfig();

signals:

    void mouseEvent(QMouseEvent *e);

protected slots:
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void paintEvent(QPaintEvent *e);
    
private:

    int w;
    int h;
    Sensor *sensor;
    QString strValue;
    QColor colorNormal, colorAlarm;

    void update(); 
};

#endif
