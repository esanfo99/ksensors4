/***************************************************************************
                          procinfo.h  -  part of KSensors4
                             -------------------
    begin                : Wed Jan 9 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <sys/types.h>

int getMemInfo(int *RamTotal, int *RamUsed, int *SwapTotal, int *SwapUsed);
bool getCpuTime(unsigned *user, unsigned *nice, unsigned *system, unsigned *idle);
int getUpTime();
int getI8KInfo(double *cpuTemp, double *leftFan, double *rightFan);
int getAcpiTemperature(double *cpuTemp);
