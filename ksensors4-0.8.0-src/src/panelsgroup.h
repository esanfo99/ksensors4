/***************************************************************************
                          panelsgroup.h  -  part of KSensors4
                             -------------------
    begin                : vie may 10 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PANELSGROUP_H
#define PANELSGROUP_H

#include <Qt/qobject.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Abstract base class for InfoPanels
 */

class PanelsGroup : public QObject {
    Q_OBJECT

public:
    PanelsGroup();
    virtual ~PanelsGroup();

public slots:
    virtual void configChanged(const char *name) = 0;
};

#endif
