/***************************************************************************
                          ksensorscfg.h  -  part of KSensors4
                             -------------------
    begin                : mie may 8 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KSENSORSCFG_H
#define KSENSORSCFG_H

#include "generalcfg.h"
#include "systemcfg.h"
#include "lmsensorscfg.h"

#include <kpagedialog.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Main Configuration dialog - controls GeneralCfg, LMSensorsCfg and
 * SystemCfg pages
 */

class KSensorsCfg : public KPageDialog {
    Q_OBJECT

public:
    KSensorsCfg(LMSensors *lsensors, LMSensorsAlarms *alarms,
            QWidget* parent = 0, const char *name = 0);
    virtual ~KSensorsCfg();

protected:
    // Debugging
    void closeEvent(QCloseEvent *e) override;
    bool eventFilter(QObject *o, QEvent *e) override;
    
private:

    LMSensors *sensors;
    GeneralCfg *m_generalcfg;
    QList<LMSensorsCfg *> m_lmsensorscfg;
    SystemCfg *m_systemcfg;
    LMSensorsAlarms *alarms;
    QWidget* sensorsDock;
};

#endif
