/***************************************************************************
                          phononplayer.cpp  -  part of KSensors4
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/* 
 * File:   phononplayer.cpp
 * Author: Eric Sanford
 *
 * Created on May 14, 2018, 3:39 PM
 * 
 * Copyright (C) 2018 Eric Sanford
 */

#include "phononplayer.h"

#include <Qt/qapplication.h>
#include <Qt/qdatetime.h>
#include <Qt/qfileinfo.h>

PhononPlayer::PhononPlayer(QObject *parent)
        : Phonon::MediaObject(parent),
        isFinished(false),
        isStopped(false),
        playMsecs(2900),
        stopMsecs(100),
        outputDeviceName(),
        soundPath(),
        soundUrl(QUrl()),
        mimeType(KMimeType::Ptr()),
        mediaSource(Phonon::MediaSource()),
        currentMrl(Phonon::Mrl()),
        metaDataTitle(),
        msecsTotalTime(0)
{
    setObjectName("phononPlayer");
    connect(this, SIGNAL(currentSourceChanged(Phonon::MediaSource)), 
            this, SLOT(mediaCurrentSourceChanged(Phonon::MediaSource)));
    connect(this, SIGNAL(finished()), this, SLOT(mediaFinished()));
    connect(this, SIGNAL(metaDataChanged()), this, SLOT(mediaMetaDataChanged()));
    connect(this, SIGNAL(stateChanged(Phonon::State, Phonon::State)), 
            this, SLOT(mediaStateChanged(Phonon::State, Phonon::State)));
    connect(this, SIGNAL(totalTimeChanged(qint64)), 
            this, SLOT(mediaTotalTimeChanged(qint64)));
    pAudioOutput = new Phonon::AudioOutput(Phonon::NotificationCategory, this);
    outputDeviceName = pAudioOutput->outputDevice().name();
    connect(&(*pAudioOutput), SIGNAL(outputDeviceChanged(const Phonon::AudioOutputDevice)), 
            this, SLOT(audioOutputDeviceChanged(const Phonon::AudioOutputDevice)));
    phononPath = Phonon::createPath(this, pAudioOutput);
}

PhononPlayer::~PhononPlayer()
{
    phononPath.disconnect();
    pAudioOutput->deleteLater();
}

/**
 * Play a sound from a local file. Stop playing after maxMsecs if sound
 * is not finished. Useful for limiting playback to the timer interval 
 * between alarm sounds.
 * 
 * @param filePath : fully-qualified path to a sound file
 * @param maxMsecs : maximum milliseconds to play
 */
void PhononPlayer::playTimedSound(const QString &filePath, unsigned maxMsecs)
{
    const QUrl *url = updateSoundUrl(filePath);
    if (url == NULL) {
        return;
    }
    soundPath = filePath;
    updateMimeType(*url);

    const Phonon::MediaSource *mSource = updateMediaSource(*url);
    if (mSource == NULL) {
        return;
    }

    if (!(*mSource == currentSource())) {
        setCurrentSource(*mSource);
    }

    playMsecs = maxMsecs - stopMsecs;
    play();

    isFinished = false;
    QTime qtime;
    qtime.start();
    while (!isFinished && qtime.elapsed() < static_cast<int> (playMsecs)) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

    if (!isStopped) {
        qtime.restart();
        stop();
        while (state() != Phonon::StoppedState && qtime.elapsed() < static_cast<int> (stopMsecs)) {
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        }
    }

    if (!isStopped) {
        qWarning("KSensors4 PhononPlayer Error: Unable to stop MediaObject");
    }
}

void PhononPlayer::close()
{
    if (!isStopped) {
        qWarning("KSensors4 PhononPlayer stopping MediaObject");
    }
    stop();
    QTime waitTime;
    waitTime.start();

    while (!isFinished && waitTime.elapsed() < static_cast<int> (playMsecs + stopMsecs)) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

    if (state() == Phonon::StoppedState) {
        qWarning("KSensors4 PhononPlayer MediaObject stopped");
    }
    else {
        qWarning("KSensors4 PhononPlayer Error: Unable to stop MediaObject!");
    }

    this->deleteLater();
}

QString PhononPlayer::toString()
{
    QString info;
    info += "PhononPlayer AudioOutput device: " + outputDeviceName + "\n";
    info += "PhononPlayer sound path:         " + soundPath + "\n";
    info += "PhononPlayer sound url:          " + soundUrl + "\n";
    info += "PhononPlayer MediaSource mrl:    " + currentMrl + "\n";
    info += "PhononPlayer KMimeType:          " + mimeType->name() + "\n";
    if (!metaDataTitle.isNull() && !metaDataTitle.isEmpty()) {
        info += "PhononPlayer MediaObject title:  " + metaDataTitle + "\n";
    }
    if (msecsTotalTime > 0) {
        info += "PhononPlayer MediaObject msecs:  " + QString::number(msecsTotalTime) + " (total time)\n";
    }
    return info;
}

// **************************** private methods ****************************

QUrl *PhononPlayer::updateSoundUrl(const QString &filePath)
{
    if (filePath.isEmpty()) {
        qWarning("KSensors4 PhononPlayer Error: No sound file path!");
        soundUrl.clear();
        return NULL;
    }

    if (soundUrl.isEmpty() || filePath.compare(soundPath) != 0) {
        QFileInfo fileInfo(filePath);
        if (!fileInfo.isFile()) {
            qWarning("KSensors4 PhononPlayer Error: Not a file: [%s]", filePath.latin1());
            soundUrl.clear();
            return NULL;
        }

        soundUrl = QUrl::fromLocalFile(filePath);

        if (soundUrl.isEmpty()) {
            qWarning("KSensors4 PhononPlayer Error: QUrl is empty!");
            soundUrl.clear();
            return NULL;
        }
        else if (!soundUrl.isValid()) {
            qWarning("KSensors4 PhononPlayer Error: Invalid QUrl: [%s]",
                     soundUrl.path().latin1());
            soundUrl.clear();
            return NULL;
        }
    }

    return &soundUrl;
}

KMimeType::Ptr PhononPlayer::updateMimeType(const QUrl &url)
{
    if (mimeType.isNull() || (soundUrl.path().compare(currentMrl.path()) != 0)) {
        KMimeType::Ptr mType = KMimeType::findByUrl(url, 0644, true);
        if (!mType->isValid()) {
            qWarning("KSensors4 PhononPlayer Warning: invalid KMimeType [%s]: ", mType->name().latin1());
        }
        mimeType = mType;
    }
    return mimeType;
}

Phonon::MediaSource *PhononPlayer::updateMediaSource(const QUrl &url)
{
    if (mediaSource.url().isEmpty() || mediaSource.url() != url) {
        mediaSource = Phonon::MediaSource(url);
        if (mediaSource.type() == Phonon::MediaSource::Empty) {
            qWarning("KSensors4 PhononPlayer Error: MediaSource is empty!");
            return NULL;
        }
        if (mediaSource.type() == Phonon::MediaSource::Invalid) {
            qWarning("KSensors4 PhononPlayer Error: MediaSource is invalid!");
            return NULL;
        }
    }
    return &mediaSource;
}

// ***************************** private slots *****************************

void PhononPlayer::mediaCurrentSourceChanged(const Phonon::MediaSource &newSource)
{
    if (newSource.mrl() != currentMrl) {
        currentMrl = newSource.mrl();
    }
}

void PhononPlayer::mediaFinished()
{
    isFinished = true;
}

void PhononPlayer::mediaMetaDataChanged()
{
    QMultiMap<QString, QString> metaData = this->metaData();
    QMultiMap<QString, QString>::iterator it;
    for (it = metaData.begin(); it != metaData.end(); ++it) {
        if (it.key().compare("TITLE") == 0) {
            QString data = (*it);
            if (!data.isNull() && !data.isEmpty()) {
                if (data.compare(metaDataTitle) != 0) {
                    metaDataTitle = data;
                }
            }
        }
    }
}

void PhononPlayer::mediaStateChanged(Phonon::State newstate, Phonon::State oldstate)
{
    switch (newstate) {
    case Phonon::StoppedState:
        isStopped = true;
        break;
    case Phonon::ErrorState:
        isStopped = false;
        qWarning("KSensors4 PhononPlayer Error: type [%d] previous state [%d] message:\n[%s]",
                 errorType(), oldstate, errorString().latin1());
        break;
    default:
        isStopped = false;
        break;
    }
}

void PhononPlayer::mediaTotalTimeChanged(qint64 newTotalTime)
{
    if (newTotalTime != msecsTotalTime) {
        msecsTotalTime = newTotalTime;
    }
}

void PhononPlayer::audioOutputDeviceChanged(const Phonon::AudioOutputDevice newOutputDevice)
{
    if (newOutputDevice.name().compare(outputDeviceName) != 0) {
        outputDeviceName = newOutputDevice.name();
    }
}
