/***************************************************************************
                          processexec.cpp  -  part of KSensors4
                             -------------------
    begin                : sab abr 27 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "processexec.h"

#include <string.h>

ProcessExec::ProcessExec()
        : KProcess(),
        out_buffer(),
        err_buffer(),
        fErrors(false)
{
    connect(this, SIGNAL(readyReadStandardOutput()), this, SLOT(slotReceivedStdout()));
    connect(this, SIGNAL(readyReadStandardError()), this, SLOT(slotReceivedStderr()));
}

ProcessExec::~ProcessExec()
{
    if (isOpen()) {
        close();
    }
}

int ProcessExec::run()
{
    clearData();
    this->setOutputChannelMode(SeparateChannels);
    int status = execute();
    return status;
}

bool ProcessExec::runAndWait()
{
    clearData();
    this->setOutputChannelMode(SeparateChannels);
    start();
    bool status = waitForFinished();
    return status;
}

void ProcessExec::slotReceivedStdout()
{
    out_buffer = out_buffer + readAllStandardOutput();
}

void ProcessExec::slotReceivedStderr()
{
    fErrors = true;
    err_buffer = readAllStandardError();
}

void ProcessExec::outInfo(QString executable)
{
    QString outData = getStdoutData();
    outData.remove(QRegExp("[\\s]"));
    if (!outData.isEmpty()) {
        qWarning("KSensors4 ProcessExec: %s stdout message:\n%s", executable.latin1(),
                 getStdoutData().latin1());
    }
}

void ProcessExec::errInfo(QString executable, int status)
{
    if (status) {
        switch (status) {
        case -2:
            qWarning("KSensors4 ProcessExec: Process [%s] could not be started!",
                     executable.latin1());
            break;
        case -1:
            qWarning("KSensors4 ProcessExec: Process [%s] crashed!",
                     executable.latin1());
            break;
        default:
            qWarning("KSensors4 ProcessExec: Process [%s] exited with code: [%d]",
                     executable.latin1(), status);
            break;
        }
    }

    if (outputErrors()) {
        qWarning("KSensors4 ProcessExec: %s stderr message:\n%s",
                 executable.latin1(), getStderrData().latin1());
    }
}
