/***************************************************************************
                    lmsensordockpanel.cpp  -  part of KSensors4
                             -------------------
    begin                : Tue Sep 18 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensordockpanel.h"

#include <ksharedconfig.h>
#include <kstandarddirs.h>

#include <Qt/qicon.h>

/**
 * 
 * @param _sensor : sensor from which to obtain values
 * @param sensorsWidget : associatedWidget
 * @param name : unique id
 */
LMSensorDockPanel::LMSensorDockPanel(Sensor *_sensor, LMSensorsWidget *sensorsWidget,
                                     const char *name)
        : KStatusNotifierItem(name, sensorsWidget), 
        w(22), 
        h(22), 
        strValue(""),
        colorNormal(Qt::darkGreen), 
        colorAlarm(Qt::red)
{
    if (name) {
        setObjectName(name);
    }
    else {
        setObjectName("no_sensor");
    }
    setCategory(Hardware); //            announce application type to system tray
    setStatus(Active); //                always show the icon
    setStandardActionsEnabled(false); // don't add Quit to menu

    if (_sensor) {
        sensor = _sensor;
        connect(sensor, SIGNAL(valueChanged(double)), this, SLOT(setValue(double)));
        connect(sensor, SIGNAL(configChanged()), this, SLOT(updateConfig()));
        updateConfig();
    }
    else {
        QIcon icon;
        icon.addPixmap(QIcon(":/pics/ksensorsdocked.png").pixmap());
        setIconByPixmap(icon);
        sensor = 0;
    }
}

LMSensorDockPanel::~LMSensorDockPanel()
{
}

void LMSensorDockPanel::setValue(double value)
{
    if (sensor) {
        switch (sensor->getType()) {
        case Sensor::lmTemp:
            strValue.sprintf("%2.0f", value);
            break;
        case Sensor::lmFan:
            strValue.sprintf("%1.1f", value / 1000);
            break;
        case Sensor::lmVoltage:
            strValue.sprintf("%2.1f", value);
            break;
        default:
            break;
        }
        update();
    }
}

void LMSensorDockPanel::updateConfig()
{
    colorAlarm = readColorAlarm(objectName());
    colorNormal = readColorNormal(objectName());
    setValue(sensor->getValue());
}

void LMSensorDockPanel::paintEvent(QPaintEvent *e)
{
    if (sensor) {
        QPixmap iconPixmap(w, h);
        iconPixmap.fill(QColor(0, 0, 0, 0));
        QPainter painter(&iconPixmap);
        QColor color = sensor->getAlarm() ? colorAlarm : colorNormal;
        QColor colorShadow = color.dark(200);
        
        painter.setPen(color);
        // top bar
        painter.drawLine(0, 0, w - 2, 0);
        // bottom bar
        painter.drawLine(0, h - 2, w - 2, h - 2);
        
        painter.setPen(colorShadow);
        // top bar shadow
        painter.drawLine(1, 1, w - 1, 1);
        // bottom bar shadow
        painter.drawLine(1, h - 1, w - 1, h - 1);
        
        if (sensor->getType() == Sensor::lmTemp) {
            QLcd::draw(&painter, 3, 4, w - 4, h - 9, strValue.toLatin1(), 
                       QLcd::drawShadow, &color, &colorShadow);
        }
        else {
            QLcd::draw(&painter, 1, 5, w - 2, h - 10, strValue.toLatin1(), 
                       QLcd::drawNumber | QLcd::alignCenter | QLcd::drawShadow, &color, &colorShadow);
        }
        setIconByPixmap(QIcon(iconPixmap));
        setStatus(sensor->getAlarm() ? NeedsAttention : Active);
    }
    e->accept();
}

void LMSensorDockPanel::mousePressEvent(QMouseEvent *e)
{
    emit mouseEvent(e);
}

void LMSensorDockPanel::mouseReleaseEvent(QMouseEvent *e)
{
    emit mouseEvent(e);
}

void LMSensorDockPanel::update()
{
    QPaintEvent e(iconPixmap().pixmap().rect());
    paintEvent(&e);
}

//*****************************************************************+

void LMSensorDockPanel::writeColorAlarm(const QString &name, const QColor &color)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    configGroup.writeEntry("colorAlarm", color);
}

void LMSensorDockPanel::writeColorNormal(const QString &name, const QColor &color)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    configGroup.writeEntry("colorNormal", color);
}

QColor LMSensorDockPanel::readColorAlarm(const QString &name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    return configGroup.readEntry<QColor>("colorAlarm", Qt::red);
}

QColor LMSensorDockPanel::readColorNormal(const QString &name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    return configGroup.readEntry<QColor>("colorNormal", Qt::darkGreen);
}

void LMSensorDockPanel::writeShowInDock(const QString &name, bool fShow)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    configGroup.writeEntry("showInDock", fShow);
}

bool LMSensorDockPanel::readShowInDock(const QString &name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    return configGroup.readEntry<bool>("showInDock", false);
}
