/***************************************************************************
                          lmsensorswidget.cpp  -  part of KSensors4
                             -------------------
    begin                : Mon Aug 6 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : 
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensorswidget.h"

#include <kglobal.h>
#include <kicon.h>
#include <ksharedconfig.h>
#include <kstandarddirs.h>

#include <Qt/qcursor.h>
#include <Qt/qnamespace.h>
#include <Qt/qshortcut.h>

/***************************************************************************
 *  Public methods                                                         *
 ***************************************************************************/

LMSensorsWidget::LMSensorsWidget(LMSensors *lsensors, QWidget *, const char *name) 
        : QWidget(0, static_cast<Qt::WindowFlags> (
                Qt::Window | 
                Qt::WindowStaysOnTopHint | 
                Qt::WA_DeleteOnClose)),
        sensors(lsensors), 
        childDraging(0), 
        panelsSize(64), 
        origMousePt(0, 0)
{
    setMinimumSize(0, 0);
    setSizeIncrement(64, 64);
    setBaseSize(64, 64);
    //
    setWindowTitle("KSensors4");
    setObjectName(name);
    //
    loadPalette();
    //
    loadGeneralOptions();
    //
    QRect rect(32, 32, 64 * 4, 64 * 3);

    KConfigGroup configGroup = KGlobal::config()->group("SensorsWidget");
    rect = configGroup.readEntry("geometry", rect);
    move(rect.topLeft());
    resize(rect.size());
    //
    createSensorPanels();
    //
    infoPanels = new InfoPanels(this);

    connect(sensors, SIGNAL(configChanged(const char *)), this, SLOT(slotConfigChanged(const char *)));
    connect(infoPanels, SIGNAL(destroyed()), this, SLOT(panelsGroupDestroyed()));
}

LMSensorsWidget::~LMSensorsWidget()
{
    if (x() >= 0 && y() >= 0) {
        cfgWriteGeometry();
    }
    delete infoPanels;
}

void LMSensorsWidget::getPerimeter(int *px, int *py)
{
    unsigned mx = 0, my = 0, x = 0, y = 0;

    const QObjectList *panels = (&children());
    if (panels && !panels->isEmpty()) {

        QObjectList::const_iterator it;
        for (it = panels->begin(); it != panels->end(); ++it) {
            Panel *panel = static_cast<Panel*> (*it);
            x = panel->x() / panelsSize;
            y = panel->y() / panelsSize;
            if (x > mx) {
                mx = x;
            }
            if (y > my) {
                my = y;
            }
        }
    }
    *px = mx + 1;
    *py = my + 1;
}


// ******************* Protected slots ******************************

void LMSensorsWidget::slotConfigChanged(const char *name)
{
    if (name && name[0] != 0) {
        LMSensorPanel *panel = findChild<LMSensorPanel *>(QString(name));
        Sensor *sensor = sensors->getSensor(name);
        if (sensor) {
            if (Panel::readShow(name) != (bool)panel) {
                if (panel) {
                    delete panel;
                }
                else {
                    panel = new LMSensorPanel(sensor, this, sensor->objectName().latin1());
                    panel->update();
                }
            }
        }
        else {
            infoPanels->configChanged(name);
        }
        panel = findChild<LMSensorPanel *>(QString(name));
        if (panel) {
            loadPalette(panel);
        }
    }
    else {
        loadPalette();
        loadGeneralOptions();
    }
}

/**
 * Debugging
 */
void LMSensorsWidget::panelsGroupDestroyed()
{
}

/***************************************************************************
 * Private methods                                                         *
 ***************************************************************************/

void LMSensorsWidget::childEvent(QChildEvent *e)
{
    if (e->polished() && QString(e->child()->className()).compare("KMenu") != 0) {
        loadPanelConfig(static_cast<Panel *> (e->child()));
    }
}

// ************* Drag panels and show menu ************************

void LMSensorsWidget::childEventReceived(QEvent *e)
{
    QMouseEvent *m;

    switch (e->type()) {
    case QEvent::MouseButtonPress:
        m = static_cast<QMouseEvent *> (e);
        if (m->button() == Qt::LeftButton) {
            startDragChild(m, static_cast<QWidget *> (sender()));
        }
        break;
    case QEvent::MouseButtonRelease:
        m = static_cast<QMouseEvent *> (e);
        if (m->button() == Qt::RightButton) {
            emit rightMouseClicked(m);
        }
        break;
    default:
        break;
    }
}

void LMSensorsWidget::startDragChild(QMouseEvent *m, QWidget *w)
{
    if (childDraging == 0) {
        childDraging = w;
        grabMouse(QCursor(Qt::SizeAllCursor));
        origMousePt = w->mapFromGlobal(m->globalPos());
        w->raise();
        w->update();
    }
}

void LMSensorsWidget::endDragChild()
{
    int x = childDraging->x();
    int y = childDraging->y();

    if (x < 0) x = 0;
    if (y < 0) y = 0;
    releaseMouse();
    childDraging->move(
                       ((x + childDraging->width() / 2) / panelsSize) * panelsSize,
                       ((y + childDraging->height() / 2) / panelsSize) * panelsSize
                       );
    savePanelConfig(static_cast<Panel *> (childDraging));
    childDraging = 0;
}

void LMSensorsWidget::mouseMoveEvent(QMouseEvent *m)
{
    if (childDraging) {
        childDraging->move(m->pos() - origMousePt);
        childDraging->update();
    }
}

void LMSensorsWidget::mouseReleaseEvent(QMouseEvent *m)
{
    if (childDraging && m->button() == Qt::LeftButton) {
        endDragChild();
    }
    else
        if (m->button() == Qt::RightButton) {
        emit rightMouseClicked(m);
    }
}

// ********************** Panels creation ************************

void LMSensorsWidget::findUnusedPosition(Panel *newPanel, int *px, int *py)
{
    bool found;
    unsigned mx = 0, my = 0, x = 0, y = 0, a[32] = {0,};

    const QObjectList *panels = (&children());
    if (panels && !panels->isEmpty()) {

        QObjectList::const_iterator it;
        for (it = panels->begin(); it != panels->end(); ++it) {
            Panel *panel = static_cast<Panel*> (*it);
            if (panel->objectName().compare(newPanel->objectName()) == 0) {
                continue;
            }
            x = panel->x() / panelsSize;
            y = panel->y() / panelsSize;
            if (x < 32 && y < 32) {
                a[y] |= (1 << x);
            }
            if (x > mx) {
                mx = x;
            }
            if (y > my) {
                my = y;
            }
        }
    }
    //
    for (x = 0, y = 0, found = false; y <= my && !found;) {
        found = ((a[y] & (1 << x)) == 0);
        if (!found) {
            if (++x > mx) {
                x = 0;
                y++;
            }
        }
    }

    if (!found) {
        x = (width() - 1) / panelsSize;
        if (x > mx) {
            x = mx + 1;
            y = 0;
        }
        else {
            x = 0;
            y = my + 1;
        }
    }
    //
    *px = x;
    *py = y;
}

void LMSensorsWidget::loadPanelConfig(Panel *panel)
{
    if (!panel) {
        return;
    }

    loadPalette(panel);

    bool fNoCfg;
    QPoint p(-1, -1);

    KConfigGroup configGroup = KGlobal::config()->group(panel->objectName());
    p = configGroup.readEntry("showPos", p);

    fNoCfg = (p.x() < 0 || p.y() < 0);

    if (fNoCfg) {
        findUnusedPosition(panel, &p.rx(), &p.ry());
        configGroup.writeEntry("showPos", p);
    }
    panel->move(p.x() * panelsSize, p.y() * panelsSize);
    panel->resize(panelsSize, panelsSize);
    panel->show();
    if (fNoCfg) {
        savePanelConfig(panel);
        adjustSize();
    }
    panel->installEventFilter(this);
    connect(panel, SIGNAL(eventReceived(QEvent *)), this, SLOT(childEventReceived(QEvent *)));
}

void LMSensorsWidget::savePanelConfig(Panel *panel)
{
    KConfigGroup configGroup = KGlobal::config()->group(panel->objectName());
    QPoint p = panel->pos() / panelsSize;
    configGroup.writeEntry("show", true);
    configGroup.writeEntry("showPos", p);
}

void LMSensorsWidget::createSensorPanels()
{
    const QObjectList *sensorGroups = sensors->getSensorsChips();
    if (sensorGroups && !sensorGroups->isEmpty()) {
        QObjectList::const_iterator group_iter;
        for (group_iter = sensorGroups->begin(); group_iter != sensorGroups->end(); ++group_iter) {
            SensorsList *group = static_cast<SensorsList *> (*group_iter);
            const QObjectList *sensorList = (group->getSensors());
            if (sensorList && !sensorList->isEmpty()) {
                QObjectList::const_iterator sensor_iter;
                for (sensor_iter = sensorList->begin(); sensor_iter != sensorList->end(); ++sensor_iter) {
                    LMSensor *sensor = static_cast<LMSensor *> (*sensor_iter);
                    if (Panel::readShow(sensor->objectName().latin1())) {
                        LMSensorPanel *panel = new LMSensorPanel(sensor, this, sensor->objectName().latin1());
                        panel->update();
                     }
                }
            }
        }
    }
}

void LMSensorsWidget::resizePanels(int newSize)
{
    const QObjectList *panels = (&children());

    if (panels && !panels->isEmpty()) {
        QObjectList::const_iterator it;
        for (it = panels->begin(); it != panels->end(); ++it) {
            Panel *panel = static_cast<Panel *> (*it);
            panel->move((panel->x() / panelsSize) * newSize,
                        (panel->y() / panelsSize) * newSize);
            panel->resize(newSize, newSize);
        }
    }
    panelsSize = newSize;
    adjustSize();
}

int LMSensorsWidget::cfgReadPanelSize()
{
    KConfigGroup configGroup = KGlobal::config()->group("SensorsWidget");
    return configGroup.readEntry("PanelsSize", 64);
}

void LMSensorsWidget::cfgWritePanelSize(int newSize)
{
    KConfigGroup configGroup = KGlobal::config()->group("SensorsWidget");
    configGroup.writeEntry("PanelsSize", newSize);
}

void LMSensorsWidget::cfgWriteGeometry()
{
    KConfigGroup configGroup = KGlobal::config()->group("SensorsWidget");
    configGroup.writeEntry<QRect>("geometry", QRect(pos(), size()));
    configGroup.sync();
}

void LMSensorsWidget::loadGeneralOptions()
{
    int newSize = cfgReadPanelSize();
    if (panelsSize != newSize) {
        setSizeIncrement(newSize, newSize);
        setBaseSize(newSize, newSize);
        resizePanels(newSize);
    }

    QPalette pal;
    QPixmap pm = QIcon(":/pics/ksensorsbackground.png").pixmap();
    pal.setBrush(backgroundRole(), QBrush(pm.scaled(newSize, newSize)));
    setPalette(pal);
}

// **************************** palette stuff ******************************

void LMSensorsWidget::loadPalette()
{
    QPalette pal = palette();
    cfgReadPalette(pal, 0, true);
    setPalette(pal);

    const QObjectList *panels = (&children());
    if (panels && !panels->isEmpty()) {
        QObjectList::const_iterator it;

        for (it = panels->begin(); it != panels->end(); ++it) {
            Panel *panel = static_cast<Panel *> (*it);
            KConfigGroup configGroup = KGlobal::config()->group(panel->objectName());
            if (!configGroup.readEntry<bool>("PaletteActive", true)) {
                panel->setPalette(pal);
            }
        }
    }
}

void LMSensorsWidget::loadPalette(Panel *panel)
{
    if (!panel) {
        loadPalette();
        return;
    }

    QPalette pal = palette();
    if (cfgReadPalette(pal, panel->objectName(), false)) {
        panel->setPalette(pal);
    }
    else {
        if (cfgReadPalette(pal, 0, false)) {
            panel->setPalette(pal);
        }
        else {
            panel->setPalette(QPalette());
        }
    }
}

/** 
 * Sensor panels are QFrame widgets with shadow style QFrame::Raised, one 
 * pixel borders. Text color applies to the panel value. Foreground color 
 * applies to the title and text below the panel value. Light color applies
 * to highlights (upper and left borders). Dark applies to shadows (lower 
 * and right borders). Qt4 default background is transparent. KDE4 
 * BackgroundNormal is Cardboard Grey (0xEFF0F1). 
 * 
 * @param pal : reference to palette object
 */
void LMSensorsWidget::getDefaultPalette(QPalette &pal)
{
    //pal.setColor(QPalette::Background, Qt::white);
    pal.setColor(QPalette::Light, Qt::white);
    pal.setColor(QPalette::Dark, Qt::black);
    pal.setColor(QPalette::Foreground, Qt::black);
    pal.setColor(QPalette::Text, Qt::black);
}

/** 
 * Set palette colors for General group or an individual sensor. If name is
 * NULL, then set palette colors for General group. Otherwise, set palette 
 * colors for the specified sensor. If PaletteActive is true in ksensorsrc,
 * then set custom palette colors specified in ksensorsrc and return true. 
 * If PaletteActive is false or not set in ksensorsrc, then consider
 * getDefault. If getDefault is true, then set default palette colors and 
 * return true. Otherwise, do not set palette colors and return false.
 * 
 * @param pal : reference to palette object
 * @param name : ksensorsrc group name, or NULL
 * @param getDefault : if true, then set default color palette, otherwise do nothing (only relevant if PaletteActive is false or not set)
 * @return true if palette colors are set, otherwise false
 */
bool LMSensorsWidget::cfgReadPalette(QPalette &pal, const QString &name,
                                     bool getDefault)
{
    KConfigGroup configGroup = KGlobal::config()->group((!name.isNull() && !name.isEmpty()) ? name : "General");

    if (configGroup.readEntry<bool>("PaletteActive", false)) {
        pal.setColor(QPalette::Background, configGroup.readEntry<QColor>("ColorBackground", Qt::white));
        pal.setColor(QPalette::Light, configGroup.readEntry<QColor>("ColorBackground", Qt::white));
        pal.setColor(QPalette::Dark, configGroup.readEntry<QColor>("ColorShadow", Qt::black));
        pal.setColor(QPalette::Foreground, configGroup.readEntry<QColor>("ColorValue", Qt::black));
        pal.setColor(QPalette::Text, configGroup.readEntry<QColor>("ColorTitle", Qt::black));
        return true;
    }

    if (getDefault) {
        getDefaultPalette(pal);
        return true;
    }

    return false;
}

void LMSensorsWidget::cfgWritePalette(const QPalette &pal, const QString &name)
{
    KConfigGroup configGroup = KGlobal::config()->group((!name.isNull() && !name.isEmpty()) ? name : "General");

    configGroup.writeEntry("PaletteActive", true);
    configGroup.writeEntry("ColorBackground", pal.background().color());
    configGroup.writeEntry("ColorShadow", pal.dark().color());
    configGroup.writeEntry("ColorValue", pal.foreground().color());
    configGroup.writeEntry("ColorTitle", pal.text().color());
}

void LMSensorsWidget::cfgUnsetPalette(const QString &name)
{
    KConfigGroup configGroup = KGlobal::config()->group((!name.isNull() && !name.isEmpty()) ? name : "General");

    configGroup.writeEntry("PaletteActive", false);
    configGroup.deleteEntry("ColorBackground");
    configGroup.deleteEntry("ColorShadow");
    configGroup.deleteEntry("ColorTitle");
    configGroup.deleteEntry("ColorValue");
}

// *********************************** End *********************************
