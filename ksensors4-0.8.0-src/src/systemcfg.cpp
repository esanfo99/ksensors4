/***************************************************************************
                          systemcfg.cpp  -  part of KSensors4
                             -------------------
    begin                : vie may 17 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "systemcfg.h"
#include "infopanels.h"

#include <klocale.h>

SystemCfg::SystemCfg(LMSensors *lsensors, QWidget *parent, const char *name)
        : SystemCfgDesign(parent, name), 
        sensors(lsensors)
{
    palPanelCfg = new PaletteCfg(boxPanelPalette, "system");
    palPanelCfg->installEventFilter(this);
    boxPanelPalette->setColumnLayout(0, Qt::Vertical);
    boxPanelPalette->layout()->setSpacing(6);
    boxPanelPalette->layout()->setMargin(12);
    QGridLayout *boxPanelPaletteLayout = new QGridLayout(boxPanelPalette->layout());
    boxPanelPaletteLayout->setAlignment(Qt::AlignTop);
    boxPanelPaletteLayout->addWidget(palPanelCfg, 0, 0);

    connect(TabWidget, SIGNAL(currentChanged(QWidget*)), this, SLOT(slotTabWidgetChanged(QWidget *)));

    listSensors->clear();
    listSensors->insertItem(i18n("CPU Speed"));
    listSensors->insertItem(i18n("CPU State"));
    listSensors->insertItem(i18n("RAM Used"));
    listSensors->insertItem(i18n("SWAP Used"));
    listSensors->insertItem(i18n("Up Time"));
    listSensors->setCurrentItem(0);
    readSensorInfo(0);

    connect(listSensors, SIGNAL(highlighted(int)), this, SLOT(slotCurrentSensorChanged(int)));
}

SystemCfg::~SystemCfg()
{
    palPanelCfg->deleteLater();
}

void SystemCfg::slotTabWidgetChanged(QWidget *)
{
    switch (TabWidget->currentPageIndex()) {
    case 0:
        readSensorInfo(listSensors->currentItem());
        break;
    case 1:
        readPreferencesInfo();
        break;
    }
}

QString SystemCfg::getPanelName(int panelIndex)
{
    switch (panelIndex) {
    case 0:
        return "proc.CPUINFO";
    case 1:
        return "proc.CPULOAD";
    case 2:
        return "proc.RAMINFO";
    case 3:
        return "proc.SWAPINFO";
    case 4:
        return "proc.UPTIME";
    default:
        break;
    }
    return 0;
}

void SystemCfg::slotCurrentSensorChanged(int panelIndex)
{
    readSensorInfo(panelIndex);
}

void SystemCfg::readSensorInfo(int panelIndex)
{
    const QString objectName = getPanelName(panelIndex);
    checkShow->setChecked(Panel::readShow(objectName));
    palPanelCfg->readPalette(objectName);
}

void SystemCfg::readPreferencesInfo()
{
    SpinUpdateTime->setValue(InfoPanels::cfgReadUpdateInterval());
}

void SystemCfg::writeSensorInfo(int panelIndex)
{
    const QString panelName = getPanelName(panelIndex);
    Panel::writeShow(panelName, checkShow->isChecked());
    palPanelCfg->savePalette(panelName);
    KConfigGroup configGroup = KGlobal::config()->group(panelName);
    configGroup.sync();
    sensors->emitConfigChanged(panelName.latin1());
}

void SystemCfg::writePreferencesInfo()
{
    InfoPanels::cfgWriteUpdateInterval(SpinUpdateTime->value());
    sensors->emitConfigChanged("proc");
}

void SystemCfg::slotApplyChanges()
{
    if (!isVisible()) {
        return;
    }
    switch (TabWidget->currentPageIndex()) {
    case 0:
        writeSensorInfo(listSensors->currentItem());
        break;
    case 1:
        writePreferencesInfo();
        break;
    default:
        break;
    }
}

bool SystemCfg::eventFilter(QObject *, QEvent *e)
{
    e->accept();
    return false;
}
