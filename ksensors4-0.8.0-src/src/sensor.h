/***************************************************************************
                          sensor.h  -  part of KSensors4
                             -------------------
    begin                : mie abr 24 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SENSOR_H
#define SENSOR_H

#include <kconfig.h>

#include <Qt/qwidget.h>
#include <Qt/qstring.h>

// forward declaration
class SensorsList;

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Common functions for derived classes LMSensor, HddSensor
 */

class Sensor : public QWidget {
    
    Q_OBJECT

    friend class SensorsList;

public:

    enum SensorClass {
        lmSensor, hdSensor
    };

    enum TempScale {
        dgCelsius, dgFahrenheit, dgKelvin, dgDefault
    };

    enum SensorType {
        lmTemp, lmFan, lmVoltage
    };

    Sensor(SensorsList *parent = 0, const char *name = 0);
    virtual ~Sensor();

    inline void setType(SensorType newType) {
        type = newType;
    };
    void setValue(double newVal, TempScale scale = dgDefault, bool ajust = true);

    inline SensorType getType() {
        return type;
    };

    inline const QString &getDescription() {
        return description;
    };
    
    inline double getValue() {
        return celsiusTo(val);
    };

    inline double getValueIdeal() {
        return celsiusTo(valIdeal);
    };

    inline double getValueMax() {
        return celsiusTo(valMax);
    };

    inline double getValueMin() {
        return celsiusTo(valMin);
    };

    inline double getCompensation() {
        return celsiusToDiff(compensation);
    };

    inline double getMultiplicator() {
        return multiplicator;
    };

    inline bool getAlarm() {
        return (val < valMin || val > valMax);
    };

    inline bool getMonitorize() {
        return monitorize;
    };

    inline bool isAlarmValue(double value) {
        value = toCelsius(value);
        return (value < valMin || value > valMax);
    };

    inline bool getAlarmOn() {
        return alarmOn;
    };
    
    void setAlarmOn(bool isAlarm) {
        alarmOn = isAlarm;
    };
    
    bool monitorized();
    void setMonitorized(bool enable);
    virtual void setDescription(const QString &desc);
    void setValueIdeal(double value, TempScale scale = dgDefault);
    void setValueMax(double value, TempScale scale = dgDefault);
    void setValueMin(double value, TempScale scale = dgDefault);
    void setCompensation(double value, TempScale scale = dgDefault);
    void setMultiplicator(double value);

    SensorClass getClass();
    TempScale getTempScale();
    double toCelsius(double val, TempScale scale = dgDefault);
    double celsiusTo(double val, TempScale scale = dgDefault);
    double toCelsiusDiff(double val, TempScale scale = dgDefault);
    double celsiusToDiff(double val, TempScale scale = dgDefault);

    QString getPrintMask(bool addSufix);

public slots:

    void readConfig();
    void writeConfig();

signals:

    void valueChanged(double);
    void configChanged();
    
private:

    SensorType type;
    QString description;
    double valMin;
    double valIdeal;
    double valMax;
    double val;
    double compensation;
    double multiplicator;
    bool monitorize;
    bool alarmOn;
    static int id;

    inline double adjustValue(double value) {
        return value * multiplicator + compensation;
    };
    const char *getSensorPrintMask(int sensorType, bool addSufix, TempScale temp);

};

#endif
