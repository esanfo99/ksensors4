/***************************************************************************
                          uptimepanel.h  -  part of KSensors4
                             -------------------
    begin                : Sun Nov 25 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef UPTIMEPANEL_H
#define UPTIMEPANEL_H

#include "panel.h"
#include "qlcdstring.h"

#include <Qt/qevent.h>
#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Panel for InfoPanels - System Information (uptime), calls getUpTime()
 */

class UpTimePanel : public Panel {
    Q_OBJECT

public:
    UpTimePanel(QWidget *parent = 0, const char *name = 0);
    virtual ~UpTimePanel();

protected:

    void updateInfo() override;
    void paintEvent(QPaintEvent *e) override;

private:

    QLCDString *lcd;
    void resizeEvent(QResizeEvent *e) override;
};

#endif
