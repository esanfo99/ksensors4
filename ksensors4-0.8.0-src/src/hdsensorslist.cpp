/***************************************************************************
                          hdsensorslist.cpp  -  part of KSensors4
                             -------------------
    begin                : vie abr 26 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "hddsensor.h"
#include "hdsensorslist.h"
#include "lmsensorschip.h"

#include <klocale.h>

#include <Qt/qstringlist.h>
#include <Qt/qregexp.h>

#include <dirent.h>


bool HDSensorsList::isHddtemp = false;
bool HDSensorsList::isDaemon = false;
unsigned HDSensorsList::daemonPort = 7634;
QStringList HDSensorsList::daemonData = QStringList();

HDSensorsList::HDSensorsList(QWidget *parent, const char *name)
        : SensorsList(parent, name),
        process(0)
{

    setDescription(QString("hddtemp"));
    setClass(HddSensor::hdSensor);
    readConfig();

    if (!isHddTempAvailable()) {
        if (!isDaemonAvailable()) {
            return;
        }
    }

    QStringList disks;

    if (isHddtemp) {
        if (!getDisks(disks)) {
            return;
        }
    }
    else if (isDaemon) {
        if (!getDisksFromDaemon(disks)) {
            return;
        }
    }

    QStringList::Iterator it;
    for (it = disks.begin(); it != disks.end(); ++it) {

        if (isHddtemp) {
            ProcessExec proc;
            proc << "hddtemp" << "-q";
            proc << *it;
            if (proc.runAndWait()) {
                double value;
                QString str;
                if (getDiskInfo(proc.getStdoutData(), str, value)) {
                    HddSensor *sensor = new HddSensor(this);
                    sensor->setType(HddSensor::lmTemp);
                    sensor->setDisk(*it);
                    QString sysName = str.replace(QChar(' '), QChar('_'));
                    QString udevName = sensor->getUdevName(sysName);
                    if (udevName.isNull()) {
                        sensor->setObjectName(sysName);
                    }
                    else {
                        sensor->setObjectName(udevName);
                    }
                    sensor->setDescription(*it);
                    sensor->setValueMax(55, HddSensor::dgCelsius);
                    sensor->setValueMin(10, HddSensor::dgCelsius);
                    sensor->setValueIdeal(value, HddSensor::dgCelsius);
                    sensor->setValue(value, HddSensor::dgCelsius);
                    sensor->readConfig();
                }
            }
        }
        else if (isDaemon) {
            int index = 1;
            while ((index + 4) < daemonData.length()) {
                QString disk = daemonData.at(index);
                if (disk.compare(*it) == 0) {
                    HddSensor *sensor = new HddSensor(this);
                    sensor->setType(HddSensor::lmTemp);
                    sensor->setDisk(*it);
                    QString sysName = QString(daemonData.at(index + 1)).replace(QChar(' '), QChar('_'));
                    QString udevName = sensor->getUdevName(sysName);
                    if (udevName.isNull()) {
                        sensor->setObjectName(sysName);
                    }
                    else {
                        sensor->setObjectName(udevName);
                    }
                    sensor->setDescription(*it);
                    sensor->setValueMax(55, HddSensor::dgCelsius);
                    sensor->setValueMin(10, HddSensor::dgCelsius);
                    bool ok;
                    double value = QString(daemonData.at(index + 2)).toDouble(&ok);
                    if (!ok) {
                        value = 30;
                    }
                    sensor->setValueIdeal(value, HddSensor::dgCelsius);
                    sensor->setValue(value, HddSensor::dgCelsius);
                    sensor->readConfig();
                    break;
                }
                index += 5;
            }
        }
    }
}

HDSensorsList::~HDSensorsList()
{
    delete process;
}

//*************

void HDSensorsList::updateSensors()
{
    if (process) {
        return;
    }

    const QObjectList *sensors = (&children());
    if (!sensors) {
        return;
    }

    if (isHddtemp) {
        QStringList params;
        QObjectList::const_iterator it;
        for (it = sensors->begin(); it != sensors->end(); ++it) {
            HddSensor *sensor = static_cast<HddSensor *> (*it);
            if (sensor->monitorized()) {
                params << sensor->getDisk();
            }
        }

        if (params.count() > 0) {
            process = new ProcessExec;
            connect(process, SIGNAL(finished(int, QProcess::ExitStatus)),
                    this, SLOT(slotProcessExited(int, QProcess::ExitStatus)));
            QStringList::iterator it;
            params.push_front("-q");
            process->setProgram("hddtemp", params);
            int status = process->run();
            if (status) {
                qWarning("KSensors4 HDSensorsList: ProcessExec error code: %d", status);
            }
        }
    }
    else if (isDaemon) {
        process = new ProcessExec;
        connect(process, SIGNAL(finished(int, QProcess::ExitStatus)),
                this, SLOT(slotProcessExited(int, QProcess::ExitStatus)));
        QStringList params;
        params << "-u" << "tcp4:localhost:" + QString::number(daemonPort) << "stdout";
        process->setProgram("socat", params);
        int status = process->run();
        if (status) {
            qWarning("KSensors4 HDSensorsList: ProcessExec error code: %d", status);
        }
    }
}

void HDSensorsList::slotProcessExited(int exitCode, QProcess::ExitStatus exitStatus)
{
    const QObjectList *sensors = (&children());

    if (!sensors || sensors->isEmpty()) {
        process->deleteLater();
        return;
    }

    if (exitStatus == QProcess::CrashExit) {
        qWarning("KSensors4 HDSensorsList: exitStatus == QProcess::CrashExit [%d]", QProcess::CrashExit);
        process->deleteLater();
        return;
    }

    if (process->outputErrors()) {
        qWarning("KSensors4 HDSensorsList: hddtemp exit code: %d stderr message:\n%s", exitCode, QString(process->getStderrData()).latin1());
        process->deleteLater();
        return;
    }

    if (isHddtemp) {
        QStringList data = process->getStdoutData().split(QChar('\n'));
        QStringList::Iterator it;
        for (it = data.begin(); it != data.end(); ++it) {
            QObjectList::const_iterator sensor_iter;
            for (sensor_iter = sensors->begin(); sensor_iter != sensors->end(); ++sensor_iter) {
                HddSensor *sensor = static_cast<HddSensor *> (*sensor_iter);
                QRegExp rx(QString(sensor->getDisk()) + QString(":\\s+.+:\\s+(\\d+).*C"));
                if (rx.indexIn((*it)) > -1) {
                    sensor->setValue(rx.cap(1).toDouble(), HddSensor::dgCelsius);
                }
            }
        }
    }
    else if (isDaemon) {
        daemonData = process->getStdoutData().split(QChar('|'));
        QObjectList::const_iterator it;
        for (it = sensors->begin(); it != sensors->end(); ++it) {
            HddSensor *sensor = static_cast<HddSensor *> (*it);
            int index = 1;
            while ((index + 4) < daemonData.length()) {
                QString disk = daemonData.at(index);
                if (disk.compare(QString(sensor->getDisk())) == 0) {
                    sensor->setValue(QString(daemonData.at(index + 2)).toDouble(), HddSensor::dgCelsius);
                    break;
                }
                index += 5;
            }
        }
    }

    process->deleteLater();
}

// ***************  Static methods

bool HDSensorsList::getDiskInfo(const QString &buf, QString &name, double &value)
{
    QRegExp rx(":\\s+(.+):\\s+(\\d+).*C");

    if (rx.indexIn(buf) > -1) {
        bool ok;
        name = rx.cap(1);
        value = rx.cap(2).toDouble(&ok);
        if (ok) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

bool HDSensorsList::getDiskInfoFromDaemon()
{
    bool status = false;
    ProcessExec proc;
    proc.clearData();

    proc << "socat" << "-u" << "tcp4:localhost:" + QString::number(daemonPort) << "stdout";
    if (proc.runAndWait()) {
        if (proc.getStderrData().isEmpty() && !proc.outputErrors()) {
            daemonData = proc.getStdoutData().split(QChar('|'));
            status = true;
        }
        else {
            qWarning("KSensors4 HDSensorsList: hddtemp stderr message:\n%s", proc.getStderrData().latin1());
        }
    }
    else {
        if (!proc.getStderrData().isEmpty()) {
            qWarning("KSensors4 HDSensorsList: hddtemp stderr message:\n%s", proc.getStderrData().latin1());
        }
    }
    if (proc.isOpen()) {
        proc.close();
    }
    return status;
}

bool HDSensorsList::isHddTempAvailable()
{
    isHddtemp = false;
    QStringList disks;
    if (!getDisks(disks) || disks.length() == 0) {
        return isHddtemp;
    }
    ProcessExec proc;
    proc << "hddtemp" << "-q";
    proc << disks.at(0);
    if (proc.runAndWait()) {
        double value;
        QString str;
        if (getDiskInfo(proc.getStdoutData(), str, value)) {
            isHddtemp = true;
        }
        if (!proc.getStderrData().isEmpty()) {
            qWarning("KSensors4 HDSensorsList: hddtemp stderr message:\n%s", proc.getStderrData().latin1());
        }
    }
    if (proc.isOpen()) {
        proc.close();
    }
    return isHddtemp;
}

bool HDSensorsList::isDaemonAvailable()
{
    if (getDiskInfoFromDaemon()) {
        isDaemon = true;
    }
    else {
        isDaemon = false;
    }
    return isDaemon;
}

bool HDSensorsList::getDisks(QStringList &disks)
{
    DIR *dir;

    /* Get a listing of the hard drives looking under /sys/block first,
     * then falling back to /proc/ide */
    if ((dir = opendir("/sys/block")) == NULL) {
        if ((dir = opendir("/proc/ide")) == NULL) {
            return false;
        }
    }
    QString str;
    struct dirent *ptr;
    while ((ptr = readdir(dir))) {
        if ((ptr->d_name[0] == 'h' || ptr->d_name[0] == 's') && ptr->d_name[1] == 'd') {
            str.sprintf("/dev/%s", ptr->d_name);
            disks << str;
        }
    }
    closedir(dir);
    return true;
}

bool HDSensorsList::getDisksFromDaemon(QStringList &disks)
{
    bool status = false;
    int index = 1;
    while ((index + 4) < daemonData.length()) {
        QString str = daemonData.at(index);
        if (str.length() == 8
            && str.startsWith("/dev/")
            && str[6] == QChar('d')
            && (str[5] == QChar('s') || str[5] == QChar('h'))
            && QChar(str[7]).isLower()) {
            if (!disks.contains(str)) {
                disks << str;
            }
        }
        else {
            qWarning("KSensors4 HDSensorsList: not adding [%s] to disks\n", str.latin1());
        }
        index += 5;
    }
    if (disks.length() > 0) {
        status = true;
    }
    return status;
}

// ***************
