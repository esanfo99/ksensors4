/***************************************************************************
                          palettecfg.h  -  part of KSensors4
                             -------------------
    begin                : lun abr 15 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PALETTECFG_H
#define PALETTECFG_H

#include "palettecfgdesign_ui.h"

#include <kcolorbutton.h>
#include <kcolordialog.h>

#include <Qt/qcolor.h>
#include <Qt/qpalette.h>
#include <Qt/qpointer.h>
#include <Qt/qobject.h>
#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Color dialog for Panel palette, called in GeneralCfg, LMSensorsCfg, SystemCfg
 */

class PaletteCfg : public PaletteCfgDesign {
    Q_OBJECT

public:
    PaletteCfg(QWidget *parent = 0, const char *name = 0);
    virtual ~PaletteCfg();

    void readPalette(const QString &objectName);
    void savePalette(const QString &objectName);

signals:
    void colorDialogFinished();

protected slots:

    void slotComboSelected(int paletteIndex = 0);
    void slotColorChanged(const QColor &newColor);
    void slotPaletteDefault();
    void slotColorButtonClicked();
    void slotColorDialogFinished();
    
private:

    bool isDefaultPal;
    QString configName;
    QPointer<KColorDialog> pKColorDialog;

    void setPanelPalette(const QPalette &pal, bool updateColorButton = false);
    bool isDefaultPalette(const QPalette &pal);
};

#endif
