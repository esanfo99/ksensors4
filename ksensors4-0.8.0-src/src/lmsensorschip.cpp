/***************************************************************************
                          lmsensorschip.cpp  -  part of KSensors4
                             -------------------
    begin                : Wed Feb 20 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensorschip.h"

#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sensors/sensors.h>

//****************************************************************************
// Public methods
//****************************************************************************

LMSensorsChip::LMSensorsChip(const sensors_chip_name *chip, QWidget *parent,
                             const char *name)
        : SensorsList(parent, name), chip_name(chip)
{
    setClass(Sensor::lmSensor);
    setObjectName(QString(chip_name->prefix));
    setDescription(chip_name->prefix);
    readConfig();
    createSensors();
}

LMSensorsChip::~LMSensorsChip()
{
}

const sensors_chip_name *LMSensorsChip::getChipName()
{
    return chip_name;
}

void LMSensorsChip::createSensors()
{
    if (!chip_name) return;
    int nr1 = 0;
    int nr2 = 0;
#if SENSORS_API_VERSION < 0x400 /* libsensor 3 code */
    const sensors_feature_data *data;
    data = sensors_get_all_features(*chip_name, &nr1, &nr2);
#else
    const sensors_feature *data;
    data = sensors_get_features(chip_name, &nr1);
#endif
    while (data) {
        LMSensor *sensor = new LMSensor(this);
        if (!sensor->init(&data, &nr1, &nr2)) {
            sensor->deleteLater();
        }
    }
}

void LMSensorsChip::updateSensors()
{
    const QObjectList *sensors =  (&children());

    if (sensors && !sensors->isEmpty()) {
        QObjectList::const_iterator it;
        for (it = sensors->begin(); it != sensors->end(); ++it) {
            LMSensor *sensor = static_cast<LMSensor *> (*it);
            if (sensor->monitorized()) {
                sensor->updateValue();
            }
        }
    }
}

/**
 * sensors_snprintf_chip_name (in the lm-sensors libarary, libsensors.so) 
 * prints a chip name from its internal representation. 
 * 
 * @return A QString containing the lm-sensors chip name, if successful, or 
 * "unknown" if unsuccessful. Useful for debugging.
 */
QString LMSensorsChip::getSensorGroupName()
{
    int status = -1;
    char chip_name_str[256] = {0,};
    if (chip_name) {
        status = sensors_snprintf_chip_name(chip_name_str, 255, chip_name);
    }
    if (status < 0) {
        return QString("unknown");
    }
    return QString(chip_name_str);
}
