/***************************************************************************
                          lmsensorsdock.h  -  part of KSensors4
                             -------------------
    begin                : Sun Sep 23 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORSDOCK_H
#define LMSENSORSDOCK_H

#include "lmsensors.h"
#include "lmsensorpanel.h"
#include "lmsensorswidget.h"
#include "lmsensordockpanel.h"
#include "lmsensorsalarms.h"
#include "ksensorscfg.h"

#include <kconfig.h>
#include <kdialog.h>
#include <kmenu.h>

#include <Qt/qpointer.h>
#include <Qt/qsystemtrayicon.h>
#include <Qt/qwidget.h>

/**
 * LMSensorsDock is the controlling QWidget. It is created in main(). It 
 * creates and destroys the system tray icons, sensors widget, configuration
 * dialog, and alarm widget. When LMSensorsDock emits a destroyed() signal,
 * the application event handler calls KApplication::quit().
 * 
 * @author Miguel Novas
 * @author Eric Sanford
 * 
 * @short Parent of LMSensorDockPanel icons, controls LMSensorsWidget, 
 * LMSensorsAlarms, KSensorsCfg, LMSensors
 */
class LMSensorsDock : public QWidget {
    Q_OBJECT

    friend class KSensorsCfg;

public:
    LMSensorsDock(bool fNoDock = false, QWidget *parent = 0, const char *name = 0);
    virtual ~LMSensorsDock();

    int getTrayIconCount() {
        return countTrayIcons;
    }

signals:

    void configChanged(const char *name);

public slots:
    void saveDesktopConfig();

protected:
    void closeEvent(QCloseEvent *e) override;
    bool eventFilter(QObject *o, QEvent *e) override;

protected slots:

    void updateItemDock(const char *name);
    void mouseEventReceived(QMouseEvent *e);
    void minimizeRestoreWidget();
    void createConfigWidget();
    void updateMenu();
    void showAbout();
    void showHelp();

    void iconActivated(bool active, const QPoint &);
    // Debugging
    void slotConfigWidgetDestroyed();

private:

    bool noDock;
    bool isMenuRestore;
    int countTrayIcons;

    LMSensors *sensors;
    QList<KMenu *> menuList;
    QPointer<KSensorsCfg> sensorsCfg;
    QPointer<LMSensorsWidget> sensorsWidget;
    QPointer<LMSensorsAlarms> sensorsAlarm;
    QPointer<LMSensorDockPanel> statusNotifier;

    void createMenu(LMSensorDockPanel *trayIcon);
    void destroyMenu(LMSensorDockPanel *trayIcon);
    void createWidgets();
    void createDockWidgets();
    void createDockSensor(Sensor *sensor);
    void deleteDockSensor(LMSensorDockPanel *sensor);
    void createShowWidget(int desktop = 0);
    void createAlarmControl();

};

#endif
