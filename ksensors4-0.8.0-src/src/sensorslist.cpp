/***************************************************************************
                          sensorslist.cpp  -  part of KSensors4
                             -------------------
    begin                : mie abr 24 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "sensorslist.h"

#include <ksharedconfig.h>
#include <kconfiggroup.h>
#include <kglobal.h>

#include <Qt/qcoreevent.h>

SensorsList::SensorsList(QWidget *parent, const char *name)
        : QWidget(parent), 
        monitorized(false), 
        updateInterval(5000),
        tempScale(Sensor::dgCelsius), 
        timerId(-1)
{
    setObjectName(QString(name));
}

SensorsList::~SensorsList()
{
    if (timerId != -1) {
        killTimer(timerId);
    }
}

void SensorsList::setMonitorized(bool enable)
{
    if (monitorized != enable) {
        monitorized = enable;
        if (enable) {
            if (updateInterval <= 100 || updateInterval > 3600000) {
                updateInterval = 5000;
            }
            if (timerId == -1) {
                timerId = startTimer(updateInterval);
            }
        }
        else {
            if (timerId != -1) {
                killTimer(timerId);
                timerId = -1;
            }
        }
    }
}

void SensorsList::setUpdateInterval(int seconds)
{
    seconds = seconds * 1000;
    if (updateInterval != seconds) {
        updateInterval = seconds;
        if (monitorized) {
            setMonitorized(false);
            setMonitorized(true);
        }
    }
}

void SensorsList::setTempScale(Sensor::TempScale scale)
{
    if (tempScale != scale) {
        tempScale = scale;
        const QObjectList *sensors = getSensors();
        if (sensors && !sensors->isEmpty()) {
            QObjectList::const_iterator it;
            for (it = sensors->begin(); it != sensors->end(); ++it) {
                Sensor *sensor = static_cast<Sensor *> (*it);
                if (sensor->getType() == Sensor::lmTemp) {
                    emit sensor->configChanged();
                }
            }
        }
    }
}

void SensorsList::readConfig()
{
    KConfigGroup configGroup = KGlobal::config()->group(objectName());
    int seconds = configGroup.readEntry("UpdateInterval", this->getClass() == Sensor::hdSensor ? 15 : 5);
    setUpdateInterval(seconds);
    QString str = configGroup.readEntry("Scale", "C");
    Sensor::TempScale tempScale;
    if (str.compare("F") == 0) {
        tempScale = Sensor::dgFahrenheit;
    }
    else if (str.compare("K") == 0) {
        tempScale = Sensor::dgKelvin;
    }
    else {
        tempScale = Sensor::dgCelsius;
    }
    setTempScale(tempScale);
}

void SensorsList::writeConfig()
{
    KConfigGroup configGroup = KGlobal::config()->group(objectName());
    configGroup.writeEntry("UpdateInterval", updateInterval / 1000);
    QString scale;
    switch (tempScale) {
    case Sensor::dgCelsius:
        scale = 'C';
        break;
    case Sensor::dgKelvin:
        scale = 'K';
        break;
    case Sensor::dgFahrenheit:
        scale = 'F';
        break;
    case Sensor::dgDefault:
        scale = 'C';
        break;
    default:
        scale = 'C';
    }
    configGroup.writeEntry("Scale", scale);
    if (tempScale == Sensor::dgCelsius) {
        if (objectName().compare("hddtemp") == 0) {
            if (updateInterval == 15000) {
                configGroup.deleteGroup();
            }
        }
        else {
            if (updateInterval == 5000) {
                configGroup.deleteGroup();
            }
        }
    }
}

//**************************************************************************
// Protected methods
//**************************************************************************

void SensorsList::timerEvent(QTimerEvent *e)
{
    updateSensors();
    e->accept();
}

//***************

void SensorsList::childEvent(QChildEvent *e)
{
    if (e->polished()) {
        connect(static_cast<Sensor *> (e->child()), SIGNAL(configChanged()), this, SLOT(slotConfigChanged()));
        connect(static_cast<Sensor *> (e->child()), SIGNAL(valueChanged(double)), this, SLOT(slotValueChanged()));
    }
}

//**************************************************************************
// Protected slots
//**************************************************************************

void SensorsList::slotConfigChanged()
{
    emit configChanged(sender()->objectName().latin1());
}

void SensorsList::slotValueChanged()
{
    emit valueChanged(static_cast<Sensor *> (sender()));
}

/***************************************************************************/
