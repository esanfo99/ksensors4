/***************************************************************************
                          lmsensorsdock.cpp  -  part of KSensors4
                             -------------------
    begin                : Sun Sep 23 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensorsdock.h"

#include <kaboutapplicationdialog.h>
#include <kapplication.h>
#include <kcmdlineargs.h>
#include <kglobal.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <ktoolinvocation.h>
#include <kwindowsystem.h>

#include <Qt/qdesktopwidget.h>

#include <netwm.h>

/**
 * @param fNoDock : if true, hide the system tray icon
 * @param parent
 * @param name
 */
LMSensorsDock::LMSensorsDock(bool fNoDock, QWidget *parent, const char *name)
        : QWidget(parent),
        noDock(fNoDock),
        isMenuRestore(false),
        countTrayIcons(0),
        sensorsCfg(NULL),
        sensorsWidget(NULL),
        sensorsAlarm(NULL),
        statusNotifier(NULL)
{
    setObjectName(QString(name));
    sensors = new LMSensors(0, "lmsensors");
    sensors->setMonitorized(true);
    connect(sensors, SIGNAL(configChanged(const char *)), 
            this, SLOT(updateItemDock(const char *)));
    menuList.clear();
    createAlarmControl();
    createWidgets();
    if (!noDock) {
        createDockWidgets();
    }
}

LMSensorsDock::~LMSensorsDock()
{
    saveDesktopConfig();
    if (sensorsCfg) {
        sensorsCfg->hide();
        sensorsCfg->deleteLater();
    }

    const QObjectList *trayIcons = (&children());
    if (trayIcons && !trayIcons->isEmpty()) {
        QObjectList::const_iterator it;
        for (it = trayIcons->begin(); it != trayIcons->end(); ++it) {
            LMSensorDockPanel *trayIcon = static_cast<LMSensorDockPanel *> (*it);
            destroyMenu(trayIcon);
            trayIcon->deleteLater();
            --countTrayIcons;
        }
    }
    if (sensorsWidget) {
        sensorsWidget->hide();
        sensorsWidget->deleteLater();
    }

    if (sensors) {
        sensors->deleteLater();
    }
}

void LMSensorsDock::createMenu(LMSensorDockPanel *trayIcon)
{
    QString name = trayIcon->objectName();
    KMenu *menu = new KMenu();
    menuList.append(menu);
    menu->setObjectName(name + "_ksensors_menu");

    // create title
    menu->addTitle(QIcon(":/pics/ksensorsdocked.png"), QString("KSensors4"));
    //
    // create configure action
    QAction *configAction = new QAction(i18n("&Configure"), menu);
    configAction->setIcon(KIcon::fromTheme("configure", SmallIcon("configure")));
    configAction->setObjectName(name + "_configAction");
    menu->insertAction(0, configAction);
    connect(configAction, SIGNAL(triggered()), this, SLOT(createConfigWidget()));
    //
    // create about action
    QAction *aboutAction = new QAction(i18n("&About KSensors4"), menu);
    aboutAction->setIcon(KIcon::fromTheme("help-about", SmallIcon("help-about")));
    aboutAction->setObjectName(name + "_aboutAction");
    menu->insertAction(0, aboutAction);
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(showAbout()));
    //
    // create minimize action
    QAction *minimizeAction = new QAction(i18n("&Minimize"), menu);
    minimizeAction->setIcon(KIcon::fromTheme("window-minimize-symbolic", SmallIcon("window-minimize")));
    minimizeAction->setObjectName(name + "_minimizeAction");
    menu->insertAction(0, minimizeAction);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(minimizeRestoreWidget()));
    //
    // create restore action
    QAction *restoreAction = new QAction(i18n("&Restore"), menu);
    restoreAction->setIcon(KIcon::fromTheme("window-restore-symbolic", SmallIcon("window-restore")));
    restoreAction->setObjectName(name + "_restoreAction");
    menu->insertAction(0, restoreAction);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(minimizeRestoreWidget()));
    //
    if (!noDock) {
        if (sensorsWidget && sensorsWidget->isVisible()) {
            minimizeAction->setVisible(true);
            restoreAction->setVisible(false);
        }
        else {
            minimizeAction->setVisible(false);
            restoreAction->setVisible(true);
        }
    }
    //
    // create help action
    QAction *helpAction = new QAction(i18n("&Help"), menu);
    helpAction->setIcon(KIcon::fromTheme("help-contents", SmallIcon("help-contents")));
    helpAction->setObjectName(name + "_helpAction");
    menu->insertAction(0, helpAction);
    connect(helpAction, SIGNAL(triggered()), this, SLOT(showHelp()));
    //
    // create exit action
    QAction *exitAction = new QAction(i18n("&Exit"), menu);
    exitAction->setIcon(KIcon::fromTheme("application-exit", SmallIcon("application-exit")));
    exitAction->setObjectName(name + "_exitAction");
    menu->insertAction(0, exitAction);
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
    //
    // attach menu to tray icon
    trayIcon->setContextMenu(menu);
}

void LMSensorsDock::createWidgets()
{
    KConfigGroup configGroup = KGlobal::config()->group("General");
    int desktop = configGroup.readEntry<int>("desktop", 0);
    if (noDock) {
        createDockSensor(0);
        createShowWidget(desktop);
        sensorsWidget->show();
        updateMenu();
    }
    else if (configGroup.readEntry<bool>("showWidget", false)) {
        createShowWidget(desktop);
        sensorsWidget->show();
        updateMenu();
    }
}

void LMSensorsDock::saveDesktopConfig()
{
    KConfigGroup configGroup = KGlobal::config()->group("General");

    if (sensorsWidget && sensorsWidget->isVisible()) {
        configGroup.writeEntry("showWidget", true);
        NETWinInfo wm_client(QX11Info::display(), sensorsWidget->winId(),
                             x11Info().appRootWindow(),
                             NET::WMDesktop);

        configGroup.writeEntry<int>("desktop", wm_client.desktop());
    }
    else {
        configGroup.writeEntry("showWidget", false);
        configGroup.deleteEntry("desktop");
    }
    configGroup.sync();
}

void LMSensorsDock::createDockWidgets()
{
    const QObjectList *sensorGroups = sensors->getSensorsChips();
    countTrayIcons = 0;

    if (sensorGroups && !sensorGroups->isEmpty()) {
        QObjectList::const_iterator group_iter;
        for (group_iter = sensorGroups->begin(); group_iter != sensorGroups->end(); ++group_iter) {
            SensorsList *group = static_cast<SensorsList *> (*group_iter);
            const QObjectList *sensors = (group->getSensors());
            if (sensors && !sensors->isEmpty()) {
                QObjectList::const_iterator sensor_iter;
                for (sensor_iter = sensors->begin(); sensor_iter != sensors->end(); ++sensor_iter) {
                    LMSensor *sensor = static_cast<LMSensor *> (*sensor_iter);
                    if (LMSensorDockPanel::readShowInDock(sensor->objectName())) {
                        createDockSensor(sensor);
                    }
                }
            }
        }
    }

    if (countTrayIcons == 0) {
        createDockSensor(0);
    }
}

void LMSensorsDock::createAlarmControl()
{
    sensorsAlarm = new LMSensorsAlarms(sensors);
}

void LMSensorsDock::minimizeRestoreWidget()
{
    if (sensorsWidget) {
        if (sensorsWidget->isVisible()) {
            if (noDock) {
                sensorsWidget->showMinimized();
            }
            if (!getTrayIconCount()) {
                sensorsWidget->showMinimized();
            }
            else {
                sensorsWidget->hide();
            }
        }
        else {
            sensorsWidget->showNormal();
        }
        updateMenu();
    }
    else {
        createShowWidget();
        sensorsWidget->show();
        updateMenu();
    }
}

void LMSensorsDock::updateMenu()
{
    const QObjectList *trayIcons = &children();
    if (!trayIcons->isEmpty()) {
        QObjectList::const_iterator it;
        for (it = trayIcons->begin(); it != trayIcons->end(); ++it) {
            LMSensorDockPanel *trayIcon = static_cast<LMSensorDockPanel *> (*it);
            QString name = trayIcon->objectName();
            KMenu *menu = trayIcon->contextMenu();

            if (menu) {
                if (noDock || !sensorsWidget) {
                    menu->findChild<QAction *>(name + "_minimizeAction")->setVisible(false);
                    menu->findChild<QAction *>(name + "_restoreAction")->setVisible(false);
                    isMenuRestore = false;
                }
                else {
                    if (sensorsWidget->isVisible()) {
                        menu->findChild<QAction *>(name + "_minimizeAction")->setVisible(true);
                        menu->findChild<QAction *>(name + "_restoreAction")->setVisible(false);
                        isMenuRestore = false;
                    }
                    else {
                        menu->findChild<QAction *>(name + "_minimizeAction")->setVisible(false);
                        menu->findChild<QAction *>(name + "_restoreAction")->setVisible(true);
                        isMenuRestore = true;
                    }
                }
            }
        }
    }
}

void LMSensorsDock::createShowWidget(int desktop)
{
    if (!sensorsWidget) {
        sensorsWidget = new LMSensorsWidget(sensors, this, "sensorsWidget");
        connect(sensorsWidget, SIGNAL(rightMouseClicked(QMouseEvent *)), 
                this, SLOT(mouseEventReceived(QMouseEvent *)));
        connect(sensorsWidget, SIGNAL(destroyed()), this, SLOT(updateMenu()));
        updateMenu();
        sensorsWidget->installEventFilter(this);
        sensorsWidget->setWindowFlags(Qt::Tool);
    }
    if ((desktop != 0) && (desktop <= KWindowSystem::numberOfDesktops())) {
        KWindowSystem::setOnDesktop(sensorsWidget->winId(), desktop);
    }
}

void LMSensorsDock::createConfigWidget()
{
    if (!sensorsCfg) {
        sensorsCfg = new KSensorsCfg(sensors, sensorsAlarm, this, "sensorsCfg");
        // Debugging
        connect(sensorsCfg, SIGNAL(destroyed()), this, SLOT(slotConfigWidgetDestroyed()));
    }
    sensorsCfg->show();
}

/**
 * This function is called when the user selects Exit in the context menu.
 * We close sensorsAlarm first because, if an alarm sound is playing, then 
 * sensorsAlarm must gracefully stop and close the Phonon MediaObject.
 * 
 * @param e
 */
void LMSensorsDock::closeEvent(QCloseEvent *e)
{
    sensorsAlarm->close();
    this->deleteLater();
    e->accept();
}

void LMSensorsDock::mouseEventReceived(QMouseEvent *e)
{
    if (e->type() == QEvent::MouseButtonRelease) {
        if (e->button() == Qt::LeftButton) {
            minimizeRestoreWidget();
        }
        if (e->button() == Qt::RightButton) {
            if (!menuList.isEmpty()) {
                menuList.at(0)->popup(e->globalPos());
            }
        }
    }
}

/**
 * Create a system tray icon. If sensor is NULL, then system tray icon 
 * displays no sensor information. Otherwise, system tray icon displays 
 * sensor value.
 * 
 * @param sensor : a hardware sensor to monitor, or NULL
 */
void LMSensorsDock::createDockSensor(Sensor *sensor)
{
    if (countTrayIcons == 0) {
        const QObjectList *trayIcons = (&children());
        if (trayIcons && !trayIcons->isEmpty()) {
            QObjectList::const_iterator it;
            for (it = trayIcons->begin(); it != trayIcons->end(); ++it) {
                LMSensorDockPanel *trayIcon = static_cast<LMSensorDockPanel *> (*it);
                delete trayIcon;
            }
        }
    }

    QString name = NULL;
    if (sensor && !sensor->objectName().isNull()) {
        name = sensor->objectName();
    }

    if (!name.isNull()) {
        QObjectList trayIcons = children();
        if (!trayIcons.isEmpty()) {
            QObjectList::iterator it;
            for (it = trayIcons.begin(); it != trayIcons.end(); ++it) {
                if (name.compare((*it)->objectName()) == 0) {
                    return;
                }
            }
        }
    }

    LMSensorDockPanel *trayIcon = new LMSensorDockPanel(sensor, sensorsWidget, name.isNull() ? 0 : name.latin1());
    createMenu(trayIcon);

    if (!statusNotifier) {
        statusNotifier = trayIcon;
    }

    connect(trayIcon, SIGNAL(mouseEvent(QMouseEvent *)),
            this, SLOT(mouseEventReceived(QMouseEvent *)));
    trayIcon->setParent(this);

    if (!noDock) {
        if (trayIcon->sensor) {
            trayIcon->setValue(trayIcon->sensor->getValue());
        }
    }
    else {
        trayIcon->setStatus(KStatusNotifierItem::Passive);
    }
    connect(trayIcon, SIGNAL(activateRequested(bool, const QPoint &)),
            this, SLOT(iconActivated(bool, const QPoint &)));

    if (!name.isNull()) {
        ++countTrayIcons;
    }
}

void LMSensorsDock::deleteDockSensor(LMSensorDockPanel *trayIcon)
{
    destroyMenu(trayIcon);
    delete trayIcon;

    if (--countTrayIcons == 0) {
        createDockSensor(0);
    }
}

void LMSensorsDock::updateItemDock(const char *name)
{
    if (!name) {
        return;
    }

    Sensor *sensor = sensors->getSensor(name);

    bool fShow = LMSensorDockPanel::readShowInDock(name);
    LMSensorDockPanel *dockSensor = findChild<LMSensorDockPanel *>(QString(name));

    if (fShow != (bool)dockSensor) {
        if (dockSensor) {
            deleteDockSensor(dockSensor);
        }
        else {
            if (!noDock) {
                createDockSensor(sensor);
            }
        }
    }
    else {
        if (dockSensor) {
            dockSensor->updateConfig();
        }
    }
}

void LMSensorsDock::showAbout()
{
    KAboutApplicationDialog about(KCmdLineArgs::parsedArgs()->aboutData());
    about.setAttribute(Qt::WA_QuitOnClose, false);
    about.exec();
}

void LMSensorsDock::showHelp()
{
    QStringList args("help:/ksensors");
    QProcess process;
    if (!process.startDetached("khelpcenter", args)) {
        qWarning("KSensors4 LMSensorsDock: startDetached(\"khelpcenter\", \"help:/ksensors\") (unsuccessful)");
    }
    process.waitForStarted();
}

/**
 * Mouse left-button clicked on a system tray icon. If the sensors widget 
 * doesn't exist, then create it. If the sensors widget is active, then 
 * resize it to fit the visible sensor panels.
 * 
 * @param active : true if sensors widget is visible, false if hidden
 */
void LMSensorsDock::iconActivated(bool active, const QPoint &)
{
    if (!sensorsWidget) {
        createShowWidget();
        sensorsWidget->show();
        QObjectList trayIcons = children();
        if (!trayIcons.isEmpty()) {
            QObjectList::iterator it;
            for (it = trayIcons.begin(); it != trayIcons.end(); ++it) {
                LMSensorDockPanel *trayIcon = static_cast<LMSensorDockPanel *> (*it);
                trayIcon->setAssociatedWidget(sensorsWidget);
            }
        }
    }
    if (noDock) {
        Qt::WindowStates state = sensorsWidget->windowState();
        if (state & Qt::WindowMinimized || sensorsWidget->isHidden()) {
            sensorsWidget->showNormal();
            active = true;
        }
    }
    else {
        updateMenu();
    }

    // In most cases, the size of the sensors widget can be adjusted by 
    // calling adjustSize(). But if the sensors widget is maximized in an X
    // windows system display, then adjustSize() might not restore the 
    // sensors widget to its correct width. As a workaround, we calculate 
    // the perimeter of the rectangle enclosing the visible sensor panels
    // and set the width and height accordingly. If the sensors widget is 
    // empty, we resize it to its default size (4x3).
    if (active) {
        int x;
        int y;
        if (sensorsWidget->isEmpty()) {
            x = 4;
            y = 3;
        }
        else {
            sensorsWidget->adjustSize();
            sensorsWidget->getPerimeter(&x, &y);
        }
        sensorsWidget->setFixedWidth(x * sensorsWidget->sizeIncrement().width());
        sensorsWidget->setFixedHeight(y * sensorsWidget->sizeIncrement().height());
        sensorsWidget->setFixedWidth(QWIDGETSIZE_MAX);
        sensorsWidget->setFixedHeight(QWIDGETSIZE_MAX);
    }
    else {
        sensorsWidget->cfgWriteGeometry();
    }
    saveDesktopConfig();
}

void LMSensorsDock::destroyMenu(LMSensorDockPanel *trayIcon)
{
    KMenu *menu = trayIcon->contextMenu();
    menuList.removeOne(menu);
    QList<QAction *> actions = menu->actions();
    QList<QAction *>::iterator it;
    for (it = actions.begin(); it != actions.end(); ++it) {
        (*it)->deleteLater();
    }
    menu->deleteLater();
}

// Debugging
void LMSensorsDock::slotConfigWidgetDestroyed()
{
}

/**
 * This function serves three purposes: hide the sensors widget if it 
 * becomes minimized on the task bar, ensure that the system tray icon 
 * is visible when sensors widget becomes hidden, and (if the user started
 * ksensors with the --nodock option) hide the system tray icon when the 
 * sensors widget becomes visible.
 * 
 * @param o : pointer to object in which event occurred
 * @param e : pointer to event
 * @return false
 */
bool LMSensorsDock::eventFilter(QObject *o, QEvent *e)
{
    if (e->type() == QEvent::Hide) {
        if (o->objectName().compare("sensorsWidget") == 0) {
            if (noDock) {
                statusNotifier->setStatus(KStatusNotifierItem::Active);
            }
            else {
                if (!isMenuRestore) {
                    updateMenu();
                }
            }
        }
    }
    else if (e->type() == QEvent::Show) {
        if (o->objectName().compare("sensorsWidget") == 0) {
            if (noDock) {
                statusNotifier->setStatus(KStatusNotifierItem::Passive);
            }
        }
    }
    else if (e->type() == QEvent::WindowStateChange) {
        if (o->objectName().compare("sensorsWidget") == 0) {
            if (sensorsWidget->windowState() & Qt::WindowMinimized) {
                sensorsWidget->hide();
                if (!noDock) {
                    updateMenu();
                }
            }
        }
    }
    return false;
}
