/***************************************************************************
                          lmsensorschip.h  -  part of KSensors4
                             -------------------
    begin                : Wed Feb 20 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORSCHIP_H
#define LMSENSORSCHIP_H

#include "lmsensor.h"
#include "sensorslist.h"

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Sensors list for a libsensors chip, inherits SensorsList
 */

class LMSensorsChip : public SensorsList {
    Q_OBJECT

    friend class LMSensor;

public:

    LMSensorsChip(const sensors_chip_name *chip = 0, QWidget *parent = 0, const char *name = 0);
    virtual ~LMSensorsChip();

    QString getSensorGroupName() override;

public slots:

    void updateSensors() override;

protected:

    const sensors_chip_name *getChipName();

private:

    const sensors_chip_name *chip_name;
    bool initSensorsChip();
    void createSensors();
};

#endif
