/***************************************************************************
                          lmsensorsalarms.cpp  -  part of KSensors4
                             -------------------
    begin                : Wed Nov 14 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensorsalarms.h"
#include "phononplayer.h"

#include <kprocess.h>
#include <kconfiggroup.h>
#include <ksharedconfig.h>
#include <kstandarddirs.h>

#include <Qt/qdir.h>

LMSensorsAlarms::LMSensorsAlarms(LMSensors *lsensors, QWidget *parent) 
        : QObject(parent), 
        timerId(-1), 
        alarmInterval(3000), 
        sensorIndex(0),
        alarmSound("ksensors_alert.ogg"), 
        phononPlayer(NULL), 
        sensors(lsensors)
{
    setObjectName("sensorsAlarm");
    alarmTimer = new QTimer(this);
    // Debugging
    sensors->installEventFilter(this);
    connect(sensors, SIGNAL(valueChanged(Sensor *)), this, SLOT(setValueChanged(Sensor *)));
    connect(sensors, SIGNAL(configChanged(const char *)), this, SLOT(slotConfigChanged(const char *)));
    connect(alarmTimer, SIGNAL(timeout()), this, SLOT(slotTimeout()));
    initializeAlarms(sensors);
}

LMSensorsAlarms::~LMSensorsAlarms()
{
    delete alarmTimer;
}

void LMSensorsAlarms::close()
{
    disconnect(alarmTimer, SIGNAL(timeout()), this, SLOT(slotTimeout()));
    timerStop();
    if (phononPlayer) {
        phononPlayer->close();
    }
    this->deleteLater();
}

//##########################################################################

/**
 * 
 * @param name : name of sensor
 * @param value : sensor value
 * @param cmd : alarm command to run
 */
void LMSensorsAlarms::runSensorCommand(const QString &name, const QString &value,
                                       const QString &cmd)
{
    QString executable;
    QStringList paramList;

    if (QString(cmd).toUpper().compare("LOGGER") == 0) {
        executable = "logger";
        paramList.append(QString("KSensors4 alert: name(") + name + ") value(" + value + ")");
        runCommand(executable, paramList);
        return;
    }

    setenv("SENSOR_NAME", name.latin1(), 1);
    setenv("SENSOR_VALUE", value.latin1(), 1);

    int spaceOffset = cmd.indexOf(" ");
    if (spaceOffset == -1) {
        executable = cmd;
        paramList.clear();
    }
    else {
        executable = cmd.left(spaceOffset);
        int paramLength = cmd.length() - spaceOffset - 1;
        QString paramString = cmd.right(paramLength);
        paramString.replace("$SENSOR_NAME", name);
        paramString.replace("SENSOR_NAME", name);
        paramString.replace("$SENSOR_VALUE", value);
        paramString.replace("SENSOR_VALUE", value);
        paramList.append(paramString);
    }
    runCommand(executable, paramList);
}

void LMSensorsAlarms::runCommand(const QString &executable, const QStringList &params)
{
    ProcessExec proc;
    proc.setProgram(executable, params);
    int status = proc.run();

    proc.outInfo(executable);

    if (status || proc.outputErrors()) {
        proc.errInfo(executable, status);
    }
}

void LMSensorsAlarms::playSound(const QString &sound, bool isTest)
{
    QString soundPath = getSoundPath(sound);
    if (soundPath.isNull()) {
        return;
    }
    if (!phononPlayer) {
        phononPlayer = new PhononPlayer(this);
    }
    phononPlayer->playTimedSound(soundPath, alarmInterval);

    if (isTest) {
        qWarning("KSensors4 PhononPlayer test results:\n%s", 
                 phononPlayer->toString().latin1());
    }
}

//##########################################################################

/**
 * If a sensor value enters an alarm zone, then run an alarm command, play
 * an alarm sound, or do nothing, depending on the user's configuration
 * settings.
 * 
 * @param sensor : a motherboard sensor or a hard drive sensor
 */
void LMSensorsAlarms::setValueChanged(Sensor *sensor)
{
    if (sensor->getAlarm()) {
        if (!sensor->getAlarmOn()) {
            switch (readAlarm(sensor->objectName())) {
            case acNothing:
                break;
            case acSound:
                sensor->setAlarmOn(true);
                if (audioList.isEmpty() && timerId == -1) {
                    timerStart();
                }
                audioList.append(sensor);
                alarmSound = readAlarmSound(sensor->objectName());
                break;
            case acCommand:
                runSensorCommand(static_cast<const char *> (sensor->getDescription()),
                                 static_cast<const char *> (QString::number(sensor->getValue())),
                                 static_cast<const char *> (readAlarmCommand(sensor->objectName())));
                break;
            default:
                break;
            }
        }
    }
    else {
        if (sensor->getAlarmOn()) {
            if (readAlarm(sensor->objectName()) == acSound) {
                if (!audioList.empty()) {
                    audioList.removeOne(sensor);
                }
                if (audioList.empty()) {
                    timerStop();
                    alarmSound = "ksensors_alert.ogg";
                }
                else {
                    alarmSound = readAlarmSound(audioList.at(audioList.size() - 1)->objectName());
                }
            }
            sensor->setAlarmOn(false);
        }
    }
}

/**
 * If the user changes alarm sound settings in the configuration dialog, 
 * then start or stop the alarm timer and update alarm sounds for the 
 * affected sensor.
 * 
 * @param name : name of sensor affected by change in configuration settings.
 */
void LMSensorsAlarms::slotConfigChanged(const char *name)
{
    Sensor *sensor = sensors->getSensor(name);
    if (sensor) {
        if (sensor->getAlarm()) {
            switch (readAlarm(sensor->objectName())) {
            case acSound:
                if (!audioList.contains(sensor)) {
                    if (audioList.isEmpty() && timerId == -1) {
                        timerStart();
                    }
                    audioList.append(sensor);
                    sensor->setAlarmOn(true);
                    alarmSound = readAlarmSound(sensor->objectName());
                }
                break;
            default:
                if (audioList.contains(sensor)) {
                    audioList.removeOne(sensor);
                    sensor->setAlarmOn(false);
                    if (audioList.empty()) {
                        timerStop();
                        alarmSound = "ksensors_alert.ogg";
                    }
                }
                break;
            }
        }
        else {
            switch (readAlarm(sensor->objectName())) {
            case acSound:
                if (audioList.contains(sensor)) {
                    audioList.removeOne(sensor);
                    sensor->setAlarmOn(false);
                    if (audioList.empty()) {
                        timerStop();
                        alarmSound = "ksensors_alert.ogg";
                    }
                }
                break;
            default:
                break;
            }
        }
    }
}

/**
 * Sensors may enter and leave an alarm zone. If the user has selected 
 * different alarm sounds for different sensors, and two or more sensors 
 * enter an alarm zone, then alternate between alarm sounds.
 * 
 */
void LMSensorsAlarms::slotTimeout()
{
    if (!audioList.isEmpty()) {
        ++sensorIndex;
        if (sensorIndex >= static_cast<unsigned> (audioList.size())) {
            sensorIndex = 0;
        }
        Sensor *sensor = audioList.at(sensorIndex);
        QString nextAlarmSound = readAlarmSound(sensor->objectName());
        if (nextAlarmSound.compare(alarmSound) != 0) {
            alarmSound = nextAlarmSound;
        }
    }
    playSound(alarmSound);
}

//##########################################################################

LMSensorsAlarms::Actions LMSensorsAlarms::readAlarm(const QString & name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    return static_cast<Actions> (configGroup.readEntry<int>("Alarm", acNothing));
}

void LMSensorsAlarms::writeAlarm(const QString &name, Actions alarm)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    configGroup.writeEntry<int>("Alarm", alarm);
}

QString LMSensorsAlarms::readAlarmCommand(const QString & name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    return configGroup.readEntry("AlarmCommand", "logger");
}

void LMSensorsAlarms::writeAlarmCommand(const QString &name, const QString & cmd)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    if (cmd.trimmed().isEmpty()) {
        configGroup.deleteEntry("AlarmCommand");
    }
    else {
        configGroup.writeEntry("AlarmCommand", cmd);
    }
}

QString LMSensorsAlarms::readAlarmSound(const QString & name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    QString alarmSound = configGroup.readEntry("AlarmSound", "ksensors_alert.ogg");
    return alarmSound;
}

void LMSensorsAlarms::writeAlarmSound(const QString &name, const QString & sound)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    if (sound.trimmed().isEmpty()) {
        configGroup.deleteEntry("AlarmSound");
    }
    else {
        configGroup.writeEntry("AlarmSound", sound);
    }
}

// **************************** private methods ****************************

void LMSensorsAlarms::timerStart()
{
    if (alarmTimer) {
        alarmTimer->start(alarmInterval);
        timerId = alarmTimer->timerId();
    }
}

void LMSensorsAlarms::timerStop()
{

    alarmTimer->stop();
    timerId = -1;
}

/**
 * Search for sound in the apps/sounds directory. If not found, then search
 * the 'kde4-config --path sound' directories. If not found, test for 
 * fully-qualified path to a custom sound file.
 * 
 * @param sound : name of (or path to) a sound file
 * @return fully-qualified path to sound file
 */
QString LMSensorsAlarms::getSoundPath(const QString &sound)
{
    QString soundFile = sound.trimmed();

    if (soundFile.isEmpty()) {
        qWarning("KSensors4 LMSensorsAlarms Error: No sound specified!");
        return NULL;
    }

    QString soundPath(KStandardDirs::locate("data", QString("sounds/") + soundFile));

    if (!KStandardDirs::exists(soundPath)) {
        soundPath = KStandardDirs::locate("sound", soundFile);
    }

    if (!KStandardDirs::exists(soundPath)) {
        soundPath = soundFile;
    }

    if (!KStandardDirs::exists(soundPath)) {
        qWarning("KSensors4 LMSensorsAlarms Error: Unable to locate sound file: [%s]",
                 soundFile.latin1());
        return NULL;
    }

    return soundPath;
}

/**
 * If a sensor is in an alarm zone when ksensors starts, then an alarm won't
 * be triggered until the sensor value changes and a timer event occurs. CPU 
 * and GPU values change often, but hdd temperatures might not change. This 
 * triggers an alarm and starts the timer if a sensor is already in an alarm
 * zone when ksensors starts. 
 * 
 * @param sensors
 */
void LMSensorsAlarms::initializeAlarms(LMSensors *sensors)
{
    const QObjectList *sensorGroups = sensors->getSensorsChips();
    if (sensorGroups && !sensorGroups->isEmpty()) {
        QObjectList::const_iterator group_iter;
        for (group_iter = sensorGroups->begin(); group_iter != sensorGroups->end(); ++group_iter) {
            SensorsList *group = static_cast<SensorsList *> (*group_iter);
            const QObjectList *sensors = group->getSensors();
            if (sensors && !sensors->isEmpty()) {
                QObjectList::const_iterator sensor_iter;
                for (sensor_iter = sensors->begin(); sensor_iter != sensors->end(); ++sensor_iter) {
                    LMSensor *sensor = static_cast<LMSensor *> (*sensor_iter);
                    setValueChanged(sensor);
                }
            }
        }
    }
}

// Debugging
bool LMSensorsAlarms::eventFilter(QObject *, QEvent *)
{
    return false;
}
