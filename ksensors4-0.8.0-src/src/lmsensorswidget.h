/***************************************************************************
                          lmsensorswidget.h  -  part of KSensors4
                             -------------------
    begin                : Mon Aug 6 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORSWIDGET_H
#define LMSENSORSWIDGET_H

#include "lmsensors.h"
#include "lmsensorpanel.h"
#include "infopanels.h"

#include <kiconloader.h>
#include <kconfiggroup.h>

#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Main window - parent of Panel widgets, associated widget of 
 * LMSensorDockPanel
 */

class LMSensorsWidget : public QWidget {
    Q_OBJECT

public:

    LMSensorsWidget(LMSensors *lsensors, QWidget *parent = 0, const char *name = 0);
    virtual ~LMSensorsWidget();

    static bool cfgReadPalette(QPalette &pal, const QString &name, bool getDefault = false);
    static void cfgWritePalette(const QPalette &pal, const QString &name);
    static void cfgUnsetPalette(const QString &name);
    static void getDefaultPalette(QPalette &pal);
    static void cfgWritePanelSize(int newSize);
    static int cfgReadPanelSize();
    
    bool isEmpty() {
        return children().size() == 0;
    }
    void getPerimeter(int *px, int *py);
    void cfgWriteGeometry();

signals:

    void rightMouseClicked(QMouseEvent *e);

protected slots:

    void slotConfigChanged(const char *name);
    void panelsGroupDestroyed();

private:

    LMSensors *sensors;
    InfoPanels *infoPanels;
    QWidget *childDraging;
    int panelsSize;
    QPoint origMousePt;

    void loadPalette();
    void loadPalette(Panel *panel);
    void loadPanelConfig(Panel *panel);
    void savePanelConfig(Panel *panel);
    void mouseReleaseEvent(QMouseEvent *m) override;
    void mouseMoveEvent(QMouseEvent *m) override;

    void createSensorPanels();
    void findUnusedPosition(Panel *newPanel, int *px, int *py);
    void startDragChild(QMouseEvent *m, QWidget *w);
    void endDragChild();
    void resizePanels(int newSize);
    void loadGeneralOptions();
  
private slots:

    void childEvent(QChildEvent *e) override;
    void childEventReceived(QEvent *e);
};

#endif
