/***************************************************************************
                          qbarmeter.cpp  -  part of KSensors4
                             -------------------
    begin                : Mon Dec 3 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "qbarmeter.h"

#include <Qt/qstring.h>

QBarMeter::QBarMeter(QWidget *parent, const char *name)
        : QWidget(parent),
        useValMax(true),
        valMax(100),
        maxColor(Qt::green),
        count(1),
        steps(10),
        dir(dirUp)
{
    std::fill(val, val + 8, 0.0);
    std::fill(color, color + 8, Qt::red);
    setObjectName(QString(name));
}

QBarMeter::~QBarMeter()
{
}

void QBarMeter::setDirection(Direction newDir)
{
    dir = newDir;
    update();
}

void QBarMeter::useValueMax(bool flag)
{
    if (useValMax != flag) {
        useValMax = flag;
        update();
    }
}

void QBarMeter::setValueMax(double newVal)
{
    if (valMax != newVal) {
        valMax = newVal;
        update();
    }
}

void QBarMeter::setValueMaxColor(const QColor &newColor)
{
    maxColor = newColor;
    update();
}

void QBarMeter::setValue(double newVal)
{
    if (val[0] != newVal) {
        val[0] = newVal;
        update();
    }
}

void QBarMeter::setValueColor(const QColor &newColor)
{
    color[0] = newColor;
    update();
}

void QBarMeter::setValue(int index, double newVal)
{
    if (val[index] != newVal) {
        val[index] = newVal;
        update();
    }
}

void QBarMeter::setValueColor(int index, const QColor &newColor)
{
    if (color[index] != newColor) {
        color[index] = newColor;
        update();
    }
}

void QBarMeter::setValueCount(int newCount)
{
    if (count != newCount) {
        count = newCount;
        update();
    }
}

void QBarMeter::setSteps(int newSteps)
{
    steps = newSteps;
    update();
}

void QBarMeter::paintEvent(QPaintEvent *e)
{
    int x, y, lx, ly, sx, sy;
    switch (dir) {
    case dirUp:
        sx = 0;
        sy = -height() / steps;
        lx = width();
        ly = -(sy * 3) / 4;
        x = 0;
        y = height() - ly;
        break;
    case dirDown:
        sx = 0;
        sy = height() / steps;
        lx = width();
        ly = (sy * 3) / 4;
        x = 0;
        y = 0;
        break;
    case dirLeft:
        sx = -width() / steps;
        sy = 0;
        lx = -(sx * 3) / 4;
        ly = height();
        x = width() - lx;
        y = 0;
        break;
    case dirRight:
        sx = width() / steps;
        sy = 0;
        lx = (sx * 3) / 4;
        ly = height();
        x = 0;
        y = 0;
        break;
    default:
        return;
    }

    if (!useValMax) {
        maxColor = color[count - 1];
        valMax = 0.0;
        for (int i = 0; i < count; i++) {
            valMax += val[i];
        }
    }

    int csteps = steps;
    QPainter p(this);

    for (int i = 0; i < count; i++) {
        int v = (valMax != 0.0) ? static_cast<int> (((val[i] * steps) / valMax) + 0.5) : 0;
        if (v == 0 && csteps > 0 && val[i] > 0) {
            v++;
        }
        csteps = (v <= csteps) ? (csteps - v) : 0;
        while (v-- > 0 && x >= 0 && y >= 0) {
            p.fillRect(x, y, lx, ly, color[i]);
            x += sx;
            y += sy;
        }
    }

    if (csteps > 0) {
        do {
            p.fillRect(x, y, lx, ly, maxColor);
            x += sx;
            y += sy;
        }
        while (--csteps > 0);
    }

    e->accept();
}
