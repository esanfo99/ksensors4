/***************************************************************************
                          infopanels.cpp  -  part of KSensors4
                             -------------------
    begin                : vie may 10 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "infopanels.h"
#include "lmsensorswidget.h"

#include <ksharedconfig.h>
#include <kconfiggroup.h>
#include <kglobal.h>

#include <Qt/qtimer.h>

InfoPanels::InfoPanels(QWidget *parent) : PanelsGroup(/*this*/), timerPanelCount(0),
sensorsWidget(parent) {
    timer = new QTimer(this);
    configChanged("proc.CPUINFO");
    configChanged("proc.RAMINFO");
    configChanged("proc.SWAPINFO");
    configChanged("proc.CPULOAD");
    configChanged("proc.UPTIME");
}

InfoPanels::~InfoPanels() {
    for (int i = panelList.size() - 1; i >= 0; --i) {
        disconnect(timer, SIGNAL(timeout()), panelList.at(i), SLOT(updateInfo()));
        panelList.at(i)->deleteLater();
    }
    delete timer;
}

/**
 * SystemCfg::writePreferencesInfo() calls this with name argument "proc". 
 * 
 * @param name : the name of a system information panel or "proc".
 */
void InfoPanels::configChanged(const char *name) {
    if (!strcmp(name, "proc")) {
        readUpdateInterval();
    } else {
        Panel *panel = sensorsWidget->findChild<Panel *>(name);
        if (Panel::readShow(name) != (bool)panel) {
            if (panel) {
                delete panel;
                panelList.removeOne(panel);
            } else {
                if (!strcmp(name, "proc.CPUINFO")) {
                    panel = new CpuPanel(sensorsWidget, name);
                    panelList.append(panel);
                } else if (!strcmp(name, "proc.RAMINFO")) {
                    panel = new RamPanel(sensorsWidget, name, RamPanel::memRAM);
                    panelList.append(panel);
                } else if (!strcmp(name, "proc.SWAPINFO")) {
                    panel = new RamPanel(sensorsWidget, name, RamPanel::memSWAP);
                    panelList.append(panel);
                } else if (!strcmp(name, "proc.CPULOAD")) {
                    panel = new CpuTimePanel(sensorsWidget, name);
                    panelList.append(panel);
                } else if (!strcmp(name, "proc.UPTIME")) {
                    panel = new UpTimePanel(sensorsWidget, name);
                    panelList.append(panel);
                }
                if (panel) {
                    timerConnect(panel);
                }
            }
        } else {
            if (panel) {
                QPalette pal;
                if (LMSensorsWidget::cfgReadPalette(pal, panel->objectName(), false)) {
                    panel->setPalette(pal);
                } else {
                    if (LMSensorsWidget::cfgReadPalette(pal, 0, false)) {
                        panel->setPalette(pal);
                    } else {
                        panel->setPalette(QPalette());
                    }
                }
            }
        }
    }
}

void InfoPanels::timerConnect(Panel *panel) {
    if (timerPanelCount == 0) {
        timer->start(cfgReadUpdateInterval()*1000);
    }
    connect(timer, SIGNAL(timeout()), panel, SLOT(updateInfo()));
    connect(panel, SIGNAL(destroyed()), this, SLOT(infoPanelDestroyed()));
    timerPanelCount++;
}

void InfoPanels::infoPanelDestroyed() {
    if (--timerPanelCount <= 0) {
        timer->stop();
    }
}

void InfoPanels::cfgWriteUpdateInterval(int interval) {
    KConfigGroup configGroup = KGlobal::config()->group("SensorsWidget");
    configGroup.writeEntry("UpdateInterval", interval);
}

int InfoPanels::cfgReadUpdateInterval() {
    KConfigGroup configGroup = KGlobal::config()->group("SensorsWidget");
    return configGroup.readEntry<int>("UpdateInterval", 5);
}

void InfoPanels::readUpdateInterval() {
    timer->setInterval(cfgReadUpdateInterval() * 1000);
}

// ******************* Public static methods **********************
