/***************************************************************************
                          lmsensors.cpp  -  part of KSensors4
                             -------------------
    begin                : Mon Aug 6 2001
    copyright            : (C) 2001 by Miguel Novas
    email                :
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensors.h"

#include <Qt/qcoreevent.h>

#include <stdio.h>

//****************************************************************************
// Public methods
//****************************************************************************

LMSensors::LMSensors(QWidget *parent, const char *name) : QWidget(parent)
{
    setObjectName(QString(name));

    if (initSensors()) {
        createLMSensors();
    }
    createI8KSensors();
    createHDSensors();

    // Debugging
    const QObjectList *sensorLists = (&children());
    if (sensorLists && !sensorLists->isEmpty()) {
        QObjectList::const_iterator it;
        for (it = sensorLists->begin(); it != sensorLists->end(); ++it) {
            LMSensor *sensor = static_cast<LMSensor *> (*it);
            connect(sensor, SIGNAL(valueChanged(Sensor *)), this, SLOT(setValueChanged(Sensor *)));
        }
    }
}

LMSensors::~LMSensors()
{
    if (count()) {
        sensors_cleanup();
    }
}

bool LMSensors::initSensors()
{
#if SENSORS_API_VERSION < 0x400 /* libsensor 3 code */
    FILE *fp = fopen("/etc/sensors.conf", "r");
    if (!fp) {
        qWarning("KSensors4 LMSensors Error: /etc/sensors.conf not found !");
        return false;
    }
#else
    FILE *fp = NULL;
#endif
    int err = sensors_init(fp);
    if (err) {
        qWarning("KSensors4 LMSensors Error: sensors_init fail, error code %d", err);
        return false;
    }
#if SENSORS_API_VERSION < 0x400 /* libsensor 3 code */
    fclose(fp);
#endif
    return true;
}

void LMSensors::createLMSensors()
{
    const sensors_chip_name *chip_name;
    int err = 0;
#if SENSORS_API_VERSION < 0x400 /* libsensor 3 code */
    while ((chip_name = sensors_get_detected_chips(&err))) {
        if (existSensor(chip_name, "temp") ||
            existSensor(chip_name, "fan")) {
            (void) new LMSensorsChip(chip_name, this, "lmsensors");
        }
    }
#else
    while ((chip_name = sensors_get_detected_chips(NULL, &err))) {
        (void) new LMSensorsChip(chip_name, this, "lmsensors");
    }
#endif
}

void LMSensors::createHDSensors()
{
    HDSensorsList *disks = new HDSensorsList(this, "hddtemp");
    if (disks->count() == 0) {
        delete disks;
    }
}

void LMSensors::createI8KSensors()
{
    if (I8KSensorsList::I8KAvailable()) {
        (void) new I8KSensorsList(this, "i8k");
    }
}


#if SENSORS_API_VERSION < 0x400 /* libsensor 3 code */

int LMSensors::existSensor(const sensors_chip_name *chip_name, const char *sensor_name)
{
    int nr1, nr2;
    const sensors_feature_data *sensor_data;

    nr1 = nr2 = 0;
    while ((sensor_data = sensors_get_all_features(*chip_name, &nr1, &nr2))) {
        if (strstr(sensor_data->name, sensor_name)) {
            return sensor_data->number;
        }
    }
    return 0;
}
#endif

void LMSensors::setMonitorized(bool enable)
{
    const QObjectList *chips = getSensorsChips();
    if (chips && !chips->isEmpty()) {
        QObjectList::const_iterator it;
        for (it = chips->begin(); it != chips->end(); ++it) {
            (static_cast<LMSensorsChip *> (*it))->setMonitorized(enable);
        }
    }
}

Sensor *LMSensors::getSensor(const char *name)
{
    int index = count();
    Sensor *sensor = 0;
    while (--index >= 0 && !sensor) {
        sensor = getSensorsChip(index)->getSensor(name);
    }
    return sensor;
}

void LMSensors::childEvent(QChildEvent *e)
{
    if (e->polished()) {
        connect(static_cast<SensorsList *> (e->child()), SIGNAL(valueChanged(Sensor *)), this, SIGNAL(valueChanged(Sensor *)));
        connect(static_cast<SensorsList *> (e->child()), SIGNAL(configChanged(const char *)), this, SIGNAL(configChanged(const char *)));
    }
}

// Debugging
void LMSensors::setValueChanged(Sensor *)
{
}
/*********************************************************************************/
