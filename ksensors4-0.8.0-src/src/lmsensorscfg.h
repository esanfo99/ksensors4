/***************************************************************************
                          lmsensorscfg.h  -  part of KSensors4
                             -------------------
    begin                : Mon Aug 13 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORSCFG_H
#define LMSENSORSCFG_H

#include "lmsensorsalarms.h"
#include "lmsensorscfgdesign_ui.h"
#include "palettecfg.h"
#include "sensorslist.h"

#include <kpagewidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Configuration page (lm-sensors, hddtemp, i8k), controlled by KSensorsCfg
 */

class LMSensorsCfg : public LMSensorsCfgDesign {
    Q_OBJECT

public:
    LMSensorsCfg(SensorsList *lsensors, LMSensorsAlarms *sensorsAlarms,
            QWidget *parent = 0, const char *name = 0);
    virtual ~LMSensorsCfg();

public slots:
    void slotApplyChanges();

protected:

    void applySensorChanges();
    void applyPreferencesChanges();
    void applySensorAlarmConfig(Sensor *sensor);

    void readSensorInfo(int index);
    void readPreferencesInfo();
    void readSensorsList();

    // Debugging
    bool eventFilter(QObject *o, QEvent *e) override;

protected slots:

    void slotAccept();
    void slotTestAlarm();
    void slotCurrentSensorChanged(int listIndex);
    void slotMainTabPageChanged(QWidget *);
    void slotComboMinMaxSelected(int comboIndex);

private:

    QPointer<PaletteCfg> palPanelCfg;
    SensorsList *sensors;
    QPointer<Sensor> curSensor;
    QPointer<LMSensorsAlarms> alarms;
};

#endif
