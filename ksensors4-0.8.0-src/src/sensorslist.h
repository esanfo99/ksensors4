/***************************************************************************
                          sensorslist.h  -  part of KSensors4
                             -------------------
    begin                : mie abr 24 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SENSORSLIST_H
#define SENSORSLIST_H

#include "sensor.h"

#include <Qt/qglobal.h>
#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Common functions for derived classes LMSensorsChip, HDSensorsList, I8KSensorsList
 */

class SensorsList : public QWidget {
    Q_OBJECT

    friend class Sensor;

public:

    SensorsList(QWidget *parent = 0, const char *name = 0);
    virtual ~SensorsList();

    void setMonitorized(bool enable);

    const QString &getDescription() {
        return description;
    };

    const QObjectList *getSensors() {
        if (children().empty()) {
            return NULL;
        }
        return  (&children());
    };

    Sensor *getSensor(int index) {
        if (!children().empty() && (index >= 0) && (index < children().size())) {
            return static_cast<Sensor *> (( (&children()))->at(index));
        }
        return NULL;
    }

    Sensor *getSensor(const char *name) {
        return findChild<Sensor *>(QString(name));
    }

    int count() {
        return (&children() != NULL) ? children().count() : 0;
    }

    void setTempScale(Sensor::TempScale scale);

    Sensor::TempScale getTempScale() {
        return tempScale;
    }

    void setUpdateInterval(int seconds);

    int getUpdateInterval() {
        return updateInterval / 1000;
    }

    Sensor::SensorClass getClass() {
        return sensorClass;
    };

    virtual QString getSensorGroupName() {
        return QString("unknown");
    }

public slots:

    virtual void updateSensors() = 0;
    virtual void readConfig();
    virtual void writeConfig();

signals:
    void valueChanged(Sensor *);
    void configChanged(const char *name);

protected slots:

    void slotConfigChanged();
    void slotValueChanged();

protected:

    void setDescription(const QString &name) {
        description = name;
    }

    void setClass(Sensor::SensorClass newClass) {
        sensorClass = newClass;
    }

    void childEvent(QChildEvent *e) override;

private:

    QString description;
    Sensor::SensorClass sensorClass;
    bool monitorized;
    int updateInterval;
    Sensor::TempScale tempScale;
    int timerId;
    
    void timerEvent(QTimerEvent *e) override;
};

#endif
