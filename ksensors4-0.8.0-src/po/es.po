# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <michaell@teleline.es>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: ksensors 0.7.3\n"
"Report-Msgid-Bugs-To: s@s.org\n"
"POT-Creation-Date: 2018-08-30 16:04-0400\n"
"PO-Revision-Date: 2002-05-21 00:45+0200\n"
"Last-Translator: Miguel Novas <michaell@teleline.es>\n"
"Language-Team: Spanish <es@li.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 0.9.6\n"

#: lmsensorsdock.cpp:111
#, fuzzy
msgid "&About KSensors4"
msgstr "&Acerca de KSensors"

#: lmsensorsdock.cpp:104
msgid "&Configure"
msgstr ""

#: lmsensorsdock.cpp:150
msgid "&Exit"
msgstr "&Finalizar"

#: lmsensorsdock.cpp:143
msgid "&Help"
msgstr ""

#: lmsensorsdock.cpp:118
msgid "&Minimize"
msgstr "&Minimizar"

#: lmsensorsdock.cpp:125
msgid "&Restore"
msgstr "&Restaurar"

#: main.cpp:45
msgid ""
"(c) 2001, Miguel Novas\n"
"(c) 2018, Eric Sanford"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:219
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:43
#, no-c-format
msgid "+1%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:244
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:63
#, no-c-format
msgid "+10%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:249
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:67
#, no-c-format
msgid "+15%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:224
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:47
#, no-c-format
msgid "+2%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:254
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:71
#, no-c-format
msgid "+20%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:229
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:51
#, no-c-format
msgid "+3%"
msgstr ""

#. i18n: file: palettecfgdesign.ui:350
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:208
msgid "+3.5V"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:259
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:75
#, no-c-format
msgid "+30%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:234
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:55
#, no-c-format
msgid "+4%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:264
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:79
#, no-c-format
msgid "+40%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:239
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:59
#, no-c-format
msgid "+5%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:269
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:83
#, no-c-format
msgid "+50%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:335
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:99
#, no-c-format
msgid "-1%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:360
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:119
#, no-c-format
msgid "-10%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:365
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:123
#, no-c-format
msgid "-15%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:340
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:103
#, no-c-format
msgid "-2%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:370
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:127
#, no-c-format
msgid "-20%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:345
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:107
#, no-c-format
msgid "-3%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:375
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:131
#, no-c-format
msgid "-30%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:350
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:111
#, no-c-format
msgid "-4%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:380
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:135
#, no-c-format
msgid "-40%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:355
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:115
#, no-c-format
msgid "-5%"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:385
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:139
#, no-c-format
msgid "-50%"
msgstr ""

#. i18n: file: generalcfgdesign.ui:74
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:12
msgid "48 x 48"
msgstr ""

#. i18n: file: generalcfgdesign.ui:66
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:9
msgid "56 x 56"
msgstr ""

#. i18n: file: generalcfgdesign.ui:55
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:6
msgid "64 x 64"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:316
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:95
msgid "Addition"
msgstr "Sumar"

#. i18n: file: lmsensorscfgdesign.ui:622
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:160
msgid "Alarm"
msgstr "Alarma"

#. i18n: file: lmsensorscfgdesign.ui:737
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:166
msgid "Alarms"
msgstr "Alarmas"

#. i18n: file: lmsensorscfgdesign.ui:759
#. i18n: ectx: property (text), widget (QCheckBox)
#: rc.cpp:172
msgid "Apply to all sensors"
msgstr "Aplicar a todos los sensores"

#: main.cpp:53
msgid "Author"
msgstr ""

#. i18n: file: generalcfgdesign.ui:119
#. i18n: ectx: property (text), widget (QCheckBox)
#: rc.cpp:21
msgid "Autostart KSensors on KDE startup"
msgstr "Ejecutar KSensors al iniciar KDE"

#. i18n: file: palettecfgdesign.ui:376
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:214
msgid "Background"
msgstr "Fondo"

#. i18n: file: palettecfgdesign.ui:381
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:217
msgid "Border"
msgstr ""

#. i18n: file: resource/ksensorsui.rc:4
#. i18n: ectx: Menu (custom)
#: rc.cpp:226
msgid "C&ustom"
msgstr ""

#: systemcfg.cpp:42
msgid "CPU Speed"
msgstr "Velocidad del procesador"

#: systemcfg.cpp:43
msgid "CPU State"
msgstr "Estado del procesador"

#. i18n: file: lmsensorscfgdesign.ui:981
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:196
msgid "Celsius"
msgstr "Celsius"

#. i18n: file: generalcfgdesign.ui:90
#. i18n: ectx: property (title), widget (QGroupBox)
#. i18n: file: lmsensorscfgdesign.ui:504
#. i18n: ectx: property (title), widget (QGroupBox)
#. i18n: file: lmsensorscfgdesign.ui:611
#. i18n: ectx: property (title), widget (QGroupBox)
#. i18n: file: systemcfgdesign.ui:176
#. i18n: ectx: property (title), widget (QGroupBox)
#: rc.cpp:15 rc.cpp:148 rc.cpp:157 rc.cpp:235
msgid "Colors"
msgstr "Colores"

#. i18n: file: lmsensorscfgdesign.ui:196
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:36
msgid "Current"
msgstr "Actual"

#. i18n: file: palettecfgdesign.ui:370
#. i18n: ectx: property (text), widget (QPushButton)
#: rc.cpp:211
msgid "Default colors"
msgstr "Colores por defecto"

#. i18n: file: lmsensorscfgdesign.ui:138
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:30
msgid "Description"
msgstr "Descripción"

#. i18n: file: lmsensorscfgdesign.ui:790
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:175
msgid "Do nothing"
msgstr "No hacer nada"

#. i18n: file: lmsensorscfgdesign.ui:531
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:151
msgid "Dock"
msgstr "Dock"

#: main.cpp:69
msgid "Don't dock in KDE system tray."
msgstr "No empotrar en la barra de tareas de KDE."

#: main.cpp:59
msgid "Eric Sanford"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:971
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:193
msgid "Fahrenheit"
msgstr "Fahrenheit"

#: lmsensorscfg.cpp:124
msgid "Fan"
msgstr "Ventilador"

#. i18n: file: lmsensorscfgdesign.ui:84
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:27
msgid "General"
msgstr "General"

#: ksensorscfg.cpp:59 ksensorscfg.cpp:60
msgid "Global settings"
msgstr "Opciones generales"

#. i18n: file: lmsensorscfgdesign.ui:297
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:89
msgid "Ideal"
msgstr "Ideal"

#: main.cpp:41
#, fuzzy
msgid "KSensors4"
msgstr "Sensores"

#: main.cpp:35
#, fuzzy
msgid "KSensors4 - A nice lmsensors frontend for KDE"
msgstr "KSensors - Un agradable lmsensors frontend para KDE"

#: ksensorscfg.cpp:52
#, fuzzy
msgid "KSensors4 Configuration"
msgstr "Configuración de KSensors"

#. i18n: file: lmsensorscfgdesign.ui:992
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:199
msgid "Kelvin"
msgstr "Kelvin"

#. i18n: file: lmsensorscfgdesign.ui:207
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:39
msgid "Maximum"
msgstr "Máximo"

#: main.cpp:52
msgid "Miguel Novas"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:305
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:92
msgid "Minimum"
msgstr "Mínimo"

#: main.cpp:47
msgid "Monitoring your motherboard"
msgstr "Monitorizando su placa base"

#. i18n: file: lmsensorscfgdesign.ui:284
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:86
msgid "Multiplier"
msgstr "Multiplicar"

#. i18n: file: lmsensorscfgdesign.ui:720
#. i18n: ectx: property (text), widget (QLabel)
#: rc.cpp:163
msgid "Normal"
msgstr "Normal"

#. i18n: file: lmsensorscfgdesign.ui:748
#. i18n: ectx: property (title), widget (QButtonGroup)
#: rc.cpp:169
msgid "On reach alarm value:"
msgstr "En caso de alarma"

#: main.cpp:68
msgid "Only start if autostart is enabled."
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:443
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:142
msgid "Panel"
msgstr "Panel"

#. i18n: file: generalcfgdesign.ui:38
#. i18n: ectx: property (title), widget (QButtonGroup)
#: rc.cpp:3
msgid "Panels size"
msgstr "Tamaño de los paneles"

#. i18n: file: lmsensorscfgdesign.ui:815
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:181
msgid "Play alarm sound"
msgstr "Sonido de aviso"

#: main.cpp:60
msgid "Port to Qt4/KDE4"
msgstr ""

#. i18n: file: lmsensorscfgdesign.ui:943
#. i18n: ectx: attribute (title), widget (QWidget)
#. i18n: file: systemcfgdesign.ui:205
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:187 rc.cpp:238
msgid "Preferences"
msgstr "Preferencias"

#: systemcfg.cpp:44
msgid "RAM Used"
msgstr "Memoria RAM usada"

#. i18n: file: lmsensorscfgdesign.ui:829
#. i18n: ectx: property (text), widget (QRadioButton)
#: rc.cpp:184
msgid "Run command"
msgstr "Ejecutar orden"

#: systemcfg.cpp:45
msgid "SWAP Used"
msgstr "Memoria SWAP usada"

#. i18n: file: lmsensorscfgdesign.ui:58
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:24
msgid "Sensors"
msgstr "Sensores"

#: main.cpp:70
msgid "Show splash screen."
msgstr "Mostrar ventana de presentación."

#. i18n: file: generalcfgdesign.ui:102
#. i18n: ectx: property (title), widget (QGroupBox)
#: rc.cpp:18
msgid "Startup"
msgstr "Inicio"

#: ksensorscfg.cpp:114 ksensorscfg.cpp:115
msgid "System Information"
msgstr "Información del sistema"

#. i18n: file: systemcfgdesign.ui:46
#. i18n: ectx: attribute (title), widget (QWidget)
#: rc.cpp:229
msgid "System Panels"
msgstr "Paneles del sistema"

#: lmsensorscfg.cpp:115
msgid "Temperature"
msgstr "Temperatura"

#. i18n: file: lmsensorscfgdesign.ui:954
#. i18n: ectx: property (title), widget (QButtonGroup)
#: rc.cpp:190
msgid "Temperatures scale"
msgstr "Escala de las temperaturas"

#. i18n: file: lmsensorscfgdesign.ui:804
#. i18n: ectx: property (text), widget (QPushButton)
#: rc.cpp:178
msgid "Test"
msgstr "Probar"

#. i18n: file: palettecfgdesign.ui:329
#. i18n: ectx: property (text), widget (QLabel)
#. i18n: file: palettecfgdesign.ui:386
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:205 rc.cpp:220
msgid "Title"
msgstr ""

#: systemcfg.cpp:46
msgid "Up Time"
msgstr "Tiempo encendido"

#. i18n: file: lmsensorscfgdesign.ui:1019
#. i18n: ectx: property (title), widget (QGroupBox)
#. i18n: file: systemcfgdesign.ui:228
#. i18n: ectx: property (title), widget (QGroupBox)
#: rc.cpp:202 rc.cpp:241
msgid "Update interval"
msgstr "Intervalo de actualización"

#. i18n: file: palettecfgdesign.ui:391
#. i18n: ectx: property (text), item, widget (QComboBox)
#: rc.cpp:223
msgid "Value"
msgstr "Valor"

#. i18n: file: lmsensorscfgdesign.ui:166
#. i18n: ectx: property (title), widget (QGroupBox)
#: rc.cpp:33
msgid "Values"
msgstr "Valores"

#. i18n: file: lmsensorscfgdesign.ui:486
#. i18n: ectx: property (text), widget (QCheckBox)
#. i18n: file: lmsensorscfgdesign.ui:568
#. i18n: ectx: property (text), widget (QCheckBox)
#. i18n: file: systemcfgdesign.ui:158
#. i18n: ectx: property (text), widget (QCheckBox)
#: rc.cpp:145 rc.cpp:154 rc.cpp:232
msgid "Visible"
msgstr "Visible"

#: lmsensorscfg.cpp:128
msgid "Voltage"
msgstr "Tensión"

#: rc.cpp:243
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "michaell@teleline.es"

#: rc.cpp:242
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Miguel Novas"

#~ msgid "EMAIL OF TRANSLATORS"
#~ msgstr "michaell@teleline.es"

#~ msgid "Hard Disks"
#~ msgstr "Discos duros"

#~ msgid "KSensors configuration"
#~ msgstr "Configuración de KSensors"

#~ msgid "NAME OF TRANSLATORS"
#~ msgstr "Miguel Novas"

#~ msgid ""
#~ "Welcome to the KSensors configuration window. Click on the left to select "
#~ "a configuration option."
#~ msgstr ""
#~ "Bienvenido a la pantalla de configuracion de KSensors. Pinche a su "
#~ "izquierda para seleccionar una opción de configuración."
