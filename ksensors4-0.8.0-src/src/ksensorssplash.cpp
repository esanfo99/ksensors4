/***************************************************************************
                          ksensorssplash.cpp  -  part of KSensors4
                             -------------------
    begin                : sab abr 20 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ksensorssplash.h"

#include <kstandarddirs.h>

#include <Qt/qdesktopwidget.h>
#include <Qt/qapplication.h>
#include <Qt/qtimer.h>
#include <qt4/Qt/qicon.h>

KSensorsSplash::KSensorsSplash(QWidget *parent, const char *name)
: QWidget(
          parent,
          static_cast<Qt::WindowFlags> (
                Qt::FramelessWindowHint |
                Qt::WA_DeleteOnClose |
                Qt::WindowStaysOnTopHint
                )
          )
{
    setObjectName(name);
    setAttribute(Qt::WA_QuitOnClose, false);
    QPixmap pm = QIcon(":/pics/ksensorssplash.png").pixmap(320,255);
    QPalette pal;
    pal.setBrush(backgroundRole(), QBrush(pm));
    setPalette(pal);

    setGeometry(QApplication::desktop()->width() / 2 - pm.width() / 2,
                QApplication::desktop()->height() / 2 - pm.height() / 2,
                pm.width(), pm.height()
                );
    show();

    QTimer::singleShot(4000, this, SLOT(close()));
}

KSensorsSplash::~KSensorsSplash()
{
}
