/***************************************************************************
                          i8ksensorslist.h  -  part of KSensors4
                             -------------------
    begin                : jue jun 20 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : migueln@users.sourceforge.net
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef I8KSENSORSLIST_H
#define I8KSENSORSLIST_H

#include "sensorslist.h"

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Dell i8k Sensor list, inherits SensorsList, calls getI8KInfo() and
 * getAcpiTemperature()
 */

class I8KSensorsList : public SensorsList {
    Q_OBJECT

public:

    I8KSensorsList(QWidget *parent, const char *name);
    virtual ~I8KSensorsList();

    static bool I8KAvailable();

    QString getSensorGroupName() override { return QString("i8k"); }

public slots:

    void updateSensors() override;

private:

    bool i8kAvail;
    bool acpiAvail;

    Sensor *cpuTempAcpi;
    Sensor *cpuTemp;
    Sensor *leftFan;
    Sensor *rightFan;

    static int AcpiAvailable();
    static int I8KOnlyAvailable();
};

#endif
