/***************************************************************************
                          generalcfg.cpp  -  part of KSensors4
                             -------------------
    begin                : mar may 14 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "generalcfg.h"

#include <ksharedconfig.h>
#include <kconfiggroup.h>

GeneralCfg::GeneralCfg(LMSensors *lsensors, QWidget *parent, const char *name)
        : GeneralCfgDesign(parent, name), 
        sensors(lsensors)
{
    palPanelCfg = new PaletteCfg(boxGeneralPalette, "general");
    palPanelCfg->installEventFilter(this);
    connect(palPanelCfg, SIGNAL(colorDialogFinished()), this, SLOT(slotColorDialogFinished()));

    boxGeneralPalette->setColumnLayout(0, Qt::Vertical);
    boxGeneralPalette->layout()->setSpacing(6);
    boxGeneralPalette->layout()->setMargin(12);
    QGridLayout *boxGeneralPaletteLayout = new QGridLayout(boxGeneralPalette->layout());
    boxGeneralPaletteLayout->setAlignment(Qt::AlignTop);
    boxGeneralPaletteLayout->addWidget(palPanelCfg, 0, 0);

    palPanelCfg->readPalette(0);

    int panelSize = LMSensorsWidget::cfgReadPanelSize();
    switch (panelSize) {
    case 64:
        Radio64->setChecked(true);
        break;
    case 56:
        Radio56->setChecked(true);
        break;
    case 48:
        Radio48->setChecked(true);
        break;
    default:
        break;
    }

    KConfigGroup configGroup = KGlobal::config()->group("General");
    CheckBoxAutoStart->setChecked(configGroup.readEntry<bool>("AutoStart", true));
}

GeneralCfg::~GeneralCfg()
{
    palPanelCfg->deleteLater();
}

void GeneralCfg::slotApplyChanges()
{
    if (!isVisible()) {
        return;
    }

    palPanelCfg->savePalette(0);

    int panelSize = 64;
    if (Radio56->isChecked()) {
        panelSize = 56;
    }
    else if (Radio48->isChecked()) {
        panelSize = 48;
    }
    LMSensorsWidget::cfgWritePanelSize(panelSize);

    KConfigGroup configGroup = KGlobal::config()->group("General");
    configGroup.writeEntry("AutoStart", CheckBoxAutoStart->isChecked());
    configGroup.sync();
    
    sensors->emitConfigChanged();
}

void GeneralCfg::slotColorDialogFinished()
{
    this->activateWindow();
}

bool GeneralCfg::eventFilter(QObject *, QEvent *e)
{
    e->accept();
    return false;
}
