/***************************************************************************
                          sensor.cpp  -  part of KSensors4
                             -------------------
    begin                : mie abr 24 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "sensor.h"
#include "sensorslist.h"

#include <ksharedconfig.h>
#include <kconfiggroup.h>
#include <kglobal.h>

int Sensor::id = 0;

Sensor::Sensor(SensorsList *parent, const char *name) 
        : QWidget(parent),
        type(lmTemp), 
        description(""), 
        valMin(0.0), 
        valIdeal(35.0), 
        valMax(60.0),
        val(0.0), 
        compensation(0.0), 
        multiplicator(1.0), 
        monitorize(false),
        alarmOn(false)
{
    setObjectName(QString(name) + "-" + QString::number(++id));
}

Sensor::~Sensor()
{
    --id; // Debugging
}

Sensor::SensorClass Sensor::getClass()
{
    return (static_cast<SensorsList *> (parent()))->getClass();
}

QString Sensor::getPrintMask(bool addSufix)
{
    return getSensorPrintMask(type, addSufix, (static_cast<SensorsList*> (parent()))->getTempScale());
}

const char *Sensor::getSensorPrintMask(int sensorType, bool addSufix, TempScale temp)
{
    if (addSufix) {
        switch (sensorType) {
        case Sensor::lmTemp:
            switch (temp) {
            case dgCelsius:
                return "%.0lf C";
            case dgFahrenheit:
                return "%.0lf F";
            case dgKelvin:
                return "%.0lf K";
            default:
                break;
            }
            break;
        case Sensor::lmFan:
            return "%.0lf rpm";
        case Sensor::lmVoltage:
            return "%.2lfV";
        default:
            break;
        }
    }
    return sensorType == Sensor::lmVoltage ? ".2lf" : ".0lf";
}

bool Sensor::monitorized()
{
    return monitorize;
}

void Sensor::setMonitorized(bool enable)
{
    if (monitorize != enable) {
        monitorize = enable;
    }
}

void Sensor::setValueIdeal(double value, TempScale scale)
{
    valIdeal = toCelsius(value, scale);
}

void Sensor::setValueMax(double value, TempScale scale)
{
    valMax = toCelsius(value, scale);
}

void Sensor::setValueMin(double value, TempScale scale)
{
    valMin = toCelsius(value, scale);
}

void Sensor::setCompensation(double value, TempScale scale)
{
    compensation = toCelsiusDiff(value, scale);
}

void Sensor::setMultiplicator(double value)
{
    multiplicator = value;
}

void Sensor::setDescription(const QString &desc)
{
    description = desc;
}

void Sensor::setValue(double newVal, TempScale scale, bool adjust)
{
    newVal = toCelsius(newVal, scale);
    if (adjust) {
        newVal = adjustValue(newVal);
    }

    if (val != newVal) {
        val = newVal;
        emit valueChanged(celsiusTo(val));
    }
}

void Sensor::writeConfig()
{
    KConfigGroup configGroup = KGlobal::config()->group(objectName());
    if (configGroup.exists()) {
        configGroup.writeEntry("description", description);
        configGroup.writeEntry("valueIdeal", valIdeal);
        configGroup.writeEntry("valueMax", valMax);
        configGroup.writeEntry("valueMin", valMin);
        configGroup.writeEntry("compensation", compensation);
        configGroup.writeEntry("multiplicator", multiplicator);
        configGroup.writeEntry("monitorize", monitorize);
        emit configChanged();
    }
}

void Sensor::readConfig()
{
    KConfigGroup configGroup = KGlobal::config()->group(objectName());
    if (configGroup.exists()) {
        valMax = configGroup.readEntry<double>("valueMax", valMax);
        valMin = configGroup.readEntry<double>("valueMin", valMin);
        compensation = configGroup.readEntry<double>("compensation", compensation);
        multiplicator = configGroup.readEntry<double>("multiplicator", multiplicator);
        description = configGroup.readEntry("description", description);
        valIdeal = configGroup.readEntry<double>("valueIdeal", valIdeal);
        setMonitorized(configGroup.readEntry<bool>("monitorize", monitorize));
    }
}

double Sensor::toCelsius(double val, TempScale scale)
{
    if (type != lmTemp) {
        return val;
    }
    switch (scale == dgDefault ? getTempScale() : scale) {
    case dgCelsius:
        return val;
    case dgFahrenheit:
        return (5.0 / 9.0)*(val - 32.0);
    default:
        break;
    }
    return val - 273.16; // Kelvin to Celsius
}

double Sensor::celsiusTo(double val, TempScale scale)
{
    if (type != lmTemp) return val;
    switch (scale == dgDefault ? getTempScale() : scale) {
    case dgCelsius:
        return val;
    case dgFahrenheit:
        return (9.0 / 5.0) * val + 32.0;
    default:
        break;
    }
    return val + 273.16; // Celsius to Kelvin
}

double Sensor::toCelsiusDiff(double val, TempScale scale)
{
    if (scale == dgDefault) {
        scale = getTempScale();
    }
    if (type != lmTemp || scale != dgFahrenheit) {
        return val;
    }
    return val * (5.0 / 9.0);
}

double Sensor::celsiusToDiff(double val, TempScale scale)
{
    if (scale == dgDefault) {
        scale = getTempScale();
    }
    if (type != lmTemp || scale != dgFahrenheit) {
        return val;
    }
    return val * (9.0 / 5.0);
}

Sensor::TempScale Sensor::getTempScale()
{
    return (static_cast<SensorsList *> (parent()))->getTempScale();
}
