/***************************************************************************
                          hdsensorslist.h  -  part of KSensors4
                             -------------------
    begin                : vie abr 26 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef HDSENSORSLIST_H
#define HDSENSORSLIST_H

#include "processexec.h"
#include "sensor.h"
#include "sensorslist.h"

#include <kprocess.h>

#include <Qt/qpointer.h>
#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short HddSensor list, inherits SensorsList
 */

class HDSensorsList : public SensorsList {
    Q_OBJECT
public:
    HDSensorsList(QWidget *parent, const char *name);
    virtual ~HDSensorsList();

    QString getSensorGroupName() override {
        return QString("hddtemp");
    }

public slots:

    void updateSensors() override;

protected slots:

    void slotProcessExited(int exitCode, QProcess::ExitStatus exitStatus);

private:

    QPointer<ProcessExec> process;
    static bool isHddtemp;
    static bool isDaemon;
    static unsigned daemonPort;
    static QStringList daemonData;

    static bool isHddTempAvailable();
    static bool isDaemonAvailable();
    static bool getDisks(QStringList &disks);
    static bool getDisksFromDaemon(QStringList &disks);
    static bool getDiskInfo(const QString &buf, QString &name, double &value);
    static bool getDiskInfoFromDaemon();
};

#endif
