/***************************************************************************
                          cpupanel.cpp  -  part of KSensors4
                             -------------------
    begin                : Sun Nov 25 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cpupanel.h"
#include "qlcddraw.h"

#include <Qt/qcolor.h>
#include <Qt/qpainter.h>
#include <Qt/qlcdnumber.h>
#include <Qt/qwidget.h>

bool getCpuInfoValue(const char *name, QString &value)
{
    char buffer[128] = {0,};
    char *ptr;
    FILE *fp;
    bool result;
    if (!name) {
        return false;
    }

    result = false;
    if ((fp = fopen("/proc/cpuinfo", "r"))) {
        while (fgets(buffer, 127, fp)) {
            if (!strncmp(name, buffer, strlen(name))) {
                ptr = strchr(buffer, ':');
                if (ptr) {
                    value = static_cast<const char *> (ptr + 2);
                    value = value.trimmed();
                }
                result = true;
                break;
            }
        }
        fclose(fp);
    }
    return result;
}

void adjustString(QString &str, int maxlen)
{
    int i = str.indexOf('(');

    str.truncate(maxlen);
    if (i >= 0) str.truncate(i);
    str = str.trimmed();
}

CpuPanel::CpuPanel(QWidget *parent, const char *name) : Panel(parent, name)
{
    setObjectName(name);

    sCpu = "Unknown";
    getCpuInfoValue("model name", sCpu);
    adjustString(sCpu, 16);

    sVendor = "Unknown";
    getCpuInfoValue("vendor_id", sVendor);
    adjustString(sVendor, 16);

    sBogomips = "0";
    getCpuInfoValue("bogomips", sBogomips);
    sBogomips.sprintf("%.0f BMPS", sBogomips.toDouble());

    QString sSpeed = "0";
    getCpuInfoValue("cpu MHz", sSpeed);
    sSpeed.sprintf("%.0f", sSpeed.toDouble());
    speed = new QLCDNumber(this, "cpu_speed");
    speed->setGeometry(6, 20, 52, 26);
    speed->setNumDigits(sSpeed.length());
    speed->setSegmentStyle(QLCDNumber::Filled);
    speed->setFrameShape(QFrame::NoFrame);
    speed->display(sSpeed);
    QPalette cg = speed->palette();
    cg.setColor(QPalette::Foreground, QColor(255, 0, 0));
    cg.setColor(QPalette::Light, QColor(255, 0, 0));
    cg.setColor(QPalette::Midlight, QColor(231, 232, 246));
    cg.setColor(QPalette::Dark, QColor(104, 105, 118));
    cg.setColor(QPalette::Mid, QColor(139, 140, 158));
    cg.setColor(QPalette::Background, palette().background());
    speed->setPalette(cg);
    speed->installEventFilter(this);
}

CpuPanel::~CpuPanel()
{
    delete speed;
}

void CpuPanel::updateInfo()
{
    getCpuInfoValue("bogomips", sBogomips);
    sBogomips.sprintf("%.0f BMPS", sBogomips.toDouble());

    getCpuInfoValue("cpu MHz", sSpeed);
    sSpeed.sprintf("%.0f", sSpeed.toDouble());
    speed->setNumDigits(sSpeed.length());
    speed->display(sSpeed);
    update();
}

void CpuPanel::paintEvent(QPaintEvent *e)
{
    QPainter p(this);

    int w = width();
    int h = height();

    int i2 = (h * 4) / 5;
    int th = h - i2 - h / 11;

    QLcd::draw(&p, 2, h / 10, w - 4, th, sCpu.toLatin1(), QLcd::alignJustify, &getColorTitle());
    QLcd::draw(&p, 2, i2 + 1, w - 4, th, sBogomips.toLatin1(), QLcd::alignCenter, &getColorTitle());

    drawBorder(&p);
    e->accept();
}

void CpuPanel::paletteChange(const QPalette &)
{
    QPalette cg = speed->palette();

    cg.setColor(QPalette::Background, palette().background());

    speed->setPalette(cg);
}

void CpuPanel::resizeEvent(QResizeEvent *e)
{
    int w = width();
    int h = height();

    int mw = w / 10;

    speed->setGeometry(mw, h / 3, w - mw * 2, (h * 2) / 5);
    e->accept();
}
