/***************************************************************************
                          panel.cpp  -  part of KSensors4
                             -------------------
    begin                : Sun Nov 11 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "panel.h"

#include <ksharedconfig.h>
#include <kconfiggroup.h>
#include <kglobal.h>

Panel::Panel(QWidget *parent, const char *name) : QFrame(parent)
{
    setObjectName(name);
    setFrameStyle(QFrame::Panel | QFrame::Raised);
    resize(64, 64);
    setAutoFillBackground(true);
    installEventFilter(this);
}

Panel::~Panel()
{
}

bool Panel::eventFilter(QObject *, QEvent *e)
{
    if (e->type() == QEvent::MouseButtonPress || e->type() == QEvent::MouseButtonRelease) {
        emit eventReceived(e);
    }
    return false;
}

//**************************************************************************

bool Panel::readShow(const QString &name)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    bool is_show = configGroup.readEntry<bool>("show", false);
    return is_show;
}

void Panel::writeShow(const QString &name, bool fShow)
{
    KConfigGroup configGroup = KGlobal::config()->group(name);
    configGroup.writeEntry("show", fShow);
    if (!fShow) {
        configGroup.deleteEntry("showPos");
    }
}

//**************************************************************************

void Panel::drawBorder(QPainter *p)
{
    int w = width();
    int h = height();

    p->setPen(palette().light());
    p->drawLine(0, 0, w - 1, 0);
    p->drawLine(0, 0, 0, h - 1);
    p->setPen(palette().dark());
    p->drawLine(0, h - 1, w - 1, h - 1);
    p->drawLine(w - 1, 0, w - 1, h - 1);
}
