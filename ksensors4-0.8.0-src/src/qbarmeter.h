/***************************************************************************
                          qbarmeter.h  -  part of KSensors4
                             -------------------
    begin                : Mon Dec 3 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef QBARMETER_H
#define QBARMETER_H

#include <Qt/qcolor.h>
#include <Qt/qevent.h>
#include <Qt/qpainter.h>
#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Bar meter widget for CpuTimePanel
 */

class QBarMeter : public QWidget {

    Q_OBJECT

public:

    enum Direction {
        dirNone, dirLeft, dirRight, dirUp, dirDown
    };

    QBarMeter(QWidget *parent = 0, const char *name = 0);
    virtual ~QBarMeter();

    void setDirection(Direction dir);
    void setValueMax(double newVal);
    void setValue(double newVal);
    void setValueColor(const QColor &newColor);
    void setValueMaxColor(const QColor &newColor);
    void setSteps(int newSteps);

    void setValue(int index, double newVal);
    void setValueColor(int index, const QColor &newColor);
    void setValueCount(int count);
    void useValueMax(bool flag);

protected:

    double val[8];
    QColor color[8];
    bool useValMax;
    double valMax;
    QColor maxColor;
    int count;
    int steps;
    Direction dir;
    
    void paintEvent(QPaintEvent *e) override;
};

#endif
