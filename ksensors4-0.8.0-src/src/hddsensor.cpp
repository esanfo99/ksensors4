/***************************************************************************
                          hddsensor.cpp  -  part of KSensors4
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/* 
 * File:   hddsensor.cpp
 * Author: Eric Sanford
 *
 * Created on April 4, 2018, 12:50 PM
 * 
 * Copyright (C) 2018 Eric Sanford
 */

#include "hddsensor.h"

#include <ksharedconfig.h>
#include <kconfiggroup.h>
#include <kglobal.h>

#include <dirent.h>

HddSensor::HddSensor(SensorsList *parent) 
        : Sensor(parent), 
        devDisk(""), 
        udevName("")
{
    setType(lmTemp);
    setValueMax(55.0);
    hasDiskById = isUdevInstalled();
}

HddSensor::~HddSensor()
{
}

/**
 * 
 * @param devName : hard drive device name (/dev/sda, /dev/hdb, ...)
 */
void HddSensor::setDisk(const QString &devName)
{
    devDisk = devName;
}

/**
 * 
 * @return current device name of hard drive
 */
const QString &HddSensor::getDisk()
{
    return devDisk;
}

bool HddSensor::isUdevInstalled()
{
    DIR *dir;

    if ((dir = opendir("/dev/disk/by-id")) == NULL) {
        return false;
    }
    closedir(dir);
    return true;
}

/**
 * Returns a unique hdd sensor name that contains a model and serial number. 
 * This should help distinguish hdds on a computer that has more than one 
 * hard drive with the same model number.
 */
QString HddSensor::getUdevName(const QString &model)
{
    if (!hasDiskById) {
        return NULL;
    }
    DIR *dir;
    udevName = model;
    if ((dir = opendir("/dev/disk/by-id")) == NULL) {
        return NULL;
    }
    struct dirent *ptr;
    while ((ptr = readdir(dir))) {
        udevName = QString(ptr->d_name);
        if (udevName.contains(QString("-part"))) {
            continue;
        }
        if (udevName.contains(model)) {
            closedir(dir);
            return udevName;
        }
    }
    closedir(dir);
    return model;
}

/**
 * Update the hdd sensor description in ksensorsrc. If a computer has any 
 * usb storage devices, then hard drive device names (/dev/sda, /dev/sdb, 
 * ...) may change when the computer is rebooted. If a hdd sensor 
 * description in ksensorsrc starts with "/dev/" and doesn't match the 
 * current hard drive device name, then write the hard drive device name to
 * the sensor description in ksensorsrc. If the user has changed the hdd 
 * sensor description to something that doesn't start with "/dev/" (e.g., 
 * HDD1 or Seagate), then do nothing.
 * 
 * @param devName
 */
void HddSensor::setDescription(const QString &devName)
{
    Sensor::setDescription(devName);
    if (!hasDiskById || udevName.isNull() || udevName.isEmpty()) {
        return;
    }
    KConfigGroup configGroup = KGlobal::config()->group(udevName);
    if (configGroup.exists()) {
        QString savedDescription = configGroup.readEntry("description", devName);
        if (devName.startsWith("/dev/")
            && !savedDescription.isNull()
            && savedDescription.startsWith("/dev/")
            && savedDescription.compare(devName) != 0) {
            configGroup.writeEntry("description", devName);
        }
    }
}
