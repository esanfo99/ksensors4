/***************************************************************************
                          systemcfg.h  -  part of KSensors4
                             -------------------
    begin                : vie may 17 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef SYSTEMCFG_H
#define SYSTEMCFG_H

#include "systemcfgdesign_ui.h"
#include "lmsensors.h"
#include "palettecfg.h"

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Configuration page (system information), controlled by KSensorsCfg
 */

class SystemCfg : public SystemCfgDesign {
    Q_OBJECT

public:

    SystemCfg(LMSensors *lsensors, QWidget *parent = 0, const char *name = 0);
    virtual ~SystemCfg();

protected:
    bool eventFilter(QObject *o, QEvent *e) override;

protected slots:

    void slotCurrentSensorChanged(int panelIndex);
    void slotTabWidgetChanged(QWidget *);

    void readSensorInfo(int panelIndex);
    void readPreferencesInfo();
    void writeSensorInfo(int panelIndex);
    void writePreferencesInfo();

public slots:

    void slotApplyChanges();

private:

    LMSensors *sensors;
    QPointer<PaletteCfg> palPanelCfg;

    QString getPanelName(int panelIndex);
};

#endif
