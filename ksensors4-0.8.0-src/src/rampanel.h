/***************************************************************************
                          rampanel.h  -  part of KSensors4
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RAMPANEL_H
#define RAMPANEL_H

#include "panel.h"
#include "qdialarc.h"
#include "qlcdstring.h"

#include <Qt/qevent.h>
#include <Qt/qpalette.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Panel for InfoPanels - System Information (ram, swap), calls getMemInfo()
 */

class RamPanel : public Panel {

    Q_OBJECT
public:

    enum RamType {
        memRAM, memSWAP
    };

    RamPanel(QWidget *parent = 0, const char *name = 0, RamType type = memRAM);
    virtual ~RamPanel();

protected:

    RamType ramType;
    int memTotal, memUsed;
    QDialArc *arc;
    QLCDString *lcd1, *lcd2;
    void updateInfo() override;
    void paletteChange(const QPalette &pal) override;
    void resizeEvent(QResizeEvent *e) override;
};

#endif
