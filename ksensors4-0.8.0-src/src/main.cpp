/***************************************************************************
                          main.cpp  -  part of KSensors4
                             -------------------
    begin                : dom ago 19 02:10:35 EDT 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ksensors.h"
#include "ksensorssplash.h"
#include "lmsensorsdock.h"

#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kshareddatacache.h>
#include <kuniqueapplication.h>

#define KSENSORS_VERSION "0.8.0"

static const char *description =
        I18N_NOOP("KSensors4 - A nice lmsensors frontend for KDE");

int main(int argc, char *argv[])
{
    KAboutData aboutData(QByteArray("ksensors"), //            appName
                         QByteArray(0), //                     catalogName
                         ki18n("KSensors4"), //                 programName
                         QByteArray(KSENSORS_VERSION), //      version
                         ki18n(description), //                shortDescription
                         KAboutData::License_GPL_V2, //        licenseType
                         ki18n("(c) 2001, Miguel Novas\n"
                               "(c) 2018, Eric Sanford"), //      copyrightStatement
                         ki18n("Monitoring your motherboard"), // otherText
                         QByteArray("https://bitbucket.org/esanfo99/ksensors4"), // homePageAddress
                         QByteArray("esanfo99@gmail.com") // bugsEmailAddress
                         );

    aboutData.addAuthor(ki18n("Miguel Novas"), //                  name
                        ki18n("Author"), //                        task
                        QByteArray("michaell@teleline.es"), //     emailAddress
                        QByteArray("http://ksensors.sourceforge.net/"), // webAddress
                        QByteArray(0) //                           ocsUsername (Open Collaboration Services)
                        );

    aboutData.addAuthor(ki18n("Eric Sanford"), //                          name
                        ki18n("Port to Qt4/KDE4"), //                      task
                        QByteArray("esanfo99@gmail.com"), //               emailAddress
                        QByteArray("https://bitbucket.org/esanfo99/ksensors4"), // webAddress
                        QByteArray(0) //                                   ocsUsername (Open Collaboration Services)
                        );
    
    KCmdLineArgs::init(argc, argv, &aboutData);
    KCmdLineOptions *options = new KCmdLineOptions();
    options->add("autostart", ki18n("Only start if autostart is enabled."));
    options->add("nodock", ki18n("Don't dock in KDE system tray."));
    options->add("splash", ki18n("Show splash screen."));
    KCmdLineArgs::addCmdLineOptions(*options);
    KUniqueApplication::addCmdLineOptions();

    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
    bool isAutoStart = args->isSet("autostart");
    bool noDock = !args->isSet("dock");
    bool showSplash = args->isSet("splash");
    args->clear();

    delete options;

    if (!KUniqueApplication::start()) {
        return 0;
    }

    KUniqueApplication kuApp; 
    
    aboutData.setProgramLogo(QIcon(":/pics/ksensors.xpm").pixmap(32,32).toImage());

    KConfigGroup configGroup = KGlobal::config()->group("General");
    if (isAutoStart && !configGroup.readEntry<bool>("AutoStart", true)) {
        return 0;
    }

    if (configGroup.readEntry("Version") != aboutData.version()) {
        configGroup.writeEntry("Version", aboutData.version());
        showSplash = true;
    }

    if (showSplash) {
        (void) new KSensorsSplash(0, "ksensorsSplash");
    }

    LMSensorsDock *sensorsDock = new LMSensorsDock(noDock);
    QObject::connect(sensorsDock, SIGNAL(destroyed()), kapp, SLOT(quit()));
    QObject::connect(kapp, SIGNAL(saveYourself()), sensorsDock, SLOT(saveDesktopConfig()));
    Q_INIT_RESOURCE(pics);
    
    return kuApp.exec();
}
