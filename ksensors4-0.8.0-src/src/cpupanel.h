/***************************************************************************
                          cpupanel.h  -  part of KSensors4
                             -------------------
    begin                : Sun Nov 25 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CPUPANEL_H
#define CPUPANEL_H

#include "panel.h"
#include "qlcddraw.h"

#include <Qt/qlcdnumber.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Panel for InfoPanels - System Information (CPU speed), calls 
 * getCpuInfoValue()
 */

class CpuPanel : public Panel {
    Q_OBJECT

public:

    CpuPanel(QWidget *parent = 0, const char *name = 0);
    virtual ~CpuPanel();

protected:

    void paintEvent(QPaintEvent *e) override;
    void paletteChange(const QPalette &pal) override;

private:

    QLCDNumber *speed;
    QString sCpu, sVendor, sSpeed, sBogomips;
    void updateInfo() override;
    void resizeEvent(QResizeEvent *e) override;
};

#endif
