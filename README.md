# ksensors4

Port of [KSensors 0.7.3](http://ksensors.sourceforge.net/) from Qt3/KDE3 to Qt4/KDE4

## Overview
KSensors is a Linux system monitoring utility for [KDE](https://www.kde.org/). It displays system information (CPU temperature, motherboard fan speed, hard drive temperature, etc.) in system-tray icons and in a small dashboard window on the [KDE Plasma desktop](https://www.kde.org/plasma-desktop). KSensors 0.7.3 was built for [KDE 3.3](https://www.kde.org/announcements/announce-3.3.php) in 2004.

## Rationale
I like the nice KSensors system-tray icon and configurable dashboard panels. Before this project, I used [KSensors 0.7.3-40.fc28](https://pkgs.org/download/ksensors) in [Fedora's KDE spin](https://spins.fedoraproject.org/kde/). Then I upgraded an old Debian-based installation to [Debian 9 (Stretch)](https://wiki.debian.org/DebianStretch). But [Debian no longer offers a KSensors package](https://tracker.debian.org/pkg/ksensors). So I [ported](https://doc.qt.io/archives/qt-4.8/porting4-designer.html) KSensors to Qt4/KDE4 for Debian 9 (Stretch). Now I use a KDE Plasma desktop with KSensors4 0.8.0 in Debian and Fedora.

## Notable differences:
- KSensors 0.7.3 was an official KDE package, and was included in sevaral Linux distributions. KSensors4 0.8.0 is an independent project. It is only available here.
- Dependencies on KDE 3.3 have been ported to [KDE 4.14](https://www.kde.org/announcements/4.14/) and [KDE Platform 4](https://en.wikipedia.org/wiki/KDELibs)
- Dependencies on [Qt 3.3](https://doc.qt.io/archives/3.3/index.html) have been ported to [Qt 4.8](https://doc.qt.io/archives/qt-4.8/qt-overview.html)
- KSensors 0.7.3 uses an [autotools](https://www.gnu.org/software/automake/manual/html_node/index.html) build system. KSensors4 0.8.0 uses a [cmake](https://cmake.org/) build system.
- Help and API documentation ([KHelpCenter](https://userbase.kde.org/KHelpCenter) and [Doxygen](http://doxygen.nl/)) have been updated in KSensors4 0.8.0

## Dependencies:
- KDE Development Platform 4.14.7 or later
- Qt 4.8.6 or later
- [Lm_sensors](https://en.wikipedia.org/wiki/Lm_sensors)
- [Hddtemp](https://savannah.nongnu.org/projects/hddtemp/)

## Conclusion
This project is far from perfect, and might not be actively maintained, but I hope you have as much fun with it as I did, and find it to be a useful system utility.

