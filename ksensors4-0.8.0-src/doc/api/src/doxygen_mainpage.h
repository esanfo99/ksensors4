// /***************************************************************************
//  *                                                                         *
//  *   This program is free software; you can redistribute it and/or modify  *
//  *   it under the terms of the GNU General Public License as published by  *
//  *   the Free Software Foundation; either version 2 of the License, or     *
//  *   (at your option) any later version.                                   *
//  *                                                                         *
//  ***************************************************************************/
// 
// /* 
//  * File:   doxygen_mainpage.h
//  * Author: Eric Sanford
//  *
//  * Created on May 31, 2018, 11:38 AM
//  *
//  * Copyright (C) 2018 Eric Sanford
//  */

/*! \mainpage
 *
 * \section intro_sec Introduction
 *
 * Welcome to KSensors4 API Documentation.
 *
 * \section overview_sec Overview
 *
 * \subsection classes Class Descriptions
 *  
 * For brief descriptions of KSensors4 classes, see the Class List.
 *
 * \subsection qt_gui Qt Designer GUI
 *  
 * For more information about the Qt Designer GUI, see Ui Namespace Reference.
 *
 * \subsection usage Installation and Usage Information
 *  
 * For general usage information, see the ksensors Unix manual page.
 *  
 * For more detailed installation and usage information, see the KSensors 
 * Handbook. 
 */

// See the \mainpage command at 
// http://www.stack.nl/~dimitri/doxygen/commands.html#cmdmainpage

