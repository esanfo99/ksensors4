/***************************************************************************
                          lmsensorsalarms.h  -  part of KSensors4
                             -------------------
    begin                : Wed Nov 14 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORSALARMS_H
#define LMSENSORSALARMS_H

#include "lmsensors.h"
#include "phononplayer.h"

#include <Qt/qevent.h>
#include <Qt/qpointer.h>
#include <Qt/qtimer.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Detect alarm conditions, generate timed alerts, controlled by LMSensorsDock
 */

class LMSensorsAlarms : public QObject {
    Q_OBJECT

    friend class LMSensorsCfg;
    friend class LMSensorsDock;

public:

    enum Actions {
        acNothing, acSound, acCommand
    };

    LMSensorsAlarms(LMSensors *sensors, QWidget *parent = 0);
    virtual ~LMSensorsAlarms();

    void close();

protected:

    void playSound(const QString &sound, bool isTest = 0);
    void runSensorCommand(const QString &name, const QString &value, const QString &cmd);

    Actions readAlarm(const QString &name);
    QString readAlarmCommand(const QString &name);
    QString readAlarmSound(const QString &name);
    void writeAlarmCommand(const QString &name, const QString &cmd);
    void writeAlarm(const QString &name, Actions alarm);
    void writeAlarmSound(const QString &name, const QString &sound);

    // Debugging
    bool eventFilter(QObject *o, QEvent *e) override;
    
protected slots:
    void setValueChanged(Sensor *sensor);
    void slotConfigChanged(const char *name);
    void slotTimeout();

private:
    int timerId;
    unsigned alarmInterval; // milliseconds
    unsigned sensorIndex;
    QString alarmSound;
    QPointer<QTimer> alarmTimer;
    QPointer<PhononPlayer> phononPlayer;
    QList<Sensor *> audioList;
    LMSensors *sensors;

    QString getSoundPath(const QString &sound);
    void initializeAlarms(LMSensors *sensors);
    void runCommand(const QString &executable, const QStringList &params);
    void timerStart();
    void timerStop();
};

#endif
