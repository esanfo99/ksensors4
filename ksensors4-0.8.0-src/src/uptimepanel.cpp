/***************************************************************************
                          uptimepanel.cpp  -  part of KSensors4
                             -------------------
    begin                : Sun Nov 25 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "uptimepanel.h"
#include "procinfo.h"
#include "qlcddraw.h"

#include <Qt/qcolor.h>
#include <Qt/qpainter.h>
#include <Qt/qstring.h>

UpTimePanel::UpTimePanel(QWidget *parent, const char *name) : Panel(parent, name)
{
    setObjectName(name);

    lcd = new QLCDString(this, "uptime_lcd");
    lcd->setGeometry(6, 25, 52, 15);
    lcd->setShadow(true);
    lcd->setForeColor(Qt::red);
    lcd->setShadowColor(Qt::darkRed);
    lcd->installEventFilter(this);

    updateInfo();
}

UpTimePanel::~UpTimePanel()
{
    delete lcd;
}

void UpTimePanel::updateInfo()
{
    int uptime = getUpTime();

    int hours = uptime / (60 * 60);
    int minutes = (uptime % (60 * 60)) / 60;

    QString str;
    str.sprintf("%02d:%02d", hours, minutes);
    lcd->display(str);

    update();
}

void UpTimePanel::paintEvent(QPaintEvent *)
{
    QPainter p(this);

    int w = width();
    int h = height();

    int i2 = (h * 4) / 5;
    int th = h - i2 - h / 11;

    QLcd::draw(&p, 2, h / 10, w - 4, th, "hh:mm", QLcd::alignCenter, &getColorTitle());
    QLcd::draw(&p, 2, i2 + 1, w - 4, th, "Up Time", QLcd::alignCenter, &getColorTitle());

    drawBorder(&p);
}

void UpTimePanel::resizeEvent(QResizeEvent *e)
{
    int w = width();
    int h = height();

    int mw = w / 10;

    lcd->setGeometry(mw, h / 3, w - mw * 2, (h * 2) / 5);
    e->accept();
}
