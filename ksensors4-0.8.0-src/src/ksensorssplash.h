/***************************************************************************
                          ksensorssplash.h  -  part of KSensors4
                             -------------------
    begin                : sab abr 20 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KSENSORSSPLASH_H
#define KSENSORSSPLASH_H

#include <Qt/qwidget.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Splash screen, conditionally called from main()
 */

class KSensorsSplash : public QWidget {
    Q_OBJECT

public:

    KSensorsSplash(QWidget *parent = 0, const char *name = 0);
    virtual ~KSensorsSplash();
};

#endif
