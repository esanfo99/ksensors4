/***************************************************************************
                          ksensorscfg.cpp  -  part of KSensors4
                             -------------------
    begin                : mie may 8 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ksensorscfg.h"
#include "lmsensorsdock.h"

#include <klocale.h>
#include <kpagedialog.h>
#include <kstandarddirs.h>

/**
 * 
 * @param lsensors
 * @param sensorsAlarms
 * @param parent
 * @param name
 */
KSensorsCfg::KSensorsCfg(LMSensors *lsensors, LMSensorsAlarms *sensorsAlarms,
                         QWidget* parent, const char *name)
        : KPageDialog(),
        sensors(lsensors),
        m_generalcfg(NULL),
        m_systemcfg(NULL),
        alarms(sensorsAlarms),
        sensorsDock(parent)
{
    setFaceType(KPageDialog::List);
    setObjectName(QString(name));
    setButtons(Apply | Close);
    setDefaultButton(Close);
    setAttribute(Qt::WA_QuitOnClose, false);

    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum));
    setCaption(i18n("KSensors4 Configuration"));

    QFrame *gen_page = new QFrame(this);
    gen_page->setObjectName("page_general");

    QVBoxLayout *lay;

    KPageWidgetItem* gen_item = addPage(gen_page, i18n("Global settings"));
    gen_item->setHeader(i18n("Global settings"));
    gen_item->setIcon(KIcon(QIcon(":/pics/earth.png")));

    m_generalcfg = new GeneralCfg(sensors, gen_page, "generalCfg");
    // Debugging
    m_generalcfg->installEventFilter(this);
    lay = new QVBoxLayout(gen_page);
    lay->addWidget(m_generalcfg);
    connect(this, SIGNAL(applyClicked()), m_generalcfg, SLOT(slotApplyChanges()));
    gen_page->show();

    if (sensors) {
        const QObjectList *groupList = sensors->getSensorsChips();
        if (groupList && !groupList->isEmpty()) {
            QObjectList::const_iterator group_iter;
            for (group_iter = groupList->begin(); group_iter != groupList->end(); ++group_iter) {
                SensorsList *group = static_cast<SensorsList *> (*group_iter);
                if (group->getClass() == Sensor::lmSensor) {
                    QFrame *mb_page = new QFrame(this);
                    mb_page->setObjectName(QString("page_") + group->getDescription());
                    KPageWidgetItem* mb_item = addPage(mb_page, i18n(group->getDescription() 
                            + QString(" sensors")));
                    mb_item->setHeader(i18n(group->getDescription() + QString(" sensors")));
                    mb_item->setIcon(KIcon(QIcon(":/pics/motherboard.png")));
                    m_lmsensorscfg.append(new LMSensorsCfg(group, alarms, mb_page, 
                                                           QString(group->getDescription()).latin1()));
                    // Debugging
                    (m_lmsensorscfg.at(m_lmsensorscfg.size() - 1))->installEventFilter(this);
                    lay = new QVBoxLayout(mb_page);
                    lay->addWidget(m_lmsensorscfg.at(m_lmsensorscfg.size() - 1));
                    connect(this, SIGNAL(applyClicked()), 
                            m_lmsensorscfg.at(m_lmsensorscfg.size() - 1), SLOT(slotApplyChanges()));
                }
                else {
                    QFrame *hdd_page = new QFrame(this);
                    hdd_page->setObjectName(QString("page_") + group->getDescription());
                    KPageWidgetItem* hdd_item = addPage(hdd_page, i18n(group->getDescription()));
                    hdd_item->setHeader(i18n(group->getDescription() + QString(" sensors")));
                    hdd_item->setIcon(KIcon(QIcon(":/pics/harddisk.png")));
                    m_lmsensorscfg.append(new LMSensorsCfg(group, alarms, hdd_page, 
                                                           QString(group->getDescription()).latin1()));
                    // Debugging
                    (m_lmsensorscfg.at(m_lmsensorscfg.size() - 1))->installEventFilter(this);
                    lay = new QVBoxLayout(hdd_page);
                    lay->addWidget(m_lmsensorscfg.at(m_lmsensorscfg.size() - 1));
                    connect(this, SIGNAL(applyClicked()), 
                            m_lmsensorscfg.at(m_lmsensorscfg.size() - 1), SLOT(slotApplyChanges()));
                }
            }
        }
    }

    QFrame *sys_page = new QFrame(this);
    sys_page->setObjectName("page_system");
    KPageWidgetItem* sys_item = addPage(sys_page, i18n("System Information"));
    sys_item->setHeader(i18n("System Information"));
    sys_item->setIcon(KIcon(QIcon(":/pics/computers.png")));
    m_systemcfg = new SystemCfg(sensors, sys_page, "systemCfg");
    // Debugging
    m_systemcfg->installEventFilter(this);
    lay = new QVBoxLayout(sys_page);
    lay->addWidget(m_systemcfg);
    connect(this, SIGNAL(applyClicked()), m_systemcfg, SLOT(slotApplyChanges()));
    // Debugging
    connect(this, SIGNAL(closeClicked()), this, SLOT(close()));

    adjustSize();
    QRect rect(this->rect());
    KConfigGroup configGroup = KGlobal::config()->group("ConfigWidget");
    rect = configGroup.readEntry("geometry", rect);
    if (rect != this->rect()) {
        move(rect.topLeft());
        resize(rect.size());
    }
}

KSensorsCfg::~KSensorsCfg()
{
    m_systemcfg->deleteLater();
    for (int i = m_lmsensorscfg.size() - 1; i >= 0; --i) {
        m_lmsensorscfg.at(i)->deleteLater();
    }
    m_generalcfg->deleteLater();

    if (x() >= 0 && y() >= 0) {
        KConfigGroup configGroup = KGlobal::config()->group("ConfigWidget");
        configGroup.writeEntry("geometry", QRect(pos(), size()));
    }
}

// Debugging
void KSensorsCfg::closeEvent(QCloseEvent *e)
{
    e->accept();
}

bool KSensorsCfg::eventFilter(QObject *, QEvent *)
{
    return false;
}
