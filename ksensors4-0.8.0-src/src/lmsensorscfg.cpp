/***************************************************************************
                          lmsensorscfg.cpp  -  part of KSensors4
                             -------------------
    begin                : Mon Aug 13 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : 
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lmsensorscfg.h"
#include "lmsensorsalarms.h"
#include "lmsensordockpanel.h"
#include "lmsensorswidget.h"

#include <klocale.h>
#include <kicon.h>
#include <kstandarddirs.h>

#include <Qt/qpushbutton.h>
#include <Qt/qcombobox.h>
#include <Qt/qwidget.h>
#include <Qt/qstring.h>

LMSensorsCfg::LMSensorsCfg(SensorsList *lsensors, LMSensorsAlarms *sensorsAlarms,
                           QWidget *parent, const char *name)
: LMSensorsCfgDesign(parent, name, Qt::WDestructiveClose),
sensors(lsensors),
curSensor(0),
alarms(sensorsAlarms)
{
    palPanelCfg = new PaletteCfg(boxPanelPalette, name);
    // Debugging
    palPanelCfg->installEventFilter(this);
    boxPanelPalette->setColumnLayout(0, Qt::Vertical);
    boxPanelPalette->layout()->setSpacing(6);
    boxPanelPalette->layout()->setMargin(12);
    QGridLayout *boxPanelPaletteLayout = new QGridLayout(boxPanelPalette->layout());
    boxPanelPaletteLayout->setAlignment(Qt::AlignTop);
    boxPanelPaletteLayout->addWidget(palPanelCfg, 0, 0);

    readSensorsList();

    connect(MainTab, SIGNAL(currentChanged(QWidget*)), this, SLOT(slotMainTabPageChanged(QWidget *)));
    connect(listSensors, SIGNAL(highlighted(int)), this, SLOT(slotCurrentSensorChanged(int)));
    connect(buttonTest, SIGNAL(clicked()), this, SLOT(slotTestAlarm()));
    connect(editMinValue, SIGNAL(activated(int)), this, SLOT(slotComboMinMaxSelected(int)));
    connect(editMaxValue, SIGNAL(activated(int)), this, SLOT(slotComboMinMaxSelected(int)));

    slotMainTabPageChanged(MainTab);
}

LMSensorsCfg::~LMSensorsCfg()
{
    palPanelCfg->deleteLater();
}

void LMSensorsCfg::readSensorsList()
{
    const QObjectList *sensorList = sensors->getSensors();
    if (sensorList && !sensorList->isEmpty()) {
        listSensors->clear();
        QPixmap pm;

        QObjectList::const_iterator it;
        for (it = sensorList->begin(); it != sensorList->end(); ++it) {
            Sensor *sensor = static_cast<Sensor *> (*it);
            switch (sensor->getType()) {
            case LMSensor::lmTemp:
                pm = QIcon(":/pics/thermometer18x18.png").pixmap();
                break;
            case LMSensor::lmFan:
                pm = QIcon(":/pics/cooler18x18.png").pixmap();
                break;
            case LMSensor::lmVoltage:
                pm = QIcon(":/pics/battery18x18.png").pixmap();
                break;
            default:
                break;
            }
            listSensors->insertItem(pm, sensor->getDescription());
        }
        listSensors->setCurrentItem(0);
    }
}

void LMSensorsCfg::readSensorInfo(int index)
{
    if (index < 0) {
        return;
    }

    Sensor *oldSensor = curSensor;

    curSensor = sensors->getSensor(index);

    if (!curSensor) {
        return;
    }

    if (oldSensor == 0 || curSensor->getType() != oldSensor->getType() || curSensor->getClass() != oldSensor->getClass()) {
        switch (curSensor->getType()) {
        case LMSensor::lmTemp:
            GroupBoxDes->setName(i18n("Temperature"));
            if (curSensor->getClass() == Sensor::lmSensor) {
                pixmapSensor->setPixmap(QIcon(":/pics/thermometer.png").pixmap().scaled(48, 48, Qt::KeepAspectRatio));
            }
            else {
                pixmapSensor->setPixmap(QIcon(":/pics/harddisk.png").pixmap().scaled(48, 48, Qt::KeepAspectRatio));
            }
            break;
        case LMSensor::lmFan:
            GroupBoxDes->setName(i18n("Fan"));
            pixmapSensor->setPixmap(QIcon(":/pics/cooler.png").pixmap().scaled(50, 47, Qt::KeepAspectRatio));
            break;
        case LMSensor::lmVoltage:
            GroupBoxDes->setName(i18n("Voltage"));
            pixmapSensor->setPixmap(QIcon(":/pics/battery.png").pixmap().scaled(48, 48, Qt::KeepAspectRatio));
            break;
        default:
            break;
        }
    }
    editDescription ->setText(curSensor->getDescription());
    QString str;

    const char *mask = curSensor->getType() == LMSensor::lmVoltage ? "%.2lf" : "%.0lf";

    str.sprintf(curSensor->getPrintMask(true), curSensor->getValue());
    editValue->setText(str);

    str.sprintf(mask, curSensor->getValueIdeal());
    editIdealValue->setText(str);

    str.sprintf(mask, curSensor->getValueMin());
    editMinValue->setEditText(str);

    str.sprintf(mask, curSensor->getValueMax());
    editMaxValue->setEditText(str);

    editSum->setText(QString::number(curSensor->getCompensation()));
    editMul->setText(QString::number(curSensor->getMultiplicator()));

    CheckAllSensors->setChecked(false);

    colorAlarm ->setColor(LMSensorDockPanel::readColorAlarm(curSensor->objectName()));
    colorNormal ->setColor(LMSensorDockPanel::readColorNormal(curSensor->objectName()));
    checkShow ->setChecked(LMSensorPanel::readShow(curSensor->objectName()));
    checkShowInDock->setChecked(checkShowInDock->isEnabled() && LMSensorDockPanel::readShowInDock(curSensor->objectName()));

    editLaunch->setText(alarms->readAlarmCommand(curSensor->objectName()));
    editPlay ->setText(alarms->readAlarmSound(curSensor->objectName()));
    switch (alarms->readAlarm(curSensor->objectName())) {

    case LMSensorsAlarms::acNothing:
        radioAlarm1->setChecked(true);
        break;
    case LMSensorsAlarms::acSound:
        radioAlarm2->setChecked(true);
        break;
    case LMSensorsAlarms::acCommand:
        radioAlarm3->setChecked(true);
        break;
    }
    palPanelCfg->readPalette(curSensor->objectName());
}

void LMSensorsCfg::readPreferencesInfo()
{
    switch (sensors->getTempScale()) {

    case LMSensor::dgCelsius:
        RadioC->setChecked(true);
        break;
    case LMSensor::dgFahrenheit:
        RadioF->setChecked(true);
        break;
    case LMSensor::dgKelvin:
        RadioK->setChecked(true);
        break;
    default:
        break;
    }
    SpinUpdateTime->setValue(sensors->getUpdateInterval());
}

void LMSensorsCfg::applySensorChanges()
{
    if (!curSensor) {
        return;
    }
    // Apply sensor configuration
    curSensor->setDescription(editDescription->text());
    curSensor->setCompensation(editSum->text().toDouble());
    curSensor->setMultiplicator(editMul->text().toDouble());
    curSensor->setValueIdeal(editIdealValue->text().toDouble());
    curSensor->setValueMax(editMaxValue->currentText().toDouble());
    curSensor->setValueMin(editMinValue->currentText().toDouble());
    curSensor->setMonitorized(
                              checkShow->isChecked() ||
                              checkShowInDock->isChecked() ||
                              !radioAlarm1->isChecked()
                              );
    // Apply show in main window configuration
    LMSensorPanel::writeShow(curSensor->objectName(), checkShow->isChecked());
    // Apply dock configuration
    LMSensorDockPanel::writeShowInDock(curSensor->objectName(), checkShowInDock->isChecked());
    LMSensorDockPanel::writeColorNormal(curSensor->objectName(), colorNormal->color());
    LMSensorDockPanel::writeColorAlarm(curSensor->objectName(), colorAlarm->color());
    //
    palPanelCfg->savePalette(curSensor->objectName());
    //
    // Apply alarms configuration
    if (CheckAllSensors->isChecked()) {
        const QObjectList *sensorList = sensors->getSensors();
        if (sensorList && !sensorList->isEmpty()) {
            QObjectList::const_iterator it;
            for (it = sensorList->begin(); it != sensorList->end(); ++it) {
                Sensor *sensor = static_cast<Sensor *> (*it);
                applySensorAlarmConfig(sensor);
                sensor->writeConfig();
            }
        }
    }
    else {
        applySensorAlarmConfig(curSensor);
        curSensor->writeConfig();
    }
    if (checkShow->isChecked()) {
        show();
    }
    KConfigGroup configGroup = KGlobal::config()->group(curSensor->objectName());
    configGroup.sync();

    // This line must always be at the end of the method
    listSensors->changeItem(*listSensors->pixmap(listSensors->currentItem()),
                            editDescription->text(), listSensors->currentItem());
}

void LMSensorsCfg::applySensorAlarmConfig(Sensor *sensor)
{
    QString name = sensor->objectName();
    alarms->writeAlarmCommand(name, editLaunch->text());
    alarms->writeAlarmSound(name, editPlay ->text());
    LMSensorsAlarms::Actions code = LMSensorsAlarms::acNothing;
    if (radioAlarm1->isChecked()) {
        code = LMSensorsAlarms::acNothing;
    }
    else if (radioAlarm2->isChecked()) {
        code = LMSensorsAlarms::acSound;
    }
    else if (radioAlarm3->isChecked()) {
        code = LMSensorsAlarms::acCommand;
    }
    alarms->writeAlarm(name, code);
}

void LMSensorsCfg::applyPreferencesChanges()
{
    LMSensor::TempScale scale = LMSensor::dgCelsius;
    if (RadioK->isChecked()) {
        scale = LMSensor::dgKelvin;
    }
    else if (RadioF->isChecked()) {

        scale = LMSensor::dgFahrenheit;
    }
    sensors->setTempScale(scale);
    sensors->setUpdateInterval(SpinUpdateTime->value());
    sensors->writeConfig();
}

void LMSensorsCfg::slotCurrentSensorChanged(int listIndex)
{
    readSensorInfo(listIndex);
}

void LMSensorsCfg::slotApplyChanges()
{
    if (!isVisible()) {
        return;
    }

    switch (MainTab->currentPageIndex()) {

    case 0:
        applySensorChanges();
        break;
    case 1:
        applyPreferencesChanges();
        break;
    default:
        break;
    }
}

void LMSensorsCfg::slotAccept()
{
    slotApplyChanges();
    close();
}

void LMSensorsCfg::slotTestAlarm()
{
    if (radioAlarm2->isChecked()) {
        alarms->playSound(editPlay->text(), true);
    }
    else if (radioAlarm3->isChecked()) {
        alarms->runSensorCommand("TEST", "100", editLaunch->text());
    }
}

void LMSensorsCfg::slotMainTabPageChanged(QWidget *)
{
    switch (MainTab->currentPageIndex()) {

    case 0:
        readSensorInfo(listSensors->currentItem());
        break;
    case 1:
        readPreferencesInfo();
        break;
    }
}

void LMSensorsCfg::slotComboMinMaxSelected(int comboIndex)
{
    QComboBox *combo = static_cast<QComboBox*> (sender());
    QString str = combo->text(comboIndex);
    double percent = str.left(str.length() - 1).toDouble();
    double value = editIdealValue->text().toDouble();

    if (value < 0) {
        percent = -percent;
    }
    value = value + value * percent / 100;

    const char *mask = curSensor->getType() == LMSensor::lmVoltage ? "%.2lf" : "%.0lf";
    str.sprintf(mask, value);

    combo->setEditText(str);
}

// Debugging

bool LMSensorsCfg::eventFilter(QObject *, QEvent *e)
{
    e->accept();
    return false;
}
