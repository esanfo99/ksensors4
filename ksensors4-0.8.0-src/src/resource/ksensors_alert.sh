#!/bin/bash

# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/
# 
# /* 
#  * File:   ksensors_alert.sh
#  * Author: Eric Sanford
#  *
#  * Created on May 25, 2018, 1:50 PM
#  *
#  * Copyright (C) 2018 Eric Sanford
#  */
# 
# This is a sample KSensors4 alert command script. To use it, copy it to 
# a directory in your PATH.
#
# To enable or disable KSensors4 alert commands, run ksensors, open the 
# Configure dialog, select a sensors group, and click the Alarms tab.
#
# To test this alert command script, select the 'Run command' option on the
# Alarms tab, enter "ksensors_alert.sh" (with or without quotes), and click
# Apply.
#
# To disable alert commands (default), select the 'Do nothing' option on the
# Alarms tab and click Apply. 
#

if ! test "$SENSOR_NAME"; then
  SENSOR_NAME="TEST"
fi

if ! test "$SENSOR_VALUE"; then
  SENSOR_VALUE="101"
fi

SCRIPT_NAME=$(basename "$0")
DEBUG="1"
DEBUG_MSG=

# As a simple test, this command sends an alert to stderr:
#
simple_alert() {
  echo "KSensors alert: sensor name($SENSOR_NAME) sensor value($SENSOR_VALUE)" 1>&2
}

# This example creates an entry in the Linux system log:
#
logger_alert() {
  DEBUG_MSG="$DEBUG_MSG\n$SCRIPT_NAME $LINENO: sensor name($SENSOR_NAME) sending alert to system log"
  logger "KSensors alert: sensor name($SENSOR_NAME) sensor value($SENSOR_VALUE)"
}

# This example sends email alerts to a user's Linux system mail address. To 
# use it, install mailx (or a compatible alternative) and a compatible MTA 
# (sendmail, postfix, exim). Email alerts have only been tested on Debian 
# (stretch) with GNU mailutils and postfix. Here's a sample email:
#
#   X-Original-To: root@debian.linux-debian.site
#   Subject: KSensors alert
#   To: <root@debian.linux-debian.site>
#   X-Mailer: mail (GNU Mailutils 3.1.1)
#   Date: Fri, 25 May 2018 16:17:59 -0400 (EDT)
#   From: root@debian.linux-debian.site (root)
#
#   sensor name(/dev/sda) sensor value(41)
#
email_alert() {
  OWN_USERID=$(whoami)
  OWN_SYSTEM_HOST_NAME=$(hostname --fqdn)
  OWN_SYSTEM_EMAIL_ADDRESS="$OWN_USERID""@""$OWN_SYSTEM_HOST_NAME"
  DEBUG_MSG="$DEBUG_MSG\n$SCRIPT_NAME $LINENO: sensor name($SENSOR_NAME) sending email to $OWN_SYSTEM_EMAIL_ADDRESS"
  (echo "sensor name($SENSOR_NAME) sensor value($SENSOR_VALUE)") | mailx -s "KSensors alert" "$OWN_SYSTEM_EMAIL_ADDRESS"
}

# This example sends email alerts to a user's Linux system mail address at 
# user-defined intervals. 
#
EMAIL_ALERT_INTERVAL="300"
periodic_email_alert() {

  ALERTS_DIR="/tmp/ksensors/alerts"
  SENSOR_FILE_NAME=$(echo "$SENSOR_NAME" | tr -d "[:cntrl:]" | tr "[:punct:]" "_" | tr "[:space:]" "_")
  if ! test -e "$ALERTS_DIR" || ! test -e "$ALERTS_DIR"/"$SENSOR_FILE_NAME"; then  
    mkdir -p "$ALERTS_DIR" 2>/dev/null
    email_alert
    echo "$SENSOR_VALUE" > "$ALERTS_DIR"/"$SENSOR_FILE_NAME"
  else 
    DEBUG_MSG="$DEBUG_MSG\n$SCRIPT_NAME $LINENO: sensor name($SENSOR_NAME) already emailed"
    CURR_TIME=$(date +%s)
    FILE_TIME=$(stat -c %Z "$ALERTS_DIR"/"$SENSOR_FILE_NAME")
    FILE_AGE=$(expr "$CURR_TIME" - "$FILE_TIME")
    DEBUG_MSG="$DEBUG_MSG\n$SCRIPT_NAME $LINENO: $ALERTS_DIR"/"$SENSOR_FILE_NAME age: $FILE_AGE seconds"
    if test "$FILE_AGE" -gt "$EMAIL_ALERT_INTERVAL" || test "$SENSOR_NAME" = "$TEST"; then
      email_alert
      echo "$SENSOR_VALUE" > "$ALERTS_DIR"/"$SENSOR_FILE_NAME"
    fi
  fi
}

# This example plays an alarm sound using SoX
#
play_alarm_sound() {
  DEBUG_MSG="$DEBUG_MSG\n$SCRIPT_NAME $LINENO: sensor name($SENSOR_NAME) playing alarm sound \"ksensors_alert.ogg\""
  play "/usr/local/share/apps/sounds/ksensors_alert.ogg"
}

# Comment/uncomment commands below to test different alert options
#
# simple_alert
logger_alert
# email_alert
# periodic_email_alert
# play_alarm_sound

if test "$DEBUG"; then
  echo -e "$DEBUG_MSG"
fi

