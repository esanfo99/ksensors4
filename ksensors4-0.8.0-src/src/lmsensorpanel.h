/***************************************************************************
                          lmsensorpanel.h  -  part of KSensors4
                             -------------------
    begin                : Sat Aug 11 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LMSENSORPANEL_H
#define LMSENSORPANEL_H

#include "panel.h"
#include "qdialarc.h"
#include "qlcdstring.h"
#include "sensor.h"

#include <Qt/qevent.h>
#include <Qt/qpalette.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Panel for LMSensor, HddSensor, and i8k Sensor objects
 */

class LMSensorPanel : public Panel {
    Q_OBJECT

public:

    LMSensorPanel(Sensor *newSensor, QWidget *parent = 0, const char *name = 0);
    virtual ~LMSensorPanel();

public slots:

    void setValue(double value);
    void updateConfig();

protected:

    void resizeEvent(QResizeEvent *e) override;

    void createTitleWidget();
    void createGraphicWidget();
    void createValueWidget();

    void timerEvent(QTimerEvent *e) override;
    void paletteChange(const QPalette &pal) override;

private:

    Sensor *sensor;
    QDialArc *arc;
    QLCDString *lcdDes;
    QLCDString *lcdVal;
    QString valMask;
    bool alarm;
    int timerId;
};

#endif
