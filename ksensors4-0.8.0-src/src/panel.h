/***************************************************************************
                          panel.h  -  part of KSensors4
                             -------------------
    begin                : Sun Nov 11 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PANEL_H
#define PANEL_H

#include <Qt/qcolor.h>
#include <Qt/qevent.h>
#include <Qt/qframe.h>
#include <Qt/qobject.h>
#include <Qt/qpainter.h>
#include <Qt/qpalette.h>
#include <Qt/qstring.h>
/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short QFrame for LMSensorsWidget Sensor panels
 */

class Panel : public QFrame {
    Q_OBJECT

public:

    Panel(QWidget *parent, const char *name);
    virtual ~Panel();

    inline const QColor &getColorTitle() {
        return palette().text();
    };

    inline const QColor &getColorValue() {
        return palette().foreground();
    };

    static bool readShow(const QString &name);
    static void writeShow(const QString &name, bool fShow);
    
public slots:

    virtual void updateInfo() {}

signals:
    void eventReceived(QEvent *e);

protected:
    void drawBorder(QPainter *p);
    
private:
    bool eventFilter(QObject *o, QEvent *e) override;

};

#endif
