/***************************************************************************
                          processexec.h  -  part of KSensors4
                             -------------------
    begin                : sab abr 27 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PROCESSEXEC_H
#define PROCESSEXEC_H

#include <kprocess.h>

#include <unistd.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Runs external processes in HDSensorsList, LMSensorsAlarms
 */

class ProcessExec : public KProcess {
    Q_OBJECT

public:
    ProcessExec();
    virtual ~ProcessExec();

    int run();
    bool runAndWait();

    bool outputErrors() {
        return fErrors;
    };

    inline QString getStdoutData() {
        return out_buffer;
    };

    inline QString getStderrData() {
        return err_buffer;
    };

    inline unsigned getStdoutDataLen() {
        return out_buffer.length();
    }

    inline void clearData() {
        out_buffer.truncate(0);
        err_buffer.truncate(0);
        fErrors = false;
    };

    void outInfo(QString executable);
    void errInfo(QString executable, int status);

protected slots:
    void slotReceivedStdout();
    void slotReceivedStderr();

private:
    QString out_buffer;
    QString err_buffer;
    bool fErrors;
};

#endif
