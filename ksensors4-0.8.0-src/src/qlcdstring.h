/***************************************************************************
                          qlcdstring.h  -  part of KSensors4
                             -------------------
    begin                : Sat Jul 21 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef QLCDSTRING_H
#define QLCDSTRING_H

#include "qlcddraw.h"

#include <Qt/qwidget.h>
#include <Qt/qevent.h>

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Ascii character widget for LMSensorPanel, CpuTimePanel, RamPanel, UpTimePanel
 */

class QLCDString : public QWidget {

    Q_OBJECT

public:

    enum Align {
        alignLeft = 0x0000,
        alignCenter = 0x0001,
        alignRight = 0x0002,
        alignJustify = 0x0003
    };

    QLCDString(QWidget *parent = 0, const char *name = 0);
    virtual ~QLCDString();

    void display(const QString &str);

    void setShadow(bool enable);

    bool getShadow() {
        return (bool)(options & QLcd::drawShadow);
    };

    void setAlign(Align newAlign);

    void setForeColor(const QColor &fore);
    void setShadowColor(const QColor &sha);

    void setNumberDisplay(bool enable);

    const QColor& foreColor();
    const QColor& shadowColor();

protected:

    void resizeEvent(QResizeEvent *e) override;
    void paintEvent(QPaintEvent *e) override;

private:

    QString digitStr;
    int options;

    QColor forColor, shaColor;
};

#endif
