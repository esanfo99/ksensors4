/***************************************************************************
                          phononplayer.h  -  part of KSensors4
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/* 
 * File:   phononplayer.h
 * Author: Eric Sanford
 *
 * Created on May 14, 2018, 3:39 PM
 * 
 * Copyright (C) 2018 Eric Sanford
 */

#ifndef PHONONPLAYER_H
#define PHONONPLAYER_H

#include <kmimetype.h>

#include <Qt/qpointer.h>

#include <phonon/mediaobject.h>
#include <phonon/audiooutput.h>

/**
 * @author Eric Sanford
 * @short Play LMSensorsAlarms sounds
 */
class PhononPlayer : public Phonon::MediaObject {
    Q_OBJECT

public:

    PhononPlayer(QObject *parent = 0);
    virtual ~PhononPlayer();

    void playTimedSound(const QString &soundPath, unsigned maxMsecs = 3000);
    QString toString();
    void close();

private:

    bool isFinished;
    bool isStopped;
    unsigned playMsecs;
    unsigned stopMsecs;
    Phonon::Path phononPath;
    QPointer<Phonon::AudioOutput> pAudioOutput;
    QString outputDeviceName;
    QString soundPath;
    QUrl soundUrl;
    KMimeType::Ptr mimeType;
    Phonon::MediaSource mediaSource;
    Phonon::Mrl currentMrl;
    QString metaDataTitle;
    qint64 msecsTotalTime;

    QUrl *updateSoundUrl(const QString &filePath);
    Phonon::MediaSource *updateMediaSource(const QUrl &url);
    KMimeType::Ptr updateMimeType(const QUrl &url);

private slots:

    void mediaCurrentSourceChanged(const Phonon::MediaSource &newSource);
    void mediaFinished();
    void mediaMetaDataChanged();
    void mediaStateChanged(Phonon::State newstate, Phonon::State oldstate);
    void mediaTotalTimeChanged(qint64 newTotalTime);
    void audioOutputDeviceChanged(const Phonon::AudioOutputDevice);
};

#endif /* PHONONPLAYER_H */
