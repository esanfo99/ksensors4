/***************************************************************************
                          infopanels.h  -  part of KSensors4
                             -------------------
    begin                : vie may 10 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INFOPANELS_H
#define INFOPANELS_H

#include "panelsgroup.h"
#include "cpupanel.h"
#include "rampanel.h"
#include "cputimepanel.h"
#include "uptimepanel.h"

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Inherits PanelsGroup, controls CpuPanel, CpuTimePanel, RamPanel,
 * UpTimePanel
 */

class InfoPanels : public PanelsGroup {
    Q_OBJECT

public:
    InfoPanels(QWidget *parent = 0);
    virtual ~InfoPanels();

    static int cfgReadUpdateInterval();
    static void cfgWriteUpdateInterval(int interval);

public slots:

    void configChanged(const char *name) override;

protected slots:

    void infoPanelDestroyed();

private:

    int timerPanelCount;
    QTimer *timer;
    QWidget *sensorsWidget;
    QList<Panel *> panelList;

    void readUpdateInterval();
    void timerConnect(Panel *panel);
};

#endif
