/***************************************************************************
                          qlcdstring.cpp  -  part of KSensors4
                             -------------------
    begin                : Sat Jul 21 2001
    copyright            : (C) 2001 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


#include "qlcdstring.h"

QLCDString::QLCDString(QWidget *parent, const char *name) 
        : QWidget(parent),
        digitStr(""),
        options(QLcd::alignCenter),
        forColor(palette().brush(QPalette::Foreground)),
        shaColor(palette().brush(QPalette::Shadow))
{
    setObjectName(QString(name));
}

QLCDString::~QLCDString()
{
}

void QLCDString::display(const QString &str)
{
    digitStr = str;
    update();
}

void QLCDString::setAlign(Align newAlign)
{
    if ((options & QLcd::alignMask) != newAlign) {
        options = (options & ~QLcd::alignMask) | newAlign;
        update();
    }
}

const QColor& QLCDString::foreColor()
{
    return forColor;
}

const QColor& QLCDString::shadowColor()
{
    return shaColor;
}

void QLCDString::setForeColor(const QColor &fore)
{
    forColor = fore;
    update();
}

void QLCDString::setShadowColor(const QColor &sha)
{
    shaColor = sha;
    update();
}

void QLCDString::resizeEvent(QResizeEvent *e)
{
    update();
    e->accept();
}

void QLCDString::setShadow(bool enable)
{
    if ((bool)(options & QLcd::drawShadow) != enable) {
        options ^= QLcd::drawShadow;
        update();
    }
}

void QLCDString::setNumberDisplay(bool enable)
{
    if ((bool)(options & QLcd::drawNumber) != enable) {
        options ^= QLcd::drawNumber;
        update();
    }
}

void QLCDString::paintEvent(QPaintEvent *e)
{
    QPainter p(this);
    QLcd::draw(&p, 0, 0, width(), height(), digitStr.toLatin1(), options, &forColor, &shaColor);
    e->accept();
}
