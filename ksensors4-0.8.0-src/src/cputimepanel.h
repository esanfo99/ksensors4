/***************************************************************************
                          cputimepanel.h  -  part of KSensors4
                             -------------------
    begin                : Fri Jan 11 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CPUTIMEPANEL_H
#define CPUTIMEPANEL_H

#include "panel.h"
#include "procinfo.h"
#include "qlcdstring.h"
#include "qbarmeter.h"

/**
 * @author Miguel Novas
 * @author Eric Sanford
 * @short Panel for InfoPanels - System Information (CPU usage), calls getCpuTime()
 */

class CpuTimePanel : public Panel {
    Q_OBJECT

public:

    CpuTimePanel(QWidget *parent = 0, const char *name = 0);
    virtual ~CpuTimePanel();

protected:

    void updateInfo() override;
    void paintEvent(QPaintEvent *e) override;
    void resizeEvent(QResizeEvent *e) override;

private:
    unsigned old_user, old_nice, old_system, old_idle;
    QLCDString *lcd1, *lcd2, *lcd3, *lcd4;
    QBarMeter *barMeter;
};

#endif
