/***************************************************************************
                          palettecfg.cpp  -  part of KSensors4
                             -------------------
    begin                : lun abr 15 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : michaell@teleline.es
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "palettecfg.h"
#include "lmsensorswidget.h"

PaletteCfg::PaletteCfg(QWidget *parent, const char *name)
        : PaletteCfgDesign(parent, (QString("palette_design_") + name)),
        isDefaultPal(true), 
        pKColorDialog(NULL)
{
    setObjectName(QString("paletteCfg_") + name);
    slotComboSelected(0);

    buttonColor->setColor(palette().background());
    buttonColor->setDefaultColor(palette().background());

    connect(comboSelect, SIGNAL(activated(int)), this, SLOT(slotComboSelected(int)));
    connect(buttonColor, SIGNAL(changed(const QColor &)), this, SLOT(slotColorChanged(const QColor &)));
    connect(buttonDefaultPalette, SIGNAL(clicked()), this, SLOT(slotPaletteDefault()));
    connect(buttonColor, SIGNAL(clicked()), this, SLOT(slotColorButtonClicked()));
}

PaletteCfg::~PaletteCfg()
{
}

/**
 * If the user selects an item in the combobox (Background, Border, Title, 
 * or Value), then get the current palette for the sample panel shown in 
 * this config object (General, LMSensor, Hddtemp, or System), select the
 * relevant color from the current palette, and change the color of the 
 * KColorButton to the relevant color.
 * 
 * @param comboIndex : the index (0-3) corresponding to Background, Border, 
 * Title, or Value
 */
void PaletteCfg::slotComboSelected(int comboIndex)
{
    if (!framePanel) {
        return;
    }
    QPalette colors = framePanel->palette();
    QColor color;

    switch (comboIndex) {

    case 0:
        color = colors.background();
        break;
    case 1:
        color = colors.dark();
        break;
    case 2:
        color = colors.text();
        break;
    case 3:
        color = colors.foreground();
        break;
    default:
        color = QColor();
    }

    buttonColor->setColor(color);
}

/**
 * The KColorButton object has emitted a changed(const QColor &) signal. 
 * Update the palette of the sample panel in the KSensorsCfg dialog page.
 * 
 * @param newColor
 */
void PaletteCfg::slotColorChanged(const QColor &newColor)
{
    QPalette pal = framePanel->palette();

    switch (comboSelect->currentItem()) {

    case 0:
        pal.setColor(QPalette::Background, newColor);
        pal.setColor(QPalette::Light, newColor);
        break;
    case 1:
        pal.setColor(QPalette::Dark, newColor);
        break;
    case 2:
        pal.setColor(QPalette::Text, newColor);
        break;
    case 3:
        pal.setColor(QPalette::Foreground, newColor);
        break;
    default:
        return;
    }

    setPanelPalette(pal, false);
    bool isDefault = isDefaultPalette(pal);
    isDefaultPal = isDefault;
}

bool PaletteCfg::isDefaultPalette(const QPalette &pal)
{
    QPalette genPal = palette();
    LMSensorsWidget::cfgReadPalette(genPal, "General", true);
    if (pal == genPal) {
        return true;
    }
    return false;
}

/**
 * The default palette has a transparent background. This resets palette to 
 * the colors returned by LMSensorsWidget::cfgReadPalette and sets the 
 * autoFillBackground attribute to false. Note: The background is set to the
 * KDE default background (and not set to transparent) the first time the 
 * user selects the Default Color button and clicks Apply. The user has to 
 * select Default Color option and Apply twice to restore the default 
 * transparent background.
 */
void PaletteCfg::slotPaletteDefault()
{
    isDefaultPal = true;
    QPalette pal;
    if (!configName.isNull() && !configName.isEmpty()) {
        LMSensorsWidget::cfgReadPalette(pal, "General", true);
        setAutoFillBackground(false); 
    }
    else {
        LMSensorsWidget::getDefaultPalette(pal);
        setAutoFillBackground(false); 
    }
    setPanelPalette(pal, true);
    savePalette(configName);
}

void PaletteCfg::setPanelPalette(const QPalette &pal, bool updateColorButton)
{
    framePanel->setPalette(pal);
    framePanel->setAutoFillBackground(true);
    QPalette newPal;
    newPal.setColor(framePanel->foregroundRole(), pal.text());
    labelTitle->setPalette(newPal);
    if (updateColorButton) {
        slotComboSelected(comboSelect->currentItem());
    }
}

void PaletteCfg::readPalette(const QString &objectName)
{
    configName = objectName;
    QPalette pal = palette();
    bool isDefault = !LMSensorsWidget::cfgReadPalette(pal, objectName, false);
    isDefaultPal = isDefault;
    if (isDefaultPal) {
        LMSensorsWidget::cfgReadPalette(pal, "General", true);
    }
    setPanelPalette(pal, true);
}

void PaletteCfg::savePalette(const QString &objectName)
{
    if (isDefaultPal) {
        LMSensorsWidget::cfgUnsetPalette(objectName);
    }
    else {
        QPalette pal = framePanel->palette();
        LMSensorsWidget::cfgWritePalette(pal, objectName);
    }
}

void PaletteCfg::slotColorButtonClicked()
{
    pKColorDialog = buttonColor->findChild<KColorDialog *>();
    if (pKColorDialog) {
        connect(pKColorDialog, SIGNAL(finished()), this, SLOT(slotColorDialogFinished()));
        pKColorDialog->setObjectName("kcolorDialog");
    }
}

/**
 * FIXME: May 21, 2018, kcolorDialog may be destroyed before the X window
 * system is finished with it. When this occurs, the X window system emits
 * an error:
 * 
 *       X Error: BadWindow (invalid Window parameter) 3
 *       Major opcode: 20 (X_GetProperty)
 *       Resource id:  0xXX0000XX
 * 
 * As a workaround, this introduces a non-blocking 100 msec pause to allow 
 * time for the ksensorscfg dialog to be re-activated and repainted before 
 * kcolorDialog is destroyed.
 */
void PaletteCfg::slotColorDialogFinished()
{
    int waitMsecs = 100;
    QTime waitTime;
    waitTime.start();
    while (waitTime.elapsed() < waitMsecs) {
        QCoreApplication::processEvents(QEventLoop::AllEvents, 50);
    }
}
