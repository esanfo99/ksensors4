/***************************************************************************
                          hddsensor.h  -  part of KSensors4
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/* 
 * File:   hddsensor.h
 * Author: Eric Sanford
 *
 * Created on April 4, 2018, 12:50 PM
 * 
 * Copyright (C) 2018 Eric Sanford
 */

#ifndef HDDSENSOR_H
#define HDDSENSOR_H

#include "sensor.h"

#include <Qt/qstring.h>
#include <Qt/qobject.h>

/**
 * @author Eric Sanford
 * @short Sensor for Hddtemp values (from /usr/sbin/hddtemp)
 */
class HddSensor : public Sensor {
    Q_OBJECT

    friend class HDSensorsList;

public:
    HddSensor(SensorsList *parent = 0);
    virtual ~HddSensor();

protected:

    void setDisk(const QString &Disk);
    const QString &getDisk();
    QString getUdevName(const QString &model);
    void setDescription(const QString &devName) override;
    
private:

    bool hasDiskById;
    QString devDisk;
    QString udevName;
    
    bool isUdevInstalled();

};

#endif /* HDDSENSOR_H */
