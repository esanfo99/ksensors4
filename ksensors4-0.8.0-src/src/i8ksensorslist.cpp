/***************************************************************************
                          i8ksensorslist.cpp  -  part of KSensors4
                             -------------------
    begin                : jue jun 20 2002
    copyright            : (C) 2002 by Miguel Novas
    email                : migueln@users.sourceforge.net
 ***************************************************************************/
/***************************************************************************
    port to Qt4/KDE4     : Fri Jun 22 2018
    copyright            : (C) 2018 by Eric Sanford
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "i8ksensorslist.h"
#include "procinfo.h"

#include <klocale.h>

I8KSensorsList::I8KSensorsList(QWidget *parent, const char *name)
        : SensorsList(parent, name), 
        i8kAvail(0), 
        acpiAvail(0), 
        cpuTempAcpi(0),
        cpuTemp(0), 
        leftFan(0), 
        rightFan(0)
{
    setDescription(QString("i8k"));

    setClass(Sensor::lmSensor);

    readConfig();

    if ((i8kAvail = (I8KOnlyAvailable() == 0))) {
        cpuTemp = new Sensor(this);
        cpuTemp->setType(Sensor::lmTemp);
        cpuTemp->setObjectName("cputemp");
        cpuTemp->setDescription("CPU Temp");
        cpuTemp->setValueMax(40, Sensor::dgCelsius);
        cpuTemp->setValueMin(0, Sensor::dgCelsius);
        cpuTemp->setValueIdeal(30, Sensor::dgCelsius);
        cpuTemp->setValue(30, Sensor::dgCelsius);
        cpuTemp->readConfig();

        leftFan = new Sensor(this);
        leftFan->setType(Sensor::lmFan);
        leftFan->setObjectName("fan1");
        leftFan->setDescription("Left Fan");
        leftFan->setValueMax(10000);
        leftFan->setValueMin(0);
        leftFan->setValueIdeal(4500);
        leftFan->setValue(4500);
        leftFan->readConfig();

        rightFan = new Sensor(this);
        rightFan->setType(Sensor::lmFan);
        rightFan->setObjectName("fan2");
        rightFan->setDescription("Right Fan");
        rightFan->setValueMax(10000);
        rightFan->setValueMin(0);
        rightFan->setValueIdeal(4500);
        rightFan->setValue(4500);
        rightFan->readConfig();
    }

    if ((acpiAvail = (AcpiAvailable() == 0))) {
        cpuTempAcpi = new Sensor(this);
        cpuTempAcpi->setType(Sensor::lmTemp);
        cpuTempAcpi->setObjectName("cputemp");
        cpuTempAcpi->setDescription("CPU Temp");
        cpuTempAcpi->setValueMax(40, Sensor::dgCelsius);
        cpuTempAcpi->setValueMin(0, Sensor::dgCelsius);
        cpuTempAcpi->setValueIdeal(30, Sensor::dgCelsius);
        cpuTempAcpi->setValue(30, Sensor::dgCelsius);
        cpuTempAcpi->readConfig();
    }

    updateSensors();
}

I8KSensorsList::~I8KSensorsList()
{
    delete cpuTemp;
    delete leftFan;
    delete rightFan;
    delete cpuTempAcpi;
}

void I8KSensorsList::updateSensors()
{
    double t, f1, f2;

    if (i8kAvail && !getI8KInfo(&t, &f1, &f2)) {
        cpuTemp->setValue(t, Sensor::dgCelsius);
        leftFan->setValue(f1);
        rightFan->setValue(f2);
    }

    if (acpiAvail && !getAcpiTemperature(&t)) {
        cpuTempAcpi->setValue(t, Sensor::dgCelsius);
    }
}

int I8KSensorsList::AcpiAvailable()
{
    double t;
    return getAcpiTemperature(&t);
}

int I8KSensorsList::I8KOnlyAvailable()
{
    double d1, d2, d3;
    return getI8KInfo(&d1, &d2, &d3);
}

bool I8KSensorsList::I8KAvailable()
{
    int result;

    switch (result = I8KOnlyAvailable()) {
    case -2:
        qWarning("KSensors4 I8KSensorsList Warning: /proc/i8k format not valid or not supported.");
        break;
    case -3:
        qWarning("KSensors4 I8KSensorsList Warning: Only format version 1.0 is supported.");
        break;
    }

    return (result == 0 || AcpiAvailable() == 0);
}
