#!/usr/bin/env bash

# Occasionally you may want to start over with a different set of cmake
# options. You could delete the entire ksensors4-0.8.0-build directory, but
# if you want to preserve some non-cmake files (diffs, custom scripts, etc.)
# then a distclean script may be helpful. 
#
# Before running this script, consider whether to run 'make unininstall-all'
# to remove files that might become orphaned with a different set of cmake
# options. 
#
# To use this script: 
# cd ../ksensors4-0.8.0-build  
# cp ../ksensors4-0.8.0-src/custom_distclean.sh .  
# chmod +x ./custom_distclean.sh  
# ./custom_distclean.sh  
#
# After running this script, you can also manually remove these files if no 
# longer needed:  
# - compile_commands.json  
# - install_manifest.txt  

DISTCLEAN_FILES="
CMakeCache.txt
cmake_install.cmake
cmake_uninstall.cmake
Makefile
CMakeDoxyfile.in
CMakeDoxygenDefaults.cmake
"

DISTCLEAN_DIRS="
CMakeFiles
CMakeTmp
doc
po
src
"

if test -f "install_manifest.txt"; then 
  make clean-all
fi

for FILE in $DISTCLEAN_FILES; do
  if test -e "$FILE"; then
    rm -v "$FILE";
  else 
    echo "Does not exist: $FILE" 
  fi
done

for DIR in $DISTCLEAN_DIRS; do
  if test -e "$DIR"; then
    echo "rm -rf $DIR" 
    rm -rf "$DIR"
  else 
    echo "Does not exist: $DIR" 
  fi
done

