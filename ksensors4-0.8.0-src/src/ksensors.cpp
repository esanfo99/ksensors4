/*
 * Copyright (C) 2003 noname <s@s.org>
 */

#include "ksensors.h"

#include <Qt/qlabel.h>

/**
 * To be implemented.
 * 
 * Set the shell's ui resource file
 */
ksensors::ksensors() : KParts::MainWindow()
{
    setXMLFile("ksensorsui.rc");
    (void) new QLabel(QString("KSensors4"), this);
}

ksensors::~ksensors()
{
}
